function [] = caxiscenter(axh)
% CAXISCENTER   Center caxis around zero
%
%    CAXISCENTER
%    CAXISCENTER(axh)
%       axh : axis handle
%

% Written by Colin Humphries
%   5/2010

% Copywrite (c) 2010-2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/

if nargin < 1
  axh = gca;
end

ct = caxis(axh);
nct = max(abs(ct));
caxis(axh,[-nct nct]);
