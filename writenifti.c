/*
writenifti

Write MRI files in the NIFTI format
%            writenifti(data,filename,vsize,orient)
%            writenifti(data,fileanme,vsize,orient,dtype)
%            writenifti(data,isize,filename,vsize,orient)
%            writenifti(data,isize,filename,vsize,orient,index)
%            writenifti(data,isize,filename,vsize,orient,dtype)
%            writenifti(data,isize,filename,vsize,orient,dtype,index)


Written by Colin Humphries
1/2011

2/2011 - added support for logical class
6/2011 - added support for .img/.hdr nifti files
6/2011 - fixed rot2quat bug

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdint.h>
#include "mex.h"
#include "nifti1.h"
#include "zlib.h"

/* Input Arguments */

#define ARG1_IN     prhs[0]
#define ARG2_IN     prhs[1]
#define ARG3_IN     prhs[2]
#define ARG4_IN     prhs[3]
#define ARG5_IN     prhs[4]
#define ARG6_IN     prhs[5]
#define ARG7_IN     prhs[6]

/* NI1 is ni1 which uses .img and .hdr
   NP1 is n+1 which uses .nii */
#define OUTPUT_TYPE_NI1 1
#define OUTPUT_TYPE_NP1 2
#define DEFAULT_OUTPUT_TYPE 2

#define DEFAULT_USEZIPPING 1

#define USE_ZLIB

void processorient(nifti_1_header *, const mxArray *);
void quat2rot(double, double, double, int, double, double, double, double *);
void rot2quat(double *, double *, int);
void convert_array_uint8(uint8_t *, const mxArray *);
void convert_array_int8(int8_t *, const mxArray *);
void convert_array_uint16(uint16_t *, const mxArray *);
void convert_array_int16(int16_t *, const mxArray *);
void convert_array_uint32(uint32_t *, const mxArray *);
void convert_array_int32(int32_t *, const mxArray *);
void convert_array_uint64(uint64_t *, const mxArray *);
void convert_array_int64(int64_t *, const mxArray *);
void convert_array_single(float *, const mxArray *);
void convert_array_double(double *, const mxArray *);
int findstr(const char *, const char *);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  char *filename, *dtype = NULL, *fileprefix;
  int ii, numdims, sindex;
  double *pTmp;
  nifti_1_header *nheader;
  const mwSize *mxdims;
  const mxArray *pIsize = NULL, *pIndex = NULL, *DATA_IN;
  mxArray *fulldata = NULL;

  void *databuffer, *p_datain;
  char extension = (0,0,0,0);
  int usezipping = 0, outputtype;

  /* Check for proper number of arguments. */
  if (nrhs < 2) {
    mexErrMsgTxt("Must include at least two inputs");
  }
 
  /* Create NIFTI header */
  nheader = (nifti_1_header *)mxMalloc(sizeof(nifti_1_header));

  /* Set header defaults */
  nheader->sizeof_hdr = 348;
  /* nheader->vox_offset = 352; */
  nheader->scl_slope = 1;
  nheader->scl_inter = 0;
  nheader->cal_min = 0;
  nheader->cal_max = 0;
  /* strcpy(nheader->magic,"n+1"); */
  for (ii=0; ii<8; ++ii) {
    nheader->pixdim[ii] = 0;
    nheader->dim[ii] = 1; /* Everybody intializes these values as 1. 
			     Not sure why. */
  }
  nheader->xyzt_units = 0;
  nheader->slice_code = 0;
  nheader->dim_info = 0;
  nheader->slice_start = 0;
  nheader->slice_end = 0;
  strcpy(nheader->descrip,"writenifti.mex");
  nheader->intent_code = NIFTI_INTENT_NONE;
  nheader->intent_p1 = 0;
  nheader->intent_p2 = 0;
  nheader->intent_p3 = 0;
  nheader->intent_name[0] = 0;
  nheader->aux_file[0] = 0;
  nheader->toffset = 0;
  nheader->data_type[0] = 0;
  nheader->db_name[0] = 0;
  nheader->slice_duration = 0;
  nheader->toffset = 0;
  nheader->dim_info = 0;
  nheader->extents = 0;
  nheader->session_error = 0;
  nheader->regular = 0;
  nheader->glmin = 0;
  nheader->glmax = 0;

  /* Parse arguments */
  DATA_IN = ARG1_IN;
  if (mxIsChar(ARG2_IN)) {
    /* Second argument is filename */
    filename = mxArrayToString(ARG2_IN);
    if (!filename) {
      mexErrMsgTxt("Can't get filename");
    }
    /* Data array must be 3-dimensional or higher */
    numdims = mxGetNumberOfDimensions(DATA_IN);
    if (numdims < 3) {
      mexErrMsgTxt("Data must be 3-d or higher, or specify isize");
    }
    if (numdims > 7) {
      mexErrMsgTxt("Data must be 7 dimensions or less");
    }
    mxdims = mxGetDimensions(DATA_IN);
    nheader->dim[0] = numdims;
    for (ii=0; ii<numdims; ++ii) {
      nheader->dim[ii+1] = mxdims[ii];
    }
    
    if (nrhs > 2) {
      /* Vsize variable */
      if (mxGetNumberOfElements(ARG3_IN) != numdims) {
	mexErrMsgTxt("Length of vsize must equal number of data dimensions");
      }
      if (!mxIsDouble(ARG3_IN)) {
	mexErrMsgTxt("vsize must be double precision");
      }
      pTmp = mxGetPr(ARG3_IN);
      for (ii=0; ii<numdims; ++ii) {
	nheader->pixdim[ii+1] = pTmp[ii];
      }
    }
    else {
      /* Use default of 1x1x1x... mm */
      for (ii=0; ii<numdims; ++ii) {
	nheader->pixdim[ii+1] = 1;
      }
    }
    if (nrhs > 3) {
      /* Orient variable */
      processorient(nheader,ARG4_IN);
    }
    else {
      processorient(nheader,NULL);
    }
    if (nrhs > 4) {
      /* DataType variable */
      if (mxIsChar(ARG5_IN) != 1) {
	mexErrMsgTxt("dtype must be a string");
      }
      dtype = mxArrayToString(ARG5_IN);
    }
  }
  else {
    /* Second argument is isize */
    pIsize = ARG2_IN;
    if (!mxIsDouble(pIsize)) {
      mexErrMsgTxt("isize must be double precision");
    }
    numdims = mxGetNumberOfElements(pIsize);
    if (numdims < 2 || numdims > 7) {
      mexErrMsgTxt("number of dimensions must be between 2 and 7");
    }
    nheader->dim[0] = numdims;
    pTmp = mxGetPr(ARG2_IN);
    for (ii=0; ii<numdims; ++ii) {
      nheader->dim[ii+1] = (int)pTmp[ii];
    }
    if (nrhs < 3) {
      mexErrMsgTxt("Not enough inputs");
    }
    /* filename variable */
    filename = mxArrayToString(ARG3_IN);
    if (!filename) {
      mexErrMsgTxt("Can't get filename");
    }
    if (nrhs > 3) {
      /* Vsize variable */
      if (mxGetNumberOfElements(ARG4_IN) != numdims) {
	mexErrMsgTxt("Length of vsize must equal number of data dimensions");
      }
      if (!mxIsDouble(ARG4_IN)) {
	mexErrMsgTxt("vsize must be double precision");
      }
      pTmp = mxGetPr(ARG4_IN);
      for (ii=0; ii<numdims; ++ii) {
	nheader->pixdim[ii+1] = pTmp[ii];
      }
    }
    else {
      /* Default vsize is 1mm */
      for (ii=0; ii<numdims; ++ii) {
	nheader->pixdim[ii+1] = 1;
      }
    }
    if (nrhs > 4) {
      /* Orient variable */
      processorient(nheader,ARG5_IN);
    }
    else {
      processorient(nheader,NULL);
    }
    if (nrhs > 5) {
      if (mxIsChar(ARG6_IN) == 1) {
	/* ARG6 is datatype string */
	dtype = mxArrayToString(ARG6_IN);
	if (nrhs > 6) {
	  /* ARG7 is the index vector */
	  if (!mxIsDouble(ARG7_IN)) {
	    mexErrMsgTxt("index must be double precision");
	  }
	  pIndex = ARG7_IN;
	}
      }
      else {
	/* ARG6 is index vector */
	if (!mxIsDouble(ARG6_IN)) {
	  mexErrMsgTxt("index must be double precision");
	}
	pIndex = ARG6_IN;
      }
    }
  }
  
  if (pIndex) {
    /* If there is an index vector then we should use it to 
       construct the full volume. */
    mwSize *dimensions = (mwSize *)mxCalloc(numdims,sizeof(mwSize));
    double *pIndexData = mxGetPr(pIndex);
    int fds;
    void *pfulldata, *pindexeddata;
    int indexval;

    if (mxGetNumberOfElements(DATA_IN) != mxGetNumberOfElements(pIndex)) {
      mexErrMsgTxt("length of data and index do not match");
    }

    for (ii=0; ii<numdims; ++ii) {
      /* Read dimension info from nifti header */
      dimensions[ii] = (mwSize)nheader->dim[ii+1];
    }
    /* Create a matlab array because it will be easier 
       to interface with later code */
    fulldata = mxCreateNumericArray(numdims,dimensions,
				    mxGetClassID(DATA_IN),
				    mxREAL);
    fds = mxGetNumberOfElements(fulldata);
    pfulldata = mxGetData(fulldata);
    pindexeddata = mxGetData(DATA_IN);
    for (ii=0; ii<mxGetNumberOfElements(pIndex); ++ii) {
      indexval = (int)pIndexData[ii]-1;
      /* Check if index value is valid */
      if (indexval < 0 || indexval >= fds) {
	mexErrMsgTxt("Index exceeds volume defined by isize");
      }
      switch (mxGetClassID(fulldata)) {
      case mxUINT8_CLASS:
	((uint8_t *)pfulldata)[indexval] = ((uint8_t *)pindexeddata)[ii];
	break;
      case mxINT8_CLASS:
	((int8_t *)pfulldata)[indexval] = ((int8_t *)pindexeddata)[ii];
	break;
      case mxUINT16_CLASS:
	((uint16_t *)pfulldata)[indexval] = ((uint16_t *)pindexeddata)[ii];
	break;
      case mxINT16_CLASS:
	((int16_t *)pfulldata)[indexval] = ((int16_t *)pindexeddata)[ii];
	break;
      case mxUINT32_CLASS:
	((uint32_t *)pfulldata)[indexval] = ((uint32_t *)pindexeddata)[ii];
	break;
      case mxINT32_CLASS:
	((int32_t *)pfulldata)[indexval] = ((int32_t *)pindexeddata)[ii];
	break;
      case mxUINT64_CLASS:
	((uint64_t *)pfulldata)[indexval] = ((uint64_t *)pindexeddata)[ii];
	break;
      case mxINT64_CLASS:
	((int64_t *)pfulldata)[indexval] = ((int64_t *)pindexeddata)[ii];
	break;
      case mxSINGLE_CLASS:
	((float *)pfulldata)[indexval] = ((float *)pindexeddata)[ii];
	break;
      case mxDOUBLE_CLASS:
	((double *)pfulldata)[indexval] = ((double *)pindexeddata)[ii];
	break;
      case mxLOGICAL_CLASS:
	((mxLogical *)pfulldata)[indexval] = ((mxLogical *)pindexeddata)[ii];
	break;
       default:
	mexErrMsgTxt("unsupported matlab datatype");
      }
    }
    mxFree(dimensions);
    DATA_IN = fulldata;
  }

  if (dtype) {
    /* In this case the output datatype is specified and we need 
       to cast all of the data into the desired type. This is a
       bit of a headache because the input data could be several
       different datatypes as well. */
    if (strcmp(dtype,"uint8") == 0) {
      nheader->datatype = NIFTI_TYPE_UINT8;
      nheader->bitpix = sizeof(uint8_t)*8;
      if (mxGetClassID(DATA_IN) == mxUINT8_CLASS) {
	databuffer = NULL;
      }
      else {
	databuffer = mxCalloc(mxGetNumberOfElements(DATA_IN),sizeof(uint8_t));
	convert_array_uint8((uint8_t *)databuffer,DATA_IN);
      }
    }
    else if (strcmp(dtype,"int8") == 0) {
      nheader->datatype = NIFTI_TYPE_INT8;
      nheader->bitpix = sizeof(int8_t)*8;
      if (mxGetClassID(DATA_IN) == mxINT8_CLASS) {
	databuffer = NULL;
      }
      else {
	databuffer = mxCalloc(mxGetNumberOfElements(DATA_IN),sizeof(int8_t));
	convert_array_int8((int8_t *)databuffer,DATA_IN);
      }
    }
    else if (strcmp(dtype,"uint16") == 0) {
      nheader->datatype = NIFTI_TYPE_UINT16;
      nheader->bitpix = sizeof(uint16_t)*8;
      if (mxGetClassID(DATA_IN) == mxUINT16_CLASS) {
	databuffer = NULL;
      }
      else {
	databuffer = mxCalloc(mxGetNumberOfElements(DATA_IN),sizeof(uint16_t));
	convert_array_uint16((uint16_t *)databuffer,DATA_IN);
      }
    }
    else if (strcmp(dtype,"int16") == 0) {
      nheader->datatype = NIFTI_TYPE_INT16;
      nheader->bitpix = sizeof(int16_t)*8;
      if (mxGetClassID(DATA_IN) == mxINT16_CLASS) {
	databuffer = NULL;
      }
      else {
	databuffer = mxCalloc(mxGetNumberOfElements(DATA_IN),sizeof(int16_t));
	convert_array_int16((int16_t *)databuffer,DATA_IN);
      }
    }
    else if  (strcmp(dtype,"uint32") == 0) {
      nheader->datatype = NIFTI_TYPE_UINT32;
      nheader->bitpix = sizeof(uint32_t)*8;
      if (mxGetClassID(DATA_IN) == mxUINT32_CLASS) {
	databuffer = NULL;
      }
      else {
	databuffer = mxCalloc(mxGetNumberOfElements(DATA_IN),sizeof(uint32_t));
	convert_array_uint32((uint32_t *)databuffer,DATA_IN);
      }
    }
    else if  (strcmp(dtype,"int32") == 0) {
      nheader->datatype = NIFTI_TYPE_INT32;
      nheader->bitpix = sizeof(int32_t)*8;
      if (mxGetClassID(DATA_IN) == mxINT32_CLASS) {
	databuffer = NULL;
      }
      else {
	databuffer = mxCalloc(mxGetNumberOfElements(DATA_IN),sizeof(int32_t));
	convert_array_int32((int32_t *)databuffer,DATA_IN);
      }
    }
    else if  (strcmp(dtype,"uint64") == 0) {
      nheader->datatype = NIFTI_TYPE_UINT64;
      nheader->bitpix = sizeof(uint64_t)*8;
      if (mxGetClassID(DATA_IN) == mxUINT64_CLASS) {
	databuffer = NULL;
      }
      else {
	databuffer = mxCalloc(mxGetNumberOfElements(DATA_IN),sizeof(uint64_t));
	convert_array_uint64((uint64_t *)databuffer,DATA_IN);
      }
    }
    else if  (strcmp(dtype,"int64") == 0) {
      nheader->datatype = NIFTI_TYPE_INT64;
      nheader->bitpix = sizeof(int64_t)*8;
      if (mxGetClassID(DATA_IN) == mxINT64_CLASS) {
	databuffer = NULL;
      }
      else {
	databuffer = mxCalloc(mxGetNumberOfElements(DATA_IN),sizeof(int64_t));
	convert_array_int64((int64_t *)databuffer,DATA_IN);
      }
    }
    else if  (strcmp(dtype,"single") == 0) {
      nheader->datatype = NIFTI_TYPE_FLOAT32;
      nheader->bitpix = sizeof(float)*8;
      if (mxGetClassID(DATA_IN) == mxSINGLE_CLASS) {
	databuffer = NULL;
      }
      else {
	databuffer = mxCalloc(mxGetNumberOfElements(DATA_IN),sizeof(float));
	convert_array_single((float *)databuffer,DATA_IN);
      }
    }
    else if  (strcmp(dtype,"double") == 0) {
      nheader->datatype = NIFTI_TYPE_FLOAT64;
      nheader->bitpix = sizeof(double)*8;
      if (mxGetClassID(DATA_IN) == mxDOUBLE_CLASS) {
	databuffer = NULL;
      }
      else {
	databuffer = mxCalloc(mxGetNumberOfElements(DATA_IN),sizeof(double));
	convert_array_double((double *)databuffer,DATA_IN);
      }
    }
    else {
      mexErrMsgTxt("unknown or unsupported NIFTI datatype");
    }
  }
  else {
    /* In this case no output datatype is specified so we will 
       write the data using the same datatype as the input. 
       Except for the Logical class which we convert to uint8 by
       default. */
    databuffer = NULL;
    switch (mxGetClassID(DATA_IN)) {
    case mxUINT8_CLASS:
      nheader->datatype = NIFTI_TYPE_UINT8;
      nheader->bitpix = sizeof(uint8_t)*8;
      break;
    case mxINT8_CLASS:
      nheader->datatype = NIFTI_TYPE_INT8;
      nheader->bitpix = sizeof(int8_t)*8;
      break;
    case mxUINT16_CLASS:
      nheader->datatype = NIFTI_TYPE_UINT16;
      nheader->bitpix = sizeof(uint16_t)*8;
      break;
    case mxINT16_CLASS:
      nheader->datatype = NIFTI_TYPE_INT16;
      nheader->bitpix = sizeof(int16_t)*8;
      break;
    case mxUINT32_CLASS:
      nheader->datatype = NIFTI_TYPE_UINT32;
      nheader->bitpix = sizeof(uint32_t)*8;
      break;
    case mxINT32_CLASS:
      nheader->datatype = NIFTI_TYPE_INT32;
      nheader->bitpix = sizeof(int32_t)*8;
      break;
    case mxUINT64_CLASS:
      nheader->datatype = NIFTI_TYPE_UINT64;
      nheader->bitpix = sizeof(uint64_t)*8;
      break;
    case mxINT64_CLASS:
      nheader->datatype = NIFTI_TYPE_INT64;
      nheader->bitpix = sizeof(int64_t)*8;
      break;
    case mxSINGLE_CLASS:
      nheader->datatype = NIFTI_TYPE_FLOAT32;
      nheader->bitpix = sizeof(float)*8;
      break;
    case mxDOUBLE_CLASS:
      nheader->datatype = NIFTI_TYPE_FLOAT64;
      nheader->bitpix = sizeof(double)*8;
      break;
    case mxLOGICAL_CLASS:
      nheader->datatype = NIFTI_TYPE_UINT8;
      nheader->bitpix = sizeof(uint8_t)*8;
      databuffer = mxCalloc(mxGetNumberOfElements(DATA_IN),sizeof(uint8_t));
      convert_array_uint8((uint8_t *)databuffer,DATA_IN);
      break;
    default:
      mexErrMsgTxt("unsupported matlab datatype");
    }
  }

  /* 
     The output format is determined from the filename. Valid outputs are
     .nii
     .nii.gz
     .img
     .img.gz
     .hdr
     none  => use default
  */

  /* If the filename has a '.gz' at the end then we will zip the output */
  ii = strlen(filename);
  fileprefix = (char *)mxCalloc(ii+8,sizeof(char));
  if (strcmp(filename+(ii-3),".gz") == 0) {
    usezipping = 1;
  }
  else {
    usezipping = 0;
  }
  /* Check for .nii, .img, .hdr, or none */
  if ((sindex = findstr(filename,".nii")) >= 0) {
    outputtype = OUTPUT_TYPE_NP1;
  }
  else if ((sindex = findstr(filename,".img")) >= 0) {
    outputtype = OUTPUT_TYPE_NI1;
  }
  else if ((sindex = findstr(filename,".hdr")) >= 0) {
    outputtype = OUTPUT_TYPE_NI1;
  }
  else {
    sindex = -1;
    outputtype = DEFAULT_OUTPUT_TYPE;
    usezipping = DEFAULT_USEZIPPING;
  }
  if (sindex < 0) {
    strcpy(fileprefix,filename);
  }
  else {
    strncpy(fileprefix,filename,sindex);
  }

  if (outputtype == OUTPUT_TYPE_NP1) {
    strcpy(nheader->magic,"n+1");
    nheader->vox_offset = 352;
  }
  else {
    strcpy(nheader->magic,"ni1");
    nheader->vox_offset = 0;
  }
#ifdef USE_ZLIB
  /* Output data */
  if (usezipping) {
    /* Output using gzipping */
    gzFile fgzout;
    if (outputtype == OUTPUT_TYPE_NP1) {
      /* Output in one .nii.gz file */
      fgzout = gzopen(strcat(fileprefix,".nii.gz"),"wb");
      gzwrite(fgzout,(char *)nheader,sizeof(nifti_1_header));
      gzwrite(fgzout,&extension,4*sizeof(char));
    }
    else {
      /* Output in two files .hdr and .img.gz */
      FILE *fout;
      ii = strlen(fileprefix);
      fout = fopen(strcat(fileprefix,".hdr"),"w");
      if (!fout) {
	mexErrMsgTxt("Could not open file for writing.");
      }
      fwrite(nheader,sizeof(nifti_1_header),1,fout);
      fclose(fout);
      fileprefix[ii] = '\0';
      fgzout = gzopen(strcat(fileprefix,".img.gz"),"wb");
    }
    if (databuffer) {
      /* output from databuffer */
      gzwrite(fgzout,(char *)databuffer,
	      mxGetNumberOfElements(DATA_IN)*(nheader->bitpix)/8);
    }
    else {
      /* output directly from mxArray */
      gzwrite(fgzout,(char *)mxGetData(DATA_IN),
	      mxGetNumberOfElements(DATA_IN)*(nheader->bitpix)/8);
    }
    gzclose(fgzout);
  }
  else {
#endif
    /* Output unzipped */
    FILE *fout;
    if (outputtype == OUTPUT_TYPE_NP1) {
      /* Output in one .nii file */
      fout = fopen(strcat(fileprefix,".nii"),"w");
      if (!fout) {
	mexErrMsgTxt("Could not open file for writing.");
      }
      fwrite(nheader,sizeof(nifti_1_header),1,fout);
      fwrite(&extension,sizeof(char),4,fout);
    }
    else {
      /* Output in two files .hdr and .img */
      ii = strlen(fileprefix);
      fout = fopen(strcat(fileprefix,".hdr"),"w");
      if (!fout) {
	mexErrMsgTxt("Could not open file for writing.");
      }
      fwrite(nheader,sizeof(nifti_1_header),1,fout);
      fclose(fout);
      fileprefix[ii] = '\0';
      fout = fopen(strcat(fileprefix,".img"),"w");
      if (!fout) {
	mexErrMsgTxt("Could not open file for writing.");
      }
    }
    if (databuffer) {
      /* output from databuffer */
      fwrite(databuffer,(nheader->bitpix)/8,mxGetNumberOfElements(DATA_IN),fout);
    }
    else {
      /* output directly from mxArray */
      fwrite(mxGetData(DATA_IN),(nheader->bitpix)/8,mxGetNumberOfElements(DATA_IN),fout);
    }
    fclose(fout);
#ifdef USE_ZLIB
  }
#endif

  /* Free memory */
  mxFree(filename);
  mxFree(fileprefix);
  if (dtype) {
    mxFree(dtype);
  }
  if (databuffer) {
    mxFree(databuffer);
  }
  if (fulldata) {
    mxDestroyArray(fulldata);
  }
}


void processorient(nifti_1_header *pnh, const mxArray *pMA)
{
  /* This function sets the orientation values in the nifti header 
     using orient input. 
     There are several possible ways of specifying the nifti orientation. 
     We are assuming that the pixdim and dim arrays are already specified in
     the NIFTI header. */
  float offx, offy, offz;
  double *pT, *pQRTmp;
  int numdims;
  mxArray *pArr;
  char letter[10];

  if (!pMA) {
    /* default orientation LPI */
    offx = -1*(float)pnh->dim[1]*pnh->pixdim[1]/2;
    offy = -1*(float)pnh->dim[2]*pnh->pixdim[2]/2;
    offz = -1*(float)pnh->dim[3]*pnh->pixdim[3]/2;
    pnh->sform_code = 1;
    pnh->srow_x[0] = pnh->pixdim[1];
    pnh->srow_x[1] = 0;
    pnh->srow_x[2] = 0;
    pnh->srow_x[3] = offx;
    pnh->srow_y[0] = 0;
    pnh->srow_y[1] = pnh->pixdim[2];
    pnh->srow_y[2] = 0;
    pnh->srow_y[3] = offy;
    pnh->srow_z[0] = 0;
    pnh->srow_z[1] = 0;
    pnh->srow_z[2] = pnh->pixdim[3];
    pnh->srow_z[3] = offz;
    
    pnh->qform_code = 1;
    pnh->pixdim[0] = 1;
    pnh->quatern_b = 0;
    pnh->quatern_c = 0;
    pnh->quatern_d = 0;
    pnh->qoffset_x = offx;
    pnh->qoffset_y = offy;
    pnh->qoffset_z = offz;
  }
  else if (mxIsEmpty(pMA)) {
    /* default orientation LPI */
    offx = -1*(float)pnh->dim[1]*pnh->pixdim[1]/2;
    offy = -1*(float)pnh->dim[2]*pnh->pixdim[2]/2;
    offz = -1*(float)pnh->dim[3]*pnh->pixdim[3]/2;
    pnh->sform_code = 1;
    pnh->srow_x[0] = pnh->pixdim[1];
    pnh->srow_x[1] = 0;
    pnh->srow_x[2] = 0;
    pnh->srow_x[3] = offx;
    pnh->srow_y[0] = 0;
    pnh->srow_y[1] = pnh->pixdim[2];
    pnh->srow_y[2] = 0;
    pnh->srow_y[3] = offy;
    pnh->srow_z[0] = 0;
    pnh->srow_z[1] = 0;
    pnh->srow_z[2] = pnh->pixdim[3];
    pnh->srow_z[3] = offz;
    
    pnh->qform_code = 1;
    pnh->pixdim[0] = 1;
    pnh->quatern_b = 0;
    pnh->quatern_c = 0;
    pnh->quatern_d = 0;
    pnh->qoffset_x = offx;
    pnh->qoffset_y = offy;
    pnh->qoffset_z = offz;
  }
  else if (mxIsDouble(pMA)) {
    numdims = mxGetNumberOfElements(pMA);
    pT = mxGetPr(pMA);
    if (numdims == 3) {
      /* orientation LPI 
         with offsets     */
      pnh->sform_code = 1;
      pnh->srow_x[0] = pnh->pixdim[1];
      pnh->srow_x[1] = 0;
      pnh->srow_x[2] = 0;
      pnh->srow_x[3] = pT[0];
      pnh->srow_y[0] = 0;
      pnh->srow_y[1] = pnh->pixdim[2];
      pnh->srow_y[2] = 0;
      pnh->srow_y[3] = pT[1];
      pnh->srow_z[0] = 0;
      pnh->srow_z[1] = 0;
      pnh->srow_z[2] = pnh->pixdim[3];
      pnh->srow_z[3] = pT[2];
      
      pnh->qform_code = 1;
      pnh->pixdim[0] = 1;
      pnh->quatern_b = 0;
      pnh->quatern_c = 0;
      pnh->quatern_d = 0;
      pnh->qoffset_x = pT[1];
      pnh->qoffset_y = pT[2];
      pnh->qoffset_z = pT[3];

    }
    else if (numdims == 6) {
      /* 3 element quaternion (b,c,d), offsets */
      pQRTmp = (double *)mxCalloc(9,sizeof(double));

      quat2rot(pT[0],pT[1],pT[2],1,pnh->pixdim[1],pnh->pixdim[2],
	       pnh->pixdim[3],pQRTmp);

      pnh->sform_code = 1;
      pnh->srow_x[0] = pQRTmp[0];
      pnh->srow_x[1] = pQRTmp[1];
      pnh->srow_x[2] = pQRTmp[2];
      pnh->srow_x[3] = pT[3];
      pnh->srow_y[0] = pQRTmp[3];
      pnh->srow_y[1] = pQRTmp[4];
      pnh->srow_y[2] = pQRTmp[5];
      pnh->srow_y[3] = pT[4];
      pnh->srow_z[0] = pQRTmp[6];
      pnh->srow_z[1] = pQRTmp[7];
      pnh->srow_z[2] = pQRTmp[8];
      pnh->srow_z[3] = pT[5];
      
      pnh->qform_code = 1;
      pnh->pixdim[0] = 1;
      pnh->quatern_b = pT[0];
      pnh->quatern_c = pT[1];
      pnh->quatern_d = pT[2];
      pnh->qoffset_x = pT[3];
      pnh->qoffset_y = pT[4];
      pnh->qoffset_z = pT[5];

      mxFree(pQRTmp);
    }
    else if (numdims == 7) {
      /* 3 element quaternion(b,c,d), offsets, qfac */
       pQRTmp = (double *)mxCalloc(9,sizeof(double));

       quat2rot(pT[0],pT[1],pT[2],(int)pT[6],pnh->pixdim[1],pnh->pixdim[2],
	       pnh->pixdim[3],pQRTmp);

      pnh->sform_code = 1;
      pnh->srow_x[0] = pQRTmp[0];
      pnh->srow_x[1] = pQRTmp[1];
      pnh->srow_x[2] = pQRTmp[2];
      pnh->srow_x[3] = pT[3];
      pnh->srow_y[0] = pQRTmp[3];
      pnh->srow_y[1] = pQRTmp[4];
      pnh->srow_y[2] = pQRTmp[5];
      pnh->srow_y[3] = pT[4];
      pnh->srow_z[0] = pQRTmp[6];
      pnh->srow_z[1] = pQRTmp[7];
      pnh->srow_z[2] = pQRTmp[8];
      pnh->srow_z[3] = pT[5];
      
      pnh->qform_code = 1;
      pnh->pixdim[0] = pT[6];
      pnh->quatern_b = pT[0];
      pnh->quatern_c = pT[1];
      pnh->quatern_d = pT[2];
      pnh->qoffset_x = pT[3];
      pnh->qoffset_y = pT[4];
      pnh->qoffset_z = pT[5];

      mxFree(pQRTmp);
    }
    else if (numdims == 12) {
      /* Reduced rotation matrix without last row */
      pQRTmp = (double *)mxCalloc(4,sizeof(double));
      
      rot2quat(pQRTmp,pT,12);
      
      pnh->sform_code = 1;
      pnh->srow_x[0] = pT[0];
      pnh->srow_x[1] = pT[3];
      pnh->srow_x[2] = pT[6];
      pnh->srow_x[3] = pT[9];
      pnh->srow_y[0] = pT[1];
      pnh->srow_y[1] = pT[4];
      pnh->srow_y[2] = pT[7];
      pnh->srow_y[3] = pT[10];
      pnh->srow_z[0] = pT[2];
      pnh->srow_z[1] = pT[5];
      pnh->srow_z[2] = pT[8];
      pnh->srow_z[3] = pT[11];
      
      pnh->qform_code = 1;
      pnh->pixdim[0] = pQRTmp[3];
      pnh->quatern_b = pQRTmp[0];
      pnh->quatern_c = pQRTmp[1];
      pnh->quatern_d = pQRTmp[2];
      pnh->qoffset_x = pT[9];
      pnh->qoffset_y = pT[10];
      pnh->qoffset_z = pT[11];

      mxFree(pQRTmp);

    }
    else if (numdims == 16) {
      /* Full rotation matrix */
      pQRTmp = (double *)mxCalloc(4,sizeof(double));
      
      rot2quat(pQRTmp,pT,16);
      
      pnh->sform_code = 1;
      pnh->srow_x[0] = pT[0];
      pnh->srow_x[1] = pT[4];
      pnh->srow_x[2] = pT[8];
      pnh->srow_x[3] = pT[12];
      pnh->srow_y[0] = pT[1];
      pnh->srow_y[1] = pT[5];
      pnh->srow_y[2] = pT[9];
      pnh->srow_y[3] = pT[13];
      pnh->srow_z[0] = pT[2];
      pnh->srow_z[1] = pT[6];
      pnh->srow_z[2] = pT[10];
      pnh->srow_z[3] = pT[14];
      
      pnh->qform_code = 1;
      pnh->pixdim[0] = pQRTmp[3];
      pnh->quatern_b = pQRTmp[0];
      pnh->quatern_c = pQRTmp[1];
      pnh->quatern_d = pQRTmp[2];
      pnh->qoffset_x = pT[12];
      pnh->qoffset_y = pT[13];
      pnh->qoffset_z = pT[14];

      mxFree(pQRTmp);
    }
    else {
      mexErrMsgTxt("unknown size for orientation");
    }
  }
  else if (mxIsStruct(pMA)) {
    /* Structure
       qfac : -1 or 1
       qform : unknown, scanner, anatomy, talairach, mni
       qmatrix : 6 element vector
       sform : unknown, scanner, anatomy, talairach, mni
       smatrix : 16 element vector
     */
    pArr = mxGetField(pMA,0,"qfac");
    if (pArr) {
      if (!mxIsDouble(pArr)) {
	mexErrMsgTxt("qfac field must be double precision");
      }
      pT = mxGetPr(pArr);
      pnh->pixdim[0] = pT[0];
    }
    else {
      mexErrMsgTxt("qfac field not in orient structure");
    }
    
    pArr = mxGetField(pMA,0,"qform");
    if (pArr) {
      if (mxIsChar(pArr)) {
	mxGetString(pArr,letter,2);
	if (letter[0] == 'u' || letter[0] == 'U') {
	  /* Unknown */
	  pnh->qform_code = 0;
	}
	else if (letter[0] == 's' || letter[0] == 'S') {
	  /* Scanner */
	  pnh->qform_code = 1;
	}
	else if (letter[0] == 'a' || letter[0] == 'A') {
	  /* Anatomy */
	  pnh->qform_code = 2;
	}
	else if (letter[0] == 't' || letter[0] == 'T') {
	  /* Talairach */
	  pnh->qform_code = 3;
	}
	else if (letter[0] == 'm' || letter[0] == 'M') {
	  /* MNI */
	  pnh->qform_code = 4;
	}
	else {
	  mexErrMsgTxt("unknown qform code");
	}
      }
      else if (mxIsDouble(pArr)) {
	pT = mxGetPr(pArr);
	if (pT[0] < 0 || pT[0] > 4) {
	  mexErrMsgTxt("qform code is not between 0 and 4");
	}
	pnh->qform_code = pT[0];
      }
      else {
	mexErrMsgTxt("qform field must be a string");
      }
    }
    else {
      mexErrMsgTxt("qform field not in orient structure");
    }
    
    pArr = mxGetField(pMA,0,"qmatrix");
    if (pArr) {
      if (!mxIsDouble(pArr)) {
	mexErrMsgTxt("qmatrix field must be double precision");
      }
      if (mxGetNumberOfElements(pArr) != 6) {
	mexErrMsgTxt("qmatrix field must be 6 elements");
      }
      pT = mxGetPr(pArr);
      pnh->quatern_b = pT[0];
      pnh->quatern_c = pT[1];
      pnh->quatern_d = pT[2];
      pnh->qoffset_x = pT[3];
      pnh->qoffset_y = pT[4];
      pnh->qoffset_z = pT[5];
    }
    else {
      mexErrMsgTxt("qmatrix field not in orient structure");
    }
    
    pArr = mxGetField(pMA,0,"sform");
    if (pArr) {
      if (mxIsChar(pArr)) {
	mxGetString(pArr,letter,2);
	if (letter[0] == 'u' || letter[0] == 'U') {
	  /* Unknown */
	  pnh->sform_code = 0;
	}
	else if (letter[0] == 's' || letter[0] == 'S') {
	  /* Scanner */
	  pnh->sform_code = 1;
	}
	else if (letter[0] == 'a' || letter[0] == 'A') {
	  /* Anatomy */
	  pnh->sform_code = 2;
	}
	else if (letter[0] == 't' || letter[0] == 'T') {
	  /* Talairach */
	  pnh->sform_code = 3;
	}
	else if (letter[0] == 'm' || letter[0] == 'M') {
	  /* MNI */
	  pnh->sform_code = 4;
	}
	else {
	  mexErrMsgTxt("unknown sform code");
	}
      }
      else if (mxIsDouble(pArr)) {
	pT = mxGetPr(pArr);
	if (pT[0] < 0 || pT[0] > 4) {
	  mexErrMsgTxt("sform code is not between 0 and 4");
	}
	pnh->sform_code = pT[0];
      }
      else {
	mexErrMsgTxt("sform field must be a string");
      }
    }
    else {
      mexErrMsgTxt("sform field not in orient structure");
    }
    
    pArr = mxGetField(pMA,0,"smatrix");
    if (pArr) {
      if (!mxIsDouble(pArr)) {
	mexErrMsgTxt("smatrix field must be double precision");
      }
      if (mxGetNumberOfElements(pArr) == 16) {
	pT = mxGetPr(pArr);
	pnh->srow_x[0] = pT[0];
	pnh->srow_x[1] = pT[4];
	pnh->srow_x[2] = pT[8];
	pnh->srow_x[3] = pT[12];
	pnh->srow_y[0] = pT[1];
	pnh->srow_y[1] = pT[5];
	pnh->srow_y[2] = pT[9];
	pnh->srow_y[3] = pT[13];
	pnh->srow_z[0] = pT[2];
	pnh->srow_z[1] = pT[6];
	pnh->srow_z[2] = pT[10];
	pnh->srow_z[3] = pT[14];
      }
      else if (mxGetNumberOfElements(pArr) == 12) {
	/* Note: this is added to provide backwards compatibility with
	 the old readnifti orient files which included the smatrix 
	 numbers directly from the file, which happened to be 
	 transposed.
	 This should not be used normally as an option. */
	pT = mxGetPr(pArr);
	pnh->srow_x[0] = pT[0];
	pnh->srow_x[1] = pT[1];
	pnh->srow_x[2] = pT[2];
	pnh->srow_x[3] = pT[3];
	pnh->srow_y[0] = pT[4];
	pnh->srow_y[1] = pT[5];
	pnh->srow_y[2] = pT[6];
	pnh->srow_y[3] = pT[7];
	pnh->srow_z[0] = pT[8];
	pnh->srow_z[1] = pT[9];
	pnh->srow_z[2] = pT[10];
	pnh->srow_z[3] = pT[11];
      }
      else {
	mexErrMsgTxt("smatrix field must be 16 elements");
      }

    }
    else {
      mexErrMsgTxt("qmatrix field not in orient structure");
    }  
  }
  else {
    mexErrMsgTxt("orientation must be a struct or double precision vector");
  }
}

void quat2rot(double qb, double qc, double qd, int qfac, double xd, double yd, double zd, double *pRot)
{
  /* pRot should be a 9 element vector */

  double qa;
  qa = 1 - qb*qb + qc*qc + qd*qd;
  if (qa < 0) {
    qa = -1*(qa-1);
    qb = qb / qa;
    qc = qc / qa;
    qd = qd / qa;
    qa = 0;
  }
  else {
    qa = sqrt(qa);
  }

  pRot[0] = qa*qa + qb*qb - qc*qc - qd*qd;
  pRot[1] = 2*(qb*qc-qa*qd);
  pRot[2] = 2*(qb*qd+qa*qc);

  pRot[3] = 2*(qb*qc+qa*qd);
  pRot[4] = qa*qa + qc*qc - qb*qb - qc*qc;
  pRot[5] = 2*(qc*qd-qa*qb);

  pRot[6] = 2*(qb*qd-qa*qc);
  pRot[7] = 2*(qc*qd+qa*qb);
  pRot[8] = qa*qa + qd*qd - qc*qc - qb*qb;

  pRot[0] = pRot[0]*xd;
  pRot[3] = pRot[3]*xd;  
  pRot[6] = pRot[6]*xd;

  pRot[1] = pRot[1]*yd;
  pRot[4] = pRot[4]*yd;  
  pRot[7] = pRot[7]*yd;

  if (qfac < 0) {
    pRot[2] = -1*pRot[2]*zd;
    pRot[5] = -1*pRot[5]*zd;
    pRot[8] = -1*pRot[8]*zd;
  }
  else {
    pRot[2] = pRot[2]*zd;
    pRot[5] = pRot[5]*zd;
    pRot[8] = pRot[8]*zd;
  }

}

void rot2quat(double *pQ, double *pT, int numelem)
{
  /* pQ is [qb qc qd qfac] */
  /* See NIFTILIB code for reference. 
     Specifically nifti1_io.c */

  double qa, qb, qc, qd;
  double xd, yd, zd, det;
  
  double r11,r12,r13,r21,r22,r23,r31,r32,r33;

  if (numelem == 12) {
    r11 = pT[0];
    r21 = pT[1];
    r31 = pT[2];
    r12 = pT[3];
    r22 = pT[4];
    r32 = pT[5];
    r13 = pT[6];
    r23 = pT[7];
    r33 = pT[8];
  }
  else {
    r11 = pT[0];
    r21 = pT[1];
    r31 = pT[2];
    r12 = pT[4];
    r22 = pT[5];
    r32 = pT[6];
    r13 = pT[8];
    r23 = pT[9];
    r33 = pT[10];
  }

  xd = sqrt(r11*r11 + r21*r21 + r31*r31);
  yd = sqrt(r12*r12 + r22*r22 + r32*r32);
  zd = sqrt(r13*r13 + r23*r23 + r33*r33);

  r11 = r11 / xd;
  r21 = r21 / xd;
  r31 = r31 / xd;
  r12 = r12 / yd;
  r22 = r22 / yd;
  r32 = r32 / yd;
  r13 = r13 / zd;
  r23 = r23 / zd;
  r33 = r33 / zd;

  det = r11*r22*r33 - r11*r32*r23 - r21*r12*r33
    + r21*r32*r13 + r31*r12*r23 - r31*r22*r13;

  if (det < 0) {
    r13 = -r13;
    r23 = -r23;
    r33 = -r33;
    pQ[3] = -1;
  }
  else {
    pQ[3] = 1;
  }

  qa = r11 + r22 + r33 + 1;
  
  if (qa > .5) {
    qa = 0.5 * sqrt(qa) ;
    qb = 0.25 * (r32-r23) / qa;
    qc = 0.25 * (r13-r31) / qa;
    qd = 0.25 * (r21-r12) / qa;
  }
  else {
    xd = 1 + r11 - (r22+r33);
    yd = 1 + r22 - (r11+r33);
    zd = 1 + r33 - (r11+r22);
    if (xd > 1) {
      qb = 0.5 * sqrt(xd) ;
      qc = 0.25* (r12+r21) / qb ;
      qd = 0.25* (r13+r31) / qb ;
      qa = 0.25* (r32-r23) / qb ;
    }
    else if (yd > 1) {
      qc = 0.5 * sqrt(yd) ;
      qb = 0.25 * (r12+r21) / qc;
      qd = 0.25 * (r23+r32) / qc;
      qa = 0.25 * (r13-r31) / qc;
    }
    else {
      qd = 0.5 * sqrt(zd) ;
      qb = 0.25 * (r13+r31) / qd;
      qc = 0.25 * (r23+r32) / qd;
      qa = 0.25 * (r21-r12) / qd;
    }
    if (qa < 0) {
      qb = -qb;
      qc = -qc;
      qd = -qd;
    }
  }

  pQ[0] = qb;
  pQ[1] = qc;
  pQ[2] = qd;

}

/* The following functions are designed to cast the data in an mxArray into a new datatype */
void convert_array_uint8(uint8_t *pD, const mxArray *pA)
{
  void *p_datain = mxGetData(pA);
  int ii;
  switch (mxGetClassID(pA)) {
  case mxUINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = ((uint8_t *)p_datain)[ii];
    }
    break;
  case mxINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint8_t)((int8_t *)p_datain)[ii];
    }
    break;
  case mxUINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint8_t)((uint16_t *)p_datain)[ii];
    }
    break;
  case mxINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint8_t)((int16_t *)p_datain)[ii];
    }
    break;
  case mxUINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint8_t)((uint32_t *)p_datain)[ii];
    }
    break;
  case mxINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint8_t)((int32_t *)p_datain)[ii];
    }
    break;
  case mxUINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint8_t)((uint64_t *)p_datain)[ii];
    }
    break;
  case mxINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint8_t)((int64_t *)p_datain)[ii];
    }
    break;
  case mxSINGLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint8_t)((float *)p_datain)[ii];
    }
    break;
  case mxDOUBLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint8_t)((double *)p_datain)[ii];
    }
    break;
  case mxLOGICAL_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint8_t)((mxLogical *)p_datain)[ii];
    }
    break;
  default:
    mexErrMsgTxt("unsupported matlab datatype");
  }
}

void convert_array_int8(int8_t *pD, const mxArray *pA)
{
  void *p_datain = mxGetData(pA);
  int ii;
  switch (mxGetClassID(pA)) {
  case mxUINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int8_t)((uint8_t *)p_datain)[ii];
    }
    break;
  case mxINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = ((int8_t *)p_datain)[ii];
    }
    break;
  case mxUINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int8_t)((uint16_t *)p_datain)[ii];
    }
    break;
  case mxINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int8_t)((int16_t *)p_datain)[ii];
    }
    break;
  case mxUINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int8_t)((uint32_t *)p_datain)[ii];
    }
    break;
  case mxINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int8_t)((int32_t *)p_datain)[ii];
    }
    break;
  case mxUINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int8_t)((uint64_t *)p_datain)[ii];
    }
    break;
  case mxINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int8_t)((int64_t *)p_datain)[ii];
    }
    break;
  case mxSINGLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int8_t)((float *)p_datain)[ii];
    }
    break;
  case mxDOUBLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int8_t)((double *)p_datain)[ii];
    }
    break;
  case mxLOGICAL_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int8_t)((mxLogical *)p_datain)[ii];
    }
    break;
  default:
    mexErrMsgTxt("unsupported matlab datatype");
  }
}

void convert_array_uint16(uint16_t *pD, const mxArray *pA)
{
  void *p_datain = mxGetData(pA);
  int ii;
  switch (mxGetClassID(pA)) {
  case mxUINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint16_t)((uint8_t *)p_datain)[ii];
    }
    break;
  case mxINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint16_t)((int8_t *)p_datain)[ii];
    }
    break;
  case mxUINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = ((uint16_t *)p_datain)[ii];
    }
    break;
  case mxINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint16_t)((int16_t *)p_datain)[ii];
    }
    break;
  case mxUINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint16_t)((uint32_t *)p_datain)[ii];
    }
    break;
  case mxINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint16_t)((int32_t *)p_datain)[ii];
    }
    break;
  case mxUINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint16_t)((uint64_t *)p_datain)[ii];
    }
    break;
  case mxINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint16_t)((int64_t *)p_datain)[ii];
    }
    break;
  case mxSINGLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint16_t)((float *)p_datain)[ii];
    }
    break;
  case mxDOUBLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint16_t)((double *)p_datain)[ii];
    }
    break;
  case mxLOGICAL_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint16_t)((mxLogical *)p_datain)[ii];
    }
    break;
  default:
    mexErrMsgTxt("unsupported matlab datatype");
  }
}

void convert_array_int16(int16_t *pD, const mxArray *pA)
{
  void *p_datain = mxGetData(pA);
  int ii;
  switch (mxGetClassID(pA)) {
  case mxUINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int16_t)((uint8_t *)p_datain)[ii];
    }
    break;
  case mxINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int16_t)((int8_t *)p_datain)[ii];
    }
    break;
  case mxUINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int16_t)((uint16_t *)p_datain)[ii];
    }
    break;
  case mxINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = ((int16_t *)p_datain)[ii];
    }
    break;
  case mxUINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int16_t)((uint32_t *)p_datain)[ii];
    }
    break;
  case mxINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int16_t)((int32_t *)p_datain)[ii];
    }
    break;
  case mxUINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int16_t)((uint64_t *)p_datain)[ii];
    }
    break;
  case mxINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int16_t)((int64_t *)p_datain)[ii];
    }
    break;
  case mxSINGLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int16_t)((float *)p_datain)[ii];
    }
    break;
  case mxDOUBLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int16_t)((double *)p_datain)[ii];
    }
    break;
  case mxLOGICAL_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int16_t)((mxLogical *)p_datain)[ii];
    }
    break;
  default:
    mexErrMsgTxt("unsupported matlab datatype");
  }
}

void convert_array_uint32(uint32_t *pD, const mxArray *pA)
{
  void *p_datain = mxGetData(pA);
  int ii;
  switch (mxGetClassID(pA)) {
  case mxUINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint32_t)((uint8_t *)p_datain)[ii];
    }
    break;
  case mxINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint32_t)((int8_t *)p_datain)[ii];
    }
    break;
  case mxUINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint32_t)((uint16_t *)p_datain)[ii];
    }
    break;
  case mxINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint32_t)((int16_t *)p_datain)[ii];
    }
    break;
  case mxUINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = ((uint32_t *)p_datain)[ii];
    }
    break;
  case mxINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint32_t)((int32_t *)p_datain)[ii];
    }
    break;
  case mxUINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint32_t)((uint64_t *)p_datain)[ii];
    }
    break;
  case mxINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint32_t)((int64_t *)p_datain)[ii];
    }
    break;
  case mxSINGLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint32_t)((float *)p_datain)[ii];
    }
    break;
  case mxDOUBLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint32_t)((double *)p_datain)[ii];
    }
    break;
  case mxLOGICAL_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint32_t)((mxLogical *)p_datain)[ii];
    }
    break;
  default:
    mexErrMsgTxt("unsupported matlab datatype");
  }
}

void convert_array_int32(int32_t *pD, const mxArray *pA)
{
  void *p_datain = mxGetData(pA);
  int ii;
  switch (mxGetClassID(pA)) {
  case mxUINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int32_t)((uint8_t *)p_datain)[ii];
    }
    break;
  case mxINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int32_t)((int8_t *)p_datain)[ii];
    }
    break;
  case mxUINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int32_t)((uint16_t *)p_datain)[ii];
    }
    break;
  case mxINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int32_t)((int16_t *)p_datain)[ii];
    }
    break;
  case mxUINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int32_t)((uint32_t *)p_datain)[ii];
    }
    break;
  case mxINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = ((int32_t *)p_datain)[ii];
    }
    break;
  case mxUINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int32_t)((uint64_t *)p_datain)[ii];
    }
    break;
  case mxINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int32_t)((int64_t *)p_datain)[ii];
    }
    break;
  case mxSINGLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int32_t)((float *)p_datain)[ii];
    }
    break;
  case mxDOUBLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int32_t)((double *)p_datain)[ii];
    }
    break;
  case mxLOGICAL_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int32_t)((mxLogical *)p_datain)[ii];
    }
    break;
  default:
    mexErrMsgTxt("unsupported matlab datatype");
  }
}

void convert_array_uint64(uint64_t *pD, const mxArray *pA)
{
  void *p_datain = mxGetData(pA);
  int ii;
  switch (mxGetClassID(pA)) {
  case mxUINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint64_t)((uint8_t *)p_datain)[ii];
    }
    break;
  case mxINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint64_t)((int8_t *)p_datain)[ii];
    }
    break;
  case mxUINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint64_t)((uint16_t *)p_datain)[ii];
    }
    break;
  case mxINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint64_t)((int16_t *)p_datain)[ii];
    }
    break;
  case mxUINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint64_t)((uint32_t *)p_datain)[ii];
    }
    break;
  case mxINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint64_t)((int32_t *)p_datain)[ii];
    }
    break;
  case mxUINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = ((uint64_t *)p_datain)[ii];
    }
    break;
  case mxINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint64_t)((int64_t *)p_datain)[ii];
    }
    break;
  case mxSINGLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint64_t)((float *)p_datain)[ii];
    }
    break;
  case mxDOUBLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint64_t)((double *)p_datain)[ii];
    }
    break;
  case mxLOGICAL_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (uint64_t)((mxLogical *)p_datain)[ii];
    }
    break;
  default:
    mexErrMsgTxt("unsupported matlab datatype");
  }
}

void convert_array_int64(int64_t *pD, const mxArray *pA)
{
  void *p_datain = mxGetData(pA);
  int ii;
  switch (mxGetClassID(pA)) {
  case mxUINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int64_t)((uint8_t *)p_datain)[ii];
    }
    break;
  case mxINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int64_t)((int8_t *)p_datain)[ii];
    }
    break;
  case mxUINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int64_t)((uint16_t *)p_datain)[ii];
    }
    break;
  case mxINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int64_t)((int16_t *)p_datain)[ii];
    }
    break;
  case mxUINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int64_t)((uint32_t *)p_datain)[ii];
    }
    break;
  case mxINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int64_t)((int32_t *)p_datain)[ii];
    }
    break;
  case mxUINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int64_t)((uint64_t *)p_datain)[ii];
    }
    break;
  case mxINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = ((int64_t *)p_datain)[ii];
    }
    break;
  case mxSINGLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int64_t)((float *)p_datain)[ii];
    }
    break;
  case mxDOUBLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int64_t)((double *)p_datain)[ii];
    }
    break;
  case mxLOGICAL_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (int64_t)((mxLogical *)p_datain)[ii];
    }
    break;
  default:
    mexErrMsgTxt("unsupported matlab datatype");
  }
}

void convert_array_single(float *pD, const mxArray *pA)
{
  void *p_datain = mxGetData(pA);
  int ii;
  switch (mxGetClassID(pA)) {
  case mxUINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (float)((uint8_t *)p_datain)[ii];
    }
    break;
  case mxINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (float)((int8_t *)p_datain)[ii];
    }
    break;
  case mxUINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (float)((uint16_t *)p_datain)[ii];
    }
    break;
  case mxINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (float)((int16_t *)p_datain)[ii];
    }
    break;
  case mxUINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (float)((uint32_t *)p_datain)[ii];
    }
    break;
  case mxINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (float)((int32_t *)p_datain)[ii];
    }
    break;
  case mxUINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (float)((uint64_t *)p_datain)[ii];
    }
    break;
  case mxINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (float)((int64_t *)p_datain)[ii];
    }
    break;
  case mxSINGLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = ((float *)p_datain)[ii];
    }
    break;
  case mxDOUBLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (float)((double *)p_datain)[ii];
    }
    break;
  case mxLOGICAL_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (float)((mxLogical *)p_datain)[ii];
    }
    break;
  default:
    mexErrMsgTxt("unsupported matlab datatype");
  }
}

void convert_array_double(double *pD, const mxArray *pA)
{
  void *p_datain = mxGetData(pA);
  int ii;
  switch (mxGetClassID(pA)) {
  case mxUINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (double)((uint8_t *)p_datain)[ii];
    }
    break;
  case mxINT8_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (double)((int8_t *)p_datain)[ii];
    }
    break;
  case mxUINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (double)((uint16_t *)p_datain)[ii];
    }
    break;
  case mxINT16_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (double)((int16_t *)p_datain)[ii];
    }
    break;
  case mxUINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (double)((uint32_t *)p_datain)[ii];
    }
    break;
  case mxINT32_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (double)((int32_t *)p_datain)[ii];
    }
    break;
  case mxUINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (double)((uint64_t *)p_datain)[ii];
    }
    break;
  case mxINT64_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (double)((int64_t *)p_datain)[ii];
    }
    break;
  case mxSINGLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (double)((float *)p_datain)[ii];
    }
    break;
  case mxDOUBLE_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = ((double *)p_datain)[ii];
    }
    break;
  case mxLOGICAL_CLASS:
    for (ii=0; ii<mxGetNumberOfElements(pA); ++ii) {
      pD[ii] = (double)((mxLogical *)p_datain)[ii];
    }
    break;
  default:
    mexErrMsgTxt("unsupported matlab datatype");
  }
}

int findstr(const char *str1, const char *str2)
{
  /* Find first occurence of str2 in str1 */
  int index = 0, foundstr = 0, ii;

  if (str1[0] == '\0' || str2[0] == '\0') {
    return -1;
  }

  while (1) {
    if (str1[index] == '\0') {
      break;
    }
    if (str1[index] == str2[0]) {
      ii = 1;
      foundstr = 1;
      while (1) {
	if (str2[ii] == '\0') {
	  break;
	}
	if (str1[index+ii] == '\0') {
	  return -1;
	}
	if (str1[index+ii] != str2[ii]) {
	  foundstr = 0;
	  break;
	}
	++ii;
      }
      if (foundstr) {
	return index;
      }
    }
    ++index;
  }

  return -1;
}
