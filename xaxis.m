%XAXIS   Control x-axis appearance
%
%   usage: [A,B] = xaxis;            - get current xaxis limits
%                  xaxis([A,B])      - change axis limits
%                  xaxis auto        - change axis scaling to auto
%                        manual      - change axis scaling to manual
%                        top         - place axis at top
%                        bottom      - place axis at bottom
%                        grid        - toggle grid on/off
%                        dir         - toggle direction normal/reverse
%                        log         - set axis scale to log
%                        linear      - set axis scale to linear
%
%

% Written by Colin Humphries, Salk Institute
%   April, 1998    colin@salk.edu

% Copywrite (c) 1998-2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/


function out = xaxis(in)

axh = gca;

if nargin < 1
  out = get(axh,'xlim');
  return  
end

if isstr(in)
  switch in
    case 'auto'
      set(axh,'Xlimmode','auto')
    case 'manual'
      set(axh,'XlimMode','manual')
    case 'top'
      set(axh,'Xaxislocation','top')
    case 'bottom'
      set(axh,'Xaxislocation','bottom')
    case 'log'
      set(axh,'Xscale','log')
    case 'linear'
      set(axh,'Xscale','linear')
    case 'grid'
      if strcmp(get(axh,'xgrid'),'on')
        set(axh,'Xgrid','off')
      else
	set(axh,'Xgrid','on')
      end
    case 'dir'
      if strcmp(get(axh,'xdir'),'normal')
        set(axh,'Xdir','reverse')
      else
	set(axh,'Xdir','normal')
      end      
    otherwise
      error('Invalid Parameter for xaxis')
  end
else
  if length(in) > 2
    set(axh,'xlim',[in(1) in(end)],'xtick',in)
  else
    set(axh,'xlim',in)
  end
end