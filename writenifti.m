%WRITENIFTI - Write MRI files in the NIFTI format
%
%  usage:
%      WRITENIFTI(DATA,FILENAME,VISZE,ORIENT)
%      WRITENIFTI(DATA,FILENAME,VSIZE,ORIENT,DTYPE)
%      WRITENIFTI(DATA,ISIZE,FILENAME,VSIZE,ORIENT)
%      WRITENIFTI(DATA,ISIZE,FILENAME,VSIZE,ORIENT,INDEX)
%      WRITENFITI(DATA,ISIZE,FILENAME,VSIZE,ORIENT,DTYPE)
%      WRITENIFTI(DATA,ISIZE,FILENAME,VSIZE,ORIENT,DTYPE,INDEX)
%
%  DATA     : MRI data to me written. Can be either an
%             N-dimensional matrix, a 1-d vector, or a 1-d vector
%             of indexed values.
%  ISIZE    : The dimensions of the data volume. Only specify this
%             if the DATA variable is a vector.
%  FILENAME : The name of the output file. The file format is
%             determined from the extension of the filename. If the
%             filename includes a '.gz' extension then the output
%             will be gzipped. Otherwise it will be unzipped. If
%             the filename has a '.nii' extension then it will be
%             saved as one file. If a '.hdr' or '.img' extension is
%             used then the output will be two files. If no
%             extension is specified then the output will be in
%             '.nii.gz' format. Valid extensions include: '.nii',
%             '.nii.gz', '.img', '.img.gz', and '.hdr'
%  VSIZE    : The voxel sizes for each dimension.
%  ORIENT   : The orienation of the volume. Can be one of:
%             [] - LPI orientation with the volume centered in
%                  space.
%             [ox,oy,oz] - LPI orienation with spatial offset
%                  of ox,oy, and oz. Note that the offset defines
%                  the position of the first voxel in the data
%                  array.
%             [qb,qc,qd,ox,oy,oz] - 3 element quaternion (qb,qc,qd)
%                  plus offsets. See NIFTI docs for explanation of
%                  the quaternion.
%             [qb,qc,qd,ox,oy,oz,qfac] - 3 element quaternion
%                  (qb,qc,qd), offsets, and qfactor. The qfactor
%                  defines the sign of the determinant of the
%                  rotation matrix (ie, radiological vs
%                  neurological coordinates). See NIFTI docs.
%             [3x4] - 12 element rotation matrix minus the 4th
%                  row.
%             [4x4] - Full 16 element rotation matrix.
%             Structure - Orientation structure in format returned
%                  by READNIFTI. The stucture contains the
%                  following elements:
%                     qfac    : -1 or 1
%                     qform   : 'unknown', 'scanner', 'anatomy',
%                             'talairach', 'mni'
%                     qmatrix : quaternion [qb,qc,qd,ox,oy,oz]
%                     sform   : 'unknown', 'scanner', 'anatomy',
%                             'talairach', 'mni'
%                     smatrix : 16 element rotation matrix
%  DTYPE    : Output datatype (optional). By default WRITENIFTI
%             will output the data in the same format that it is
%             stored in the matlab array. Specifying DTYPE will
%             save in a different format. DTYPE can be 'uint8',
%             'int8', 'uint16', 'int16', 'uint32', 'int32',
%             'uint64', 'int64', 'single', 'double'. Note 
%             that complex valued data, rgb data, and 128-bit
%             floating point data are not supported.
% INDEX     : Index of non-zero values. The full data volume is
%             reconstructed in the following way.
%               datafull = zeros(ISIZE);
%               datafull(INDEX) = DATA;
%

% Written by Colin Humphries
%  1/2011

% compile as mex writenifti.c -lz
%
