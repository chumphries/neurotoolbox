%YAXIS   Control y-axis appearance
%
%   usage: [A,B] = yaxis;            - get current yaxis limits
%                  yaxis([A,B])      - change axis limits
%                  yaxis auto        - change axis scaling to auto
%                        manual      - change axis scaling to manual
%                        left        - place axis at left
%                        right       - place axis at right
%                        grid        - toggle grid on/off
%                        dir         - toggle direction normal/reverse
%                        log         - set axis scale to log
%                        linear      - set axis scale to linear
%
%

% Written by Colin Humphries, Salk Institute
%   April, 1998    colin@salk.edu

% Copywrite (c) 1998-2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/


function out = yaxis(in)

axh = gca;

if nargin < 1
  out = get(axh,'ylim');
  return  
end

if isstr(in)
  switch in
    case 'auto'
      set(axh,'Ylimmode','auto')
    case 'manual'
      set(axh,'YlimMode','manual')
    case 'left'
      set(axh,'Yaxislocation','left')
    case 'right'
      set(axh,'Yaxislocation','right')
    case 'log'
      set(axh,'Yscale','log')
    case 'linear'
      set(axh,'Yscale','linear')
    case 'grid'
      if strcmp(get(axh,'Ygrid'),'on')
        set(axh,'Ygrid','off')
      else
	set(axh,'Ygrid','on')
      end
    case 'dir'
      if strcmp(get(axh,'ydir'),'normal')
        set(axh,'Ydir','reverse')
      else
	set(axh,'Ydir','normal')
      end      
    otherwise
      error('Invalid Parameter for yaxis')
  end
else
  if length(in) > 2
    set(axh,'ylim',[in(1) in(end)],'ytick',in)
  else
    set(axh,'ylim',in)
  end
end





