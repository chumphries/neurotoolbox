function [X] = fmrireadxmat(filename)
%FMRIREADXMAT - Read AFNI xmat file
%

% Copywrite (c) 2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/

fid = fopen(filename,'r');

M = 0;
N = 0;
while 1
  line = fgetl(fid);
  if ~ischar(line)
    break
  end
  if length(line) == 0
    continue
  end
  if line(1) == '#'
    continue
  end
  if N == 0
    tt = str2num(line);
    N = length(tt);
  end
  M = M + 1;
end
fseek(fid,0,-1);
X = zeros(M,N);
M = 1;
while 1
  line = fgetl(fid);
  if ~ischar(line)
    break
  end
  if length(line) == 0
    continue
  end
  if line(1) == '#'
    continue
  end
  tt = str2num(line);
  X(M,:) = tt;
  M = M + 1;
end

fclose(fid);