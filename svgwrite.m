function [] = svgwrite(filename,figh)
%SVGWRITE Output figure as an SVG file
%
% usage: svgwrite(filename)
%        svgwrite(filename,figurehandle)
% 

% Written by Colin Humphries
% chumphri@mcw.edu

% Copywrite (c) 2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/


if nargin < 2
  figh = gcf;
end

fid = fopen(filename,'w');
if fid < 0
  error('Cannot open file')
end

oldunits = get(figh,'units');
set(figh,'units','pixels');
figpos = get(figh,'position');
set(figh,'units',oldunits);

fprintf(fid,'<?xml version="1.0"?>\n');
fprintf(fid,['<svg xmlns="http://www.w3.org/2000/svg" version="1.2" ',...
             'xmlns:xlink="http://www.w3.org/1999/xlink" ',...
             'width="',num2str(figpos(3)),...
             '" height="',num2str(figpos(4)),...
             '" preserveAspectRatio="none">\n']);
fprintf(fid,'<defs>\n');
fprintf(fid,'  <style type="text/css"><![CDATA[\n');
fprintf(fid,'line {stroke: black; stroke-width: 1px;}\n');
fprintf(fid,'text {text-anchor:middle;font-family:"Arial";}\n');
fprintf(fid,'  ]]></style>\n');
fprintf(fid,'</defs>\n');
fprintf(fid,'<g>\n');

child = get(figh,'children');

for ii = 1:length(child)
  axh = child(ii);
  if strcmp(get(axh,'type'),'axes')
    fprintf(fid,['\n<!-- Axes #',num2str(ii),' -->\n']);
    oldunits = get(axh,'units');
    set(axh,'units','pixels');
    apos = get(axh,'position');
    set(axh,'units',oldunits);
    apos2 = [apos(1) figpos(4)-apos(2) ...
             apos(1)+apos(3) figpos(4)-(apos(2)+apos(4))];
    xlim = get(axh,'xlim');
    ylim = get(axh,'ylim');
    tlen = get(axh,'ticklength');
    fsize = get(axh,'fontsize');
    fname = get(axh,'fontname');
    if apos(3) > apos(4)
      tlen = tlen(1)*apos(3);
    else
      tlen = tlen(1)*apos(4);
    end
    if strcmp(get(axh,'visible'),'on')
      % background color
      if ~strcmp(get(axh,'color'),'none')
        fprintf(fid,['\n<!-- Background -->\n']);
        ctmp = get(axh,'color');
        ctmp = round(ctmp*255);
        fprintf(fid,['<rect x="',num2str(apos2(1)),...
                     '" y="',num2str(apos2(4)),...
                     '" width="',num2str(apos(3)),...
                     '" height="',num2str(apos(4)),...
                     '" style="fill:rgb(',num2str(ctmp(1)),...
                     ',',num2str(ctmp(2)),',',num2str(ctmp(3)),');" ',...
                     '/>\n']);
      end
    end
    fprintf(fid,['\n<!-- Axis Elements -->\n']);
    ch = get(axh,'children');
    for jj = length(ch):-1:1
      oh = ch(jj);
      if strcmp(get(oh,'type'),'line')
        xdata = get(oh,'xdata');
        xdata = (xdata-xlim(1))/(xlim(2)-xlim(1));
        if strcmp(get(axh,'xdir'),'reverse')
          xdata = 1-xdata;
        end
        xdata = apos(3)*xdata + apos(1);
        ydata = get(oh,'ydata');
        ydata = (ydata-ylim(1))/(ylim(2)-ylim(1));
        if strcmp(get(axh,'ydir'),'normal')
          ydata = 1-ydata;
        end
        ydata = apos(4)*ydata + apos2(4);
        ctmp = get(oh,'color');
        ctmp = round(ctmp*255);
        ctmps = ['rgb(',num2str(ctmp(1)),',',...
                 num2str(ctmp(2)),',',num2str(ctmp(3)),...
                 ')'];
        if ~strcmp(get(oh,'linestyle'),'none')
          lwidth = get(oh,'linewidth');
          if length(xdata) == 2
            fprintf(fid,['<line x1="',num2str(xdata(1)),...
                         '" x2="',num2str(xdata(2)),...
                         '" y1="',num2str(ydata(1)),...
                         '" y2="',num2str(ydata(2)),...
                         '" style="stroke-width:',num2str(lwidth),...
                         ';stroke:',ctmps,';"',...
                         ' />\n']);
          else
            fprintf(fid,'<polyline points="');
            for kk = 1:length(xdata)
              fprintf(fid,[num2str(xdata(kk)),',',...
                           num2str(ydata(kk))]);
              if kk < length(xdata)
                fprintf(fid,' ');
              end
            end
            fprintf(fid,['" style="fill:none;stroke-width:',num2str(lwidth),...
                         ';stroke:',ctmps,';"',...
                         ' />\n']);
          end
          
        end

        if ~strcmp(get(oh,'marker'),'none')
          ms = get(oh,'markersize');
          ecol = get(oh,'markeredgecolor');
          if strcmp(ecol,'auto')
            % ecol = ctmp;
            ecol = makecolorstring(ctmp);
          elseif ~ischar(ecol)              
            ecol = makecolorstring(round(ecol*255));
          end
          for kk = 1:length(xdata)
            switch get(oh,'marker')
             case 'o'
              % fprintf(fid,['<circle cx="',num2str(xdata(kk)),...
              %              '" cy="',num2str(ydata(kk)),...
              %              '" r="',num2str(ms/2),'" ',...
              %              'style="fill:none;',...
              %              'stroke:rgb(',...
              %              num2str(ecol(1)),...
              %              ',',num2str(ecol(2)),',',num2str(ecol(3)),');"',...
              %              '/>\n']);
              fprintf(fid,['<circle cx="',num2str(xdata(kk)),...
                           '" cy="',num2str(ydata(kk)),...
                           '" r="',num2str(ms/2),'" ',...
                           'style="fill:rgb(',...
                           num2str(180),...
                           ',',num2str(180),',',num2str(255),');',...
                           'stroke:',ecol,';"',...
                           '/>\n']);
             case 'square'
              fprintf(fid,['<rect x="',num2str(xdata(kk)-ms/2),...
                           '" y="',num2str(ydata(kk)-ms/2),...
                           '" width="',num2str(ms),'" ',...
                           'height="',num2str(ms),'" ',...
                           'style="fill:none;stroke:',ecol,';"',...
                           '/>\n']);
             case '+'
              fprintf(fid,['<line x1="',num2str(xdata(kk)-ms/2),...
                           '" x2="',num2str(xdata(kk)+ms/2),...
                           '" y1="',num2str(ydata(kk)),...
                           '" y2="',num2str(ydata(kk)),...
                           '" style="stroke:',ecol,';"',...
                           ' />\n']);
              fprintf(fid,['<line x1="',num2str(xdata(kk)),...
                           '" x2="',num2str(xdata(kk)),...
                           '" y1="',num2str(ydata(kk)-ms/2),...
                           '" y2="',num2str(ydata(kk)+ms/2),...
                           '" style="stroke:;',ecol,';"',...
                           ' />\n']);
             case 'x'
              fprintf(fid,['<line x1="',num2str(xdata(kk)-ms/2),...
                           '" x2="',num2str(xdata(kk)+ms/2),...
                           '" y1="',num2str(ydata(kk)-ms/2),...
                           '" y2="',num2str(ydata(kk)+ms/2),...
                           '" style="stroke:',ecol,';"',...
                           ' />\n']);
              fprintf(fid,['<line x1="',num2str(xdata(kk)+ms/2),...
                           '" x2="',num2str(xdata(kk)-ms/2),...
                           '" y1="',num2str(ydata(kk)-ms/2),...
                           '" y2="',num2str(ydata(kk)+ms/2),...
                           '" style="stroke:',ecol,';"',...
                           ' />\n']);
             case '.'
              fprintf(fid,['<circle cx="',num2str(xdata(kk)),...
                           '" cy="',num2str(ydata(kk)),...
                           '" r="',num2str(2),'" ',...
                           'style="fill:',ecol,';',...
                           'stroke:',ecol,';"',...
                           '/>\n']);
             otherwise
              error('unsupported marker type');
            end
          end
        end
      end
      if strcmp(get(oh,'type'),'contour')
        lwidth = get(oh,'linewidth');
        ctmp = get(oh,'linecolor');
        ctmps = ['rgb(',num2str(ctmp(1)),',',...
                 num2str(ctmp(2)),',',num2str(ctmp(3)),...
                 ')'];
        cmat = get(oh,'contourmatrix');
        cmpnt = 1;
        while cmpnt < size(cmat,2)
          nn = cmat(2,cmpnt);
          % keyboard
          xdata = cmat(1,(cmpnt+1):(cmpnt+nn));
          ydata = cmat(2,(cmpnt+1):(cmpnt+nn));
          xdata = (xdata-xlim(1))/(xlim(2)-xlim(1));
        if strcmp(get(axh,'xdir'),'reverse')
          xdata = 1-xdata;
        end
        xdata = apos(3)*xdata + apos(1);
        ydata = (ydata-ylim(1))/(ylim(2)-ylim(1));
        if strcmp(get(axh,'ydir'),'normal')
          ydata = 1-ydata;
        end
        ydata = apos(4)*ydata + apos2(4);
          fprintf(fid,'<polyline points="');
          for kk = 1:length(xdata)
            fprintf(fid,[num2str(xdata(kk)),',',...
                         num2str(ydata(kk))]);
            if kk < length(xdata)
              fprintf(fid,' ');
            end
          end
          fprintf(fid,['" style="fill:none;stroke-width:',num2str(lwidth),...
                       ';stroke:',ctmps,';"',...
                       ' />\n']);
          cmpnt = cmpnt + nn +1;
        end
      end
      if strcmp(get(oh,'type'),'text')
        pp = get(oh,'position');
        tstr = get(oh,'string');
        xdata = (pp(1)-xlim(1))/(xlim(2)-xlim(1));
        if strcmp(get(axh,'xdir'),'reverse')
          xdata = 1-xdata;
        end
        xdata = apos(3)*xdata + apos(1);
        ydata = (pp(2)-ylim(1))/(ylim(2)-ylim(1));
        if strcmp(get(axh,'ydir'),'normal')
          ydata = 1-ydata;
        end
        ydata = apos(4)*ydata + apos2(4);
        fsize = get(oh,'fontsize');
        tanc = 'start';
        fprintf(fid,['<text x="',...
                     num2str(xdata),...
                     '" y="',num2str(ydata),...
                     '" style="text-anchor:',tanc,';font-size:',...
                     num2str(fsize),'pt">',...
                     tstr,...
                     '</text>\n']);
      end
      if strcmp(get(oh,'type'),'image')
        % Need to add check for uint8/uint16
        xdata = get(oh,'xdata');
        xdata = (xdata-xlim(1))/(xlim(2)-xlim(1));
        if strcmp(get(axh,'xdir'),'reverse')
          xdata = 1-xdata;
        end
        xdata = apos(3)*xdata + apos(1);
        ydata = get(oh,'ydata');
        ydata = (ydata-ylim(1))/(ylim(2)-ylim(1));
        if strcmp(get(axh,'ydir'),'normal')
          ydata = 1-ydata;
        end
        ydata = apos(4)*ydata + apos2(4);
        % apos
        img = get(oh,'CData');
        if isa(img,'single')
          img = double(img);
        end
        if ydata(1) > ydata(end)
          img = flipud(img);
        end
        if xdata(1) > xdata(end)
          img = fliplr(img);
        end
        % xdata
        % sfind = strfind(filename,'.svg');
        % if ~isempty(sfind)
        %   fname = filename(1:(sfind-1));
        % else
        %   fname = filename;
        % end
        % fname = [fname,'_img',num2str(ii,'%.2d'),'.png'];
        fname = [tempname,'.png'];
        % xxd = abs(xdata(2)-xdata(1));
        % yyd = abs(ydata(2)-ydata(1));
        if length(xdata) == 1
          xxd = apos(3);
        elseif length(xdata) == 2
          xxd = abs(xdata(2)-xdata(1))/(size(img,2)-1);
        else
          xxd = abs(xdata(2)-xdata(1));
        end
        if length(ydata) == 1
          yyd = apos(4);
        elseif length(ydata) == 2
          yyd = abs(ydata(2)-ydata(1))/(size(img,1)-1);
        else
          yyd = abs(ydata(2)-ydata(1));
        end
        % keyboard
        if strcmp(get(oh,'Cdatamapping'),'direct')
          if size(img) > 2
            img = imresize(img,round([abs(ydata(end)-ydata(1))+yyd ...
                            abs(xdata(end)-xdata(1))+xxd]),'nearest',...
                           'Colormap','original');
            imwrite(img,fname,'png');
          else
            img = imresize(img,get(figh,'colormap'),...
                           round([abs(ydata(end)-ydata(1))+yyd ...
                            abs(xdata(end)-xdata(1))+xxd]),'nearest',...
                           'Colormap','original');
            imwrite(img,get(figh,'colormap'),fname,'png');
          end
        else
          clim = get(axh,'clim');
          if isa(clim,'single')
            clim = double(clim);
          end
          img = size(get(figh,'colormap'),1) * ...
                (img - clim(1))/(clim(2)-clim(1))+1;          
          img = imresize(img,get(figh,'colormap'),...
                         round([abs(ydata(end)-ydata(1))+yyd ...
                              abs(xdata(end)-xdata(1))+xxd]),'nearest',...
                         'Colormap','original');
          imwrite(img,get(figh,'colormap'),fname,'png');
        end
        ftin = fopen(fname,'r');
        ftdata = fread(ftin,inf,'uint8');
        fclose(ftin);
        
        % fprintf(fid,['<image x="',num2str(min(xdata)-xxd/2),...
        %              '" y="',num2str(min(ydata)-yyd/2),...
        %              '" width="',num2str(abs(xdata(end)-xdata(1))+xxd),...
        %              '" height="',num2str(abs(ydata(end)-ydata(1))+yyd),...
        %              '" preserveAspectRatio="none',...
        %              '" xlink:href="',fname,'"',' />\n']);
        fprintf(fid,['<image x="',num2str(min(xdata)-xxd/2),...
                     '" y="',num2str(min(ydata)-yyd/2),...
                     '" width="',num2str(abs(xdata(end)-xdata(1))+xxd),...
                     '" height="',num2str(abs(ydata(end)-ydata(1))+yyd),...
                     '" preserveAspectRatio="none',...
                     '" xlink:href="data:image/png;base64,',matlab.net.base64encode(ftdata),'"',' />\n']);
        delete(fname);
      end
    end
    if strcmp(get(axh,'visible'),'on')
      
      % x axis
      fprintf(fid,['\n<!-- X Axis -->\n']);
      if strcmp(get(axh,'xaxislocation'),'top')
        tpos1 = apos2(4);
        tpos2 = apos2(2);
        xvdir = 1;
      else
        tpos1 = apos2(2);
        tpos2 = apos2(4);
        xvdir = -1;
      end
      fprintf(fid,['<line x1="',num2str(apos2(1)),...
                   '" x2="',num2str(apos2(3)),...
                   '" y1="',num2str(tpos1),...
                   '" y2="',num2str(tpos1),...
                   '" />\n']);
      if strcmp(get(axh,'box'),'on')
        fprintf(fid,['<line x1="',num2str(apos2(1)),...
                     '" x2="',num2str(apos2(3)),...
                     '" y1="',num2str(tpos2),...
                     '" y2="',num2str(tpos2),...
                     '" />\n']);
      end
      % xaxis Tick marks and tick labels
      xt = get(axh,'xtick');      
      if ~isempty(xt)
        fprintf(fid,['\n<!-- X Tic Marks/Labels -->\n']);
        xt = (xt-xlim(1))/(xlim(2)-xlim(1));
        if strcmp(get(axh,'xdir'),'reverse')
          xt = 1-xt;
        end
        xt = apos(3)*xt + apos(1);
        xlab = get(axh,'xticklabel');
        if ischar(xlab)
          xlab = cellstr(xlab);
        end
        count = 1;
        for jj = 1:length(xt)
          fprintf(fid,['<line x1="',num2str(xt(jj)),...
                       '" x2="',num2str(xt(jj)),...
                       '" y1="',num2str(tpos1),...
                       '" y2="',num2str(tpos1+tlen*xvdir),...
                       '" />\n']);
          if strcmp(get(axh,'box'),'on')
            fprintf(fid,['<line x1="',num2str(xt(jj)),...
                       '" x2="',num2str(xt(jj)),...
                       '" y1="',num2str(tpos2),...
                       '" y2="',num2str(tpos2-tlen*xvdir),...
                       '" />\n']);
          end
          if ~isempty(xlab)
            fprintf(fid,['<text x="',num2str(xt(jj)),...
                         '" y="',num2str(tpos1+fsize/2-(fsize/2+5)*xvdir),...
                         '" style="font-size:',...
                         num2str(fsize),'pt;">',...
                         xlab{count},...
                         '</text>\n']);
            count = count + 1;
            if count > length(xlab)
              count = 1;
            end
          end
        end
      end 
      % xlabel
      xlh = get(axh,'xlabel');
      xstr = get(xlh,'string');      
      if length(xstr) > 0
        ppp = get(xlh,'position');
        ppp(1) = (ppp(1)-xlim(1))/(xlim(2)-xlim(1));
        if strcmp(get(axh,'xdir'),'reverse')
          ppp(1) = 1-ppp(1);
        end
        ppp(1) = apos(3)*ppp(1) + apos(1);
        ppp(2) = (ppp(2)-ylim(1))/(ylim(2)-ylim(1));
        if strcmp(get(axh,'ydir'),'normal')
          ppp(2) = 1-ppp(2);
        end
        ppp(2) = apos(4)*ppp(2) + apos2(4);
        fst = get(xlh,'fontsize');
        fprintf(fid,['\n<!-- X Axis Label -->\n']);
        fprintf(fid,['<text x="',num2str(ppp(1)),...
                     '" y="',num2str(ppp(2)),...
                     '" dy="12px" style="font-size:',...
                     num2str(fst),'pt;">',...
                     xstr,...
                     '</text>\n']);      
      end
      % y axis
      if strcmp(get(axh,'yaxislocation'),'left')
        tpos1 = apos2(1);
        tpos2 = apos2(3);
        xvdir = 1;
        tanc = 'end';
      else
        tpos1 = apos2(3);
        tpos2 = apos2(1);
        xvdir = -1;
        tanc = 'start';
      end
      fprintf(fid,['\n<!-- Y Axis -->\n']);
      fprintf(fid,['<line x1="',num2str(tpos1),...
                   '" x2="',num2str(tpos1),...
                   '" y1="',num2str(apos2(2)),...
                   '" y2="',num2str(apos2(4)),...
                   '" />\n']);
      if strcmp(get(axh,'box'),'on')
        fprintf(fid,['<line x1="',num2str(tpos2),...
                     '" x2="',num2str(tpos2),...
                     '" y1="',num2str(apos2(2)),...
                     '" y2="',num2str(apos2(4)),...
                     '" />\n']);
      end
      % yaxis Tick marks and tick labels
      yt = get(axh,'ytick');      
      if ~isempty(yt)
        fprintf(fid,['\n<!-- Y Tic Marks/Labels -->\n']);
        yt = (yt-ylim(1))/(ylim(2)-ylim(1));
        if strcmp(get(axh,'ydir'),'normal')
          yt = 1-yt;
        end
        yt = apos(4)*yt + apos2(4);
        ylab = get(axh,'yticklabel');
        if ischar(ylab)
          ylab = cellstr(ylab);
        end
        count = 1;
        for jj = 1:length(yt)
          fprintf(fid,['<line x1="',num2str(tpos1),...
                       '" x2="',num2str(tpos1+tlen*xvdir),...
                       '" y1="',num2str(yt(jj)),...
                       '" y2="',num2str(yt(jj)),...
                       '" />\n']);
          if strcmp(get(axh,'box'),'on')
            fprintf(fid,['<line x1="',num2str(tpos2),...
                       '" x2="',num2str(tpos2-tlen*xvdir),...
                       '" y1="',num2str(yt(jj)),...
                       '" y2="',num2str(yt(jj)),...
                       '" />\n']);
          end
          if ~isempty(ylab)
            fprintf(fid,['<text x="',...
                         num2str(tpos1-5*xvdir),...
                         '" y="',num2str(yt(jj)+fsize*.4),...
                         '" style="text-anchor:',tanc,';font-size:',...
                         num2str(fsize),'pt">',...
                         ylab{count},...
                         '</text>\n']);
            count = count + 1;
            if count > length(ylab)
              count = 1;
            end
          end
        end
      end
      % ylabel
      ylh = get(axh,'ylabel');
      ystr = get(ylh,'string');      
      if length(ystr) > 0
        ppp = get(ylh,'position');
        ppp(1) = (ppp(1)-xlim(1))/(xlim(2)-xlim(1));
        if strcmp(get(axh,'xdir'),'reverse')
          ppp(1) = 1-ppp(1);
        end
        ppp(1) = apos(3)*ppp(1) + apos(1);
        ppp(2) = (ppp(2)-ylim(1))/(ylim(2)-ylim(1));
        if strcmp(get(axh,'ydir'),'normal')
          ppp(2) = 1-ppp(2);
        end
        ppp(2) = apos(4)*ppp(2) + apos2(4);
        fst = get(ylh,'fontsize');
        fprintf(fid,['\n<!-- Y Axis Label -->\n']);
        fprintf(fid,['<text x="',num2str(ppp(1)),...
                     '" y="',num2str(ppp(2)),...
                     '" transform="rotate(-90,',num2str(ppp(1)),...
                     ',',num2str(ppp(2)),')"',...
                     ' dy="-5px" style="font-size:',...
                     num2str(fst),'pt;">',...
                     ystr,...
                     '</text>\n']);      
      end
      % title
      ylh = get(axh,'title');
      ystr = get(ylh,'string');      
      if length(ystr) > 0
        ppp = get(ylh,'position');
        ppp(1) = (ppp(1)-xlim(1))/(xlim(2)-xlim(1));
        if strcmp(get(axh,'xdir'),'reverse')
          ppp(1) = 1-ppp(1);
        end
        ppp(1) = apos(3)*ppp(1) + apos(1);
        ppp(2) = (ppp(2)-ylim(1))/(ylim(2)-ylim(1));
        if strcmp(get(axh,'ydir'),'normal')
          ppp(2) = 1-ppp(2);
        end
        ppp(2) = apos(4)*ppp(2) + apos2(4);
        fst = get(ylh,'fontsize');
        fprintf(fid,['\n<!-- Title -->\n']);
        fprintf(fid,['<text x="',num2str(ppp(1)),...
                     '" y="',num2str(ppp(2)),...
                     '" dy="-5px" style="font-size:',...
                     num2str(fst),'pt;">',...
                     ystr,...
                     '</text>\n']);      
      end

    end
    
  end
end  


fprintf(fid,'</g>\n');
fprintf(fid,'</svg>\n');
fclose(fid);



function [cstr] = makecolorstring(col)
cstr = ['rgb(',num2str(col(1)),',',num2str(col(2)),...
        ',',num2str(col(3)),')'];
