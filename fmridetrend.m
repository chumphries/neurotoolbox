function [o1,o2,o3] = fmridetrend(data,order,mv,runlength)
%FMRIDETREND   Remove mean, trend, and nuisance variables from fMRI data
%
%   usage:
%     [ndata,means,fit] = fmridetrend(data)
%                         fmridetrend(data,order)
%                         fmridetrend(data,order,motionvecs);
%                         fmridetrend(data,order,motionvecs,runlength);
%
%        data  - fmri data to be detrended (2d matrix) [time x voxels]
%        order - order of the polynomial fit (ie, 0=> mean, 1=> mean +
%                                                trend, etc)
%        motionvecs - regress out motion parameters as well [time x 6]
%                     If empty matrix [] then motion parameters are not included.
%        runlength - length of each run. Used if multiple runs are
%                    concatonated together. Each run is detrended separately.
%
%        ndata - fmri data with mean,trends, and motion effects regressed
%                out.
%        means - mean of each voxel (optional)
%        fit   - the fitted model of the mean, trend, and motion
%                parameters (optional)
%

% Written by Colin Humphries
%    11/2008

% Added output mean.  CH (2/2010)

% Copywrite (c) 2008-2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/


DEFAULT_ORDER = 1;

if nargin < 4
  runlength = [];
end
if nargin < 3
  mv = [];
end
if nargin < 2
  order = [];
end

if isempty(order)
  order = DEFAULT_ORDER;
end

o1 = zeros(size(data));
if nargout > 1
  if isempty(runlength)
    o2 = zeros(1,size(data,2));
  else
    if (size(data,1)/runlength) ~= floor(size(data,1)/runlength)
      error('Runlength does not divide into data size');
    end
    o2 = zeros(size(data,1)/runlength,size(data,2));
  end
end
if nargout > 2
  o3 = zeros(size(data));
end

if isempty(runlength)
  % Create design matrix for regression
  xx = linspace(-1,1,size(data,1));
  G = zeros(length(xx),order+1);
  for ii = 0:order
    tmp = legendre(ii,xx);
    G(:,(ii+1)) = tmp(1,:)';
  end
  if ~isempty(mv)
    G = [G mv];
  end

  Ginv = inv(G'*G);
  % keyboard
  for ii=1:size(data,2)
    x = double(data(:,ii));
    b = Ginv*(G'*x);
    o1(:,ii) = x - G*b;
    if nargout > 1
      o2(ii) = b(1);
    end
    if nargout > 2
      o3(:,ii) = G*b;
    end
  end
else
  if (size(data,1)/runlength) ~= floor(size(data,1)/runlength)
    error('Runlength does not divide into data size');
  end
  % Create design matrix for regression
  xx = linspace(-1,1,runlength);
  G0 = zeros(length(xx),order+1);
  for ii = 0:order
    % Use legendre polynomials
    tmp = legendre(ii,xx);
    G0(:,(ii+1)) = tmp(1,:)';
  end
  
  for jj = 1:(size(data,1)/runlength)
    if ~isempty(mv)
      G = [G0 mv((jj-1)*runlength+1:jj*runlength,:)];
    else
      G = G0;
    end
    Ginv = inv(G'*G);
    for ii=1:size(data,2)
      x = double(data((jj-1)*runlength+1:jj*runlength,ii));
      b = Ginv*(G'*x);
      o1((jj-1)*runlength+1:jj*runlength,ii) = x - G*b;
      if nargout > 1
        o2(jj,ii) = b(1);
      end
      if nargout > 2
        o3((jj-1)*runlength+1:jj*runlength,ii) = G*b;
      end
    end
  end
end

