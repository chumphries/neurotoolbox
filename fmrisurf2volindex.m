function [index] = fmrisurf2volindex(white,pial,sindex,target,surf1,units1,pnts1,surf2,units2,pnts2,N)
%FMRISURF2VOLINDEX  Map points on a cortical surface to volume indices
%
% [INDEX] = 
%    FMRISURF2VOLINDEX(WHITE,PIAL,SINDEX,TARGET,BASESURF,UNITS,PNTS)
%    FMRISURF2VOLINDEX(WHITE,PIAL,SINDEX,TARGET,SURF1,UNITS1,PNT1,SURF2,UNITS2,PNT2,N)
%
%   WHITE    - white matter surface
%   PIAL     - pial surface
%   SINDEX    - index of surface vertices ([] - use all vertices)
%   TARGET   - target volume (NIFTI filename or cell array
%                                             {isize,rotation matrix})
%
% Option 1: defines a set of points based on distances from either
% the WHITE or PIAL surface.
%   BASESURF - surface to use as a reference (1-white,2-pial);
%   UNITS    - 'mm' - absolute distance
%              '%'  - relative distance (percentage of the distance
%                        from the white to pial surfaces)
%   PNTS     - set of distances
%
% Option 2: defines a set of points based on a linear spacing
% between two specified points.
%   SURF1    - Surface of starting point (1 or 2)
%   UNITS1   - 'mm' or '%'
%   PNT1     - First point. Defined by the distance to SURF1.
%   SURF2    - Surface of ending point (1 or 2)
%   UNITS2   - 'mm' or '%'
%   PNT2     - End point. Defined by the distance to SURF2.
%   N        - Number of linear spaces. (Must be more than 1)
%

% Written by Colin Humphries
% 1/2011

% Copywrite (c) 2011-2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/

if ischar(target)
  if strcmp(target,'surfvol')
    R = [-1 0 0 128;0 0 1 -128;0 -1 0 128;0 0 0 1];
    isize = [256 256 256];
  else
    [isize,vsize,or] = readnifti(target,'info');
    if length(isize) > 3
      isize = isize(1:3);
      vsize = vsize(1:3);
    end
    R = or.smatrix;
  end
else
  % cell array
  isize = target{1};
  R = target{2};
end

Ri = inv(R);

if ischar(white)
  white = readsurface(white);
end
if ischar(pial)
  pial = readsurface(pial);
end

if isempty(sindex)
  sindex = 1:size(white,1);
end

if nargin < 8
  N = length(pnts1);
end


index = zeros(N,length(sindex));

norms = (pial(sindex,:)-white(sindex,:));
dist = sqrt(sum((norms.^2)')');
indtmp = find(dist > 0);
norms(indtmp,:) = norms(indtmp,:)./repmat(dist(indtmp),1,3);

if nargin < 8
  surf = surf1;
  units = units1;
  if surf == 1
    vbase = white(sindex,:);
  else
    vbase = pial(sindex,:);
  end
  pnts = pnts1;
  if strcmp(units,'mm')
    units = 1;
  else
    units = 2;
  end
  for ii = 1:N
    if units == 1
      vtmp = [vbase+norms*pnts(ii) ones(size(vbase,1),1)];
    else
      % keyboard
      vtmp = [vbase+norms.*(pnts(ii)/100).*repmat(dist,1,3) ...
              ones(size(vbase,1),1)];
    end
    vox = round(Ri*vtmp'+1);
    % keyboard
    MM = (vox(1:3,:) > repmat(isize(:),1,size(vox,2))) | ...
         (vox(1:3,:) < 1);
    if any(MM(:))
      fprintf(['Warning: some surface vertices are outside of target ' ...
               'boundaries.\nReturning zeros for those points.\n']);
      zrind = find(sum(MM) == 0);
      index(ii,zrind) = sub2ind(isize,vox(1,zrind),vox(2,zrind),...
                                vox(3,zrind));
    else
      index(ii,:) = sub2ind(isize,vox(1,:),vox(2,:),vox(3,:));
    end
  end
else
  if surf1 == 1;
    vtmp1 = white(sindex,:);
  else
    vtmp1 = pial(sindex,:);
  end
  if surf2 == 1;
    vtmp2 = white(sindex,:);
  else
    vtmp2 = pial(sindex,:);
  end
  if strcmp(units1,'mm')
    vtmp1 = vtmp1 + norms*pnts1;
  else
    vtmp1 = vtmp1 + norms.*repmat((pnts1/100)*dist,1,3);
  end
  if strcmp(units2,'mm')
    vtmp2 = vtmp2 + norms*pnts2;
  else
    vtmp2 = vtmp2 + norms.*repmat((pnts2/100)*dist,1,3);
  end
  
  vtdist = sqrt(sum(((vtmp2-vtmp1).^2)')');
  vtnorm = (vtmp2-vtmp1);
  indtmp = find(vtdist > 0);
  vtnorm(indtmp,:) = vtnorm(indtmp,:)./repmat(vtdist(indtmp),1,3);
  
  for ii = 1:N
    vtmp = [vtmp1+(ii-1)*vtnorm.*(repmat(vtdist,1,3)/(N-1)) ...
            ones(size(vtmp1,1),1)];
    vox = round(Ri*vtmp'+1);
    MM = (vox(1:3,:) > repmat(isize(:),1,size(vox,2))) | (vox(1:3,:) < 1);
    if any(MM(:))
      fprintf(['Warning: some surface vertices are outside of target ' ...
               'boundaries.\nReturning zeros for those points.\n']);
      zrind = find(sum(MM) == 0);
      index(ii,zrind) = sub2ind(isize,vox(1,zrind),vox(2,zrind),...
                                vox(3,zrind));
    else
      index(ii,:) = sub2ind(isize,vox(1,:),vox(2,:),vox(3,:));
    end
  end
  
  
end
