function [S] = fmrisinebasis(lags,num,p1)
% FMRISINEBASIS - Create sinewave basis set for fMRI analysis
%
%   usage:
%      [S] = fmrisinebasis(lags,numsines)
%      [S] = fmrisinebasis(lags,numsines,arg)
%
%         lags     - length in tr's of basis vectors
%         numsines - number of sinewaves
%         arg      - 'base' add in baseline term
%                    'trend' add in baseline and linear trend terms
%

% Written by Colin Humphries
%    Medical College of Wisconsin
%    11/2007

% Copywrite (c) 2007-2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/


if nargin < 3
  p1 = 'none';
end

xx = linspace(0,1,lags);

S = repmat(xx',1,num);

S = S*diag((1:num)*pi);
S
S = sin(S);

switch p1
 case 'base'
  S = [ones(size(S,1),1) S];
 case 'trend'
  S = [ones(size(S,1),1) xx' S];
  
end