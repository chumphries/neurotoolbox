#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "mex.h"

#define DATA_IN      prhs[0]
#define ARG2_IN      prhs[1]
/* #define ARG3_IN      prhs[2] */

#define CLUST_OUT    plhs[0]
#define CSIZE_OUT    plhs[1]

/*

fmricluster(data)
fmricluster(data,level)
fmricluster(data,vsize,dist) *not implemented yet.

 */

void cluster_nd_matrix(double *, const int *, int, int);
void getneighbors(int, const mwSize *, int, int *, int, int *);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{

  int numdim, ii, jj, numclust, numpnts;
  const mwSize *isize;
  void *p_data;
  int isdouble, level;
  double dval, *p_clust, *p_size;
  int *buffer, bind1, bind2, *nlist, *tmpbuf;

  if (nrhs < 1) {
    mexErrMsgTxt("Must include at least one input");
  }

  p_data = mxGetData(DATA_IN);
  if (mxIsDouble(DATA_IN)) {
    isdouble = 1;
  }
  else if (mxIsLogical(DATA_IN)) {
    isdouble = 0;
  }
  else {
    mexErrMsgTxt("Data must be double precision or logical type");
  }

  numdim = mxGetNumberOfDimensions(DATA_IN);
  isize = mxGetDimensions(DATA_IN);

  if (nrhs > 1) {
    if (!mxIsDouble(ARG2_IN)) {
      mexErrMsgTxt("arg 2 must be double precision");
    }
    level = (int)mxGetScalar(ARG2_IN);
    if (level > numdim || level < 1) {
      mexErrMsgTxt("level must be less than or equal to number of dimensions");
    }
  }
  else {
    level = numdim;
  }

  CLUST_OUT = mxCreateNumericArray(numdim,isize,mxDOUBLE_CLASS,mxREAL);
  p_clust = mxGetPr(CLUST_OUT);
  
  /* Populate the CLUST_OUT array. If there is a nonzero value in data then
     assign a value of -1 to CLUST_OUT array. Otherwise 0. 
     Data should be either double or logical precision.  */
  numpnts = 0;
  if (isdouble) {
    for (ii=0; ii<mxGetNumberOfElements(CLUST_OUT); ++ii) {
      if (((double *)p_data)[ii]) {
	p_clust[ii] = -1;
	++numpnts;
      }
      else {
	p_clust[ii] = 0;
      }
    }
  }
  else {
    for (ii=0; ii<mxGetNumberOfElements(CLUST_OUT); ++ii) {
      if (((mxLogical *)p_data)[ii]) {
	p_clust[ii] = -1;
	++numpnts;
      }
      else {
	p_clust[ii] = 0;
      }
    }
  }
  
  cluster_nd_matrix(p_clust,(int *)isize,numdim,level);

  if (nlhs > 1) {
    numclust = 0;
    for (ii=0; ii<mxGetNumberOfElements(CLUST_OUT); ++ii) {
      if (p_clust[ii] > numclust) {
	numclust = p_clust[ii];
      }
    }
    CSIZE_OUT = mxCreateNumericMatrix(1,numclust,
                                      mxDOUBLE_CLASS,mxREAL);
    p_size = mxGetPr(CSIZE_OUT);
    for (ii=0; ii<mxGetNumberOfElements(CSIZE_OUT); ++ii) {
      p_size[ii] = 0;
    }
    for (ii=0; ii<mxGetNumberOfElements(CLUST_OUT); ++ii) {
      if (p_clust[ii]) {
	p_size[(int)(p_clust[ii]-1)] += 1;
      }
    }

  }

  

}

void cluster_nd_matrix(double *p_clust, const int *isize, int numdim, int level)
{
  /* This function does the clustering. The input should be an 
     n-dimensional matrix of 0s and -1s. -1s refer to points where
     there is activation.
     
   */

  int ii, jj, numelements = 1, bind1, bind2, clustind;
  int *nlist, *tmpbuf, *buffer;

  for (ii=0; ii<numdim; ++ii) {
    numelements = numelements * isize[ii];
  }

  /* Allocate tempory storage. */
  nlist = (int *)mxCalloc(pow(3,numdim),sizeof(int));
  tmpbuf = (int *)mxCalloc(numdim*2,sizeof(int));
  buffer = (int *)mxCalloc(numelements+1,sizeof(int));

  clustind = 1;
  for (ii=0; ii<numelements; ++ii) {
    if (p_clust[ii] == -1) {
      p_clust[ii] = (double)clustind;
      getneighbors(ii,isize,numdim,nlist,level,tmpbuf);
      bind1 = 0;
      bind2 = 0;
      for (jj=0; jj<pow(3,numdim); ++jj) {
	if (nlist[jj] < 0) {
	  break;
	}
	else {
	  if (p_clust[nlist[jj]] == -1) {
	    p_clust[nlist[jj]] = (double)clustind;
	    buffer[bind2] = nlist[jj];
	    ++bind2;
	  }
	}
      }
      while (bind1 < bind2) {
	getneighbors(buffer[bind1],isize,numdim,nlist,level,tmpbuf);
	for (jj=0; jj<pow(3,numdim); ++jj) {
	  if (nlist[jj] < 0) {
	    break;
	  }
	  else {
	    if (p_clust[nlist[jj]] == -1) {
	      p_clust[nlist[jj]] = (double)clustind;
	      buffer[bind2] = nlist[jj];
	      ++bind2;
	    }
	  }
	}
	++bind1;
      }
      ++clustind;
    }
  }

  mxFree(nlist);
  mxFree(tmpbuf);
  mxFree(buffer);  



}

void getneighbors(int ind, const mwSize *isize, int numdim, int *narray, int level, int *tmpbuf)
{
  /* This function finds the neighbors of the index ind and returns them
     in a array (narray). level is based on the number of neighbors and should
     always be less than or equal to the number of dimensions in the array.
     ind - index into array
     isize - size of each dimension in the array
     numdim - number of dimensions
     narray - return array (must be 3^numdim in length)
     level - 1,2,3,... 
       Note: level one only finds neighbors along 1 dimension. For 2-d data 
       this would be 4 neighbors, 3-d 6 neighbors. level two returns level 
       one plus diagonals along 2 dimensions. Level three - diagonals along 
       3 dimensions, etc.
     tmpbuf - temporary array. Must be 2*numdim in size.
*/
  int N, ii, jj, tt1, tt2, aind;
  int inbound, levelnum;

  N = pow(3,numdim);
  /* mexPrintf("ind = %d\n",ind); */
  /* The following code converts the variable 'ind' which is an index
     into the entire array, into x,y,z,... indices. */
  tmpbuf[0+numdim] = 1;
  for (ii=1; ii<numdim; ++ii) {
    tmpbuf[ii+numdim] = tmpbuf[ii-1+numdim]*isize[ii-1];
  }
  tt1 = ind;
  for (ii=(numdim-1); ii >= 0; --ii) {
    tt2 = ind % tmpbuf[ii+numdim];
    tmpbuf[ii] = (tt1 - tt2)/tmpbuf[ii+numdim];
    tt1 = tt2;
  }
  aind = 0;
  for (ii=0; ii<N; ++ii) {
    inbound = 1;
    levelnum = 0;
    tt2 = 0;
    for (jj=0; jj<numdim; ++jj) {
      tt1 = ((int)floor((double)ii/pow(3,jj)) % 3)-1;
      if (tt1) {
	++levelnum;
      }
      tt1 = tt1 + tmpbuf[jj];
      if (tt1 < 0 || tt1 >= isize[jj] ) {
	inbound = 0;
      }
      tt2 = tt2 + tt1*tmpbuf[jj+numdim];
    }
    if (inbound && (levelnum > 0) && (levelnum <= level) ) {
      narray[aind] = tt2;
      ++aind;
    }
  }
  narray[aind] = -1;

}
