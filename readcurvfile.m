function [vals] = readcurvfile(filename)
% READCURVFILE  Read FreeSurfer binary curv/thickness file
%
%  usage:
%         [vals] = readcurvfile(filename);
%
%

% written by Colin Humphries
%   Medical College of Wisconsin
%    6/2006


% Note: apparently freesurfer uses big endian formating, which I believe
% could be different than the native machine format?

% Copywrite (c) 2006-2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/

fid = fopen(filename,'r','b');
if fid < 0
  error('Cannot open file');
end

fseek(fid,3,'bof');

numverts = fread(fid,1,'int32');
numfaces = fread(fid,1,'int32');
numcols = fread(fid,1,'int32');

vals = fread(fid,numverts,'float32');

fclose(fid);
