/*
  nearestpoint.c

%NEARESTPOINT   Find the nearest point to a reference point 
%   NEARESTPOINT(X,Y) finds the point in matrix Y that is closest in space
%   to the point X. Each row of Y is a separate point. If X is a a matrix
%   then the function returns a value for each row of X. The number of
%   columns for X and Y must be equal.

% Written by Colin Humphries
%   Feb, 2010

mex trackdistance.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"
*/

#include <stdio.h>
#include <stdlib.h>
#include "mex.h"

#define A_IN        prhs[0]
#define B_IN        prhs[1]
#define INDEX_OUT   plhs[0]

void swap(int *A, int *B)
{
  int C = *B;
  *B = *A;
  *A = C;
}

void makekdtreeindex(int *pindex, int iostart, int ioend,
		     const double *pX, int M, int N, int depth)
{
  int ipos, ii;
  int istart = iostart;
  int iend = ioend;

  if ((iend-istart + 1) < 2) {
    return;
  }

  if ((iend-istart + 1) == 2 ) {
    if (pX[pindex[istart]+depth*M] > pX[pindex[iend]+depth*M]) {
      swap(pindex+istart,pindex+iend);
    }
    return;
  }

  int median = (istart+iend)/2;
  int pivotpos = median;

  swap(pindex+pivotpos,pindex+iend);

  while(true) {
    ipos = istart;
    for (ii=istart; ii<= iend; ++ii) {
      if (pX[pindex[ii]+depth*M] < pX[pindex[iend]+depth*M]) {
	swap(pindex+ipos,pindex+ii);
	++ipos;
      }
    }
    swap(pindex+ipos,pindex+iend);
    if (ipos > median) {
      iend = ipos-1;
    }
    else if (ipos < median) {
      istart = ipos+1;
    }
    else {
      break;
    }
  }
  ++depth;
  if (depth == N) {
    depth = 0;
  }

  makekdtreeindex(pindex,iostart,ipos-1,pX,M,N,depth);
  makekdtreeindex(pindex,ipos+1,ioend,pX,M,N,depth);

}

double distp(const double *point, int PM, const double *pX, int N, int M, int K)
{
  int ii;
  double dist = 0;
  for (ii=0; ii<N; ++ii) {
    dist += (pX[K+ii*M] - *(point+ii*PM)) * (pX[K+ii*M] - *(point+ii*PM));
  }
  return dist;
}

int npoint(double *pdist, const double *point, int PM, const int *ptree, 
	    int iostart, int ioend, const double *pX,
	    int M, int N, int depth)
{
  int inda, indb;
  double dista, distb;
  int median = (iostart+ioend)/2;
  int ndepth = depth;
  ++ndepth;
  if (ndepth == N) {
    ndepth = 0;
  }
  /* mexPrintf("start st = %d en = %d md = %d\n",iostart,ioend,median); */
  if ((ioend-iostart + 1) < 2) {
    inda = ptree[ioend];
    dista = distp(point,PM,pX,N,M,inda);
  }
  else if ((ioend-iostart+1) == 2) {
    dista = distp(point,PM,pX,N,M,ptree[iostart]);
    distb = distp(point,PM,pX,N,M,ptree[ioend]);
    if (dista < distb) {
      inda = ptree[iostart];
    }
    else {
      dista = distb;
      inda = ptree[ioend];
    }
  }
  else {
    /* if (point[depth] < pX[median+depth*M]) { */
    if (*(point+depth*PM) < pX[median+depth*M]) {
      inda = npoint(&dista,point,PM,ptree,iostart,median-1,pX,M,N,ndepth);

      if (((*(point+depth*PM) - pX[ptree[median]+depth*M]) * 
	   (*(point+depth*PM) - pX[ptree[median]+depth*M])) < dista) {
	indb = npoint(&distb,point,PM,ptree,median+1,ioend,pX,M,N,ndepth);
	if (distb < dista) {
	  inda = indb;
	  dista = distb;
	}
	distb = distp(point,PM,pX,N,M,ptree[median]);
	if (distb < dista) {
	  inda = ptree[median];
	  dista = distb;
	}
      }
    }
    else {
      inda = npoint(&dista,point,PM,ptree,median+1,ioend,pX,M,N,ndepth);
      if (((*(point+depth*PM) - pX[ptree[median]+depth*M]) * 
	   (*(point+depth*PM) - pX[ptree[median]+depth*M])) < dista) {
	indb = npoint(&distb,point,PM,ptree,iostart,median-1,pX,M,N,ndepth);
	if (distb < dista) {
	  inda = indb;
	  dista = distb;
	}
	distb = distp(point,PM,pX,N,M,ptree[median]);
	if (distb < dista) {
	  inda = ptree[median];
	  dista = distb;
	}
      }
    }
  }
  *pdist = dista;
  /* mexPrintf("end ind = %d dist = %g st = %d en = %d\n",inda,*pdist,iostart,ioend);  */
  return inda;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  int numdim, sizeA, sizeB, ii, jj;
  double *pA, *pB, *pIndex, dist, *pPoint;
  int *pTree;

  /* Check for proper number of arguments. */
  if (nrhs < 2) {
    mexErrMsgTxt("Not enough inputs.");
  }

  sizeA = mxGetM(A_IN);
  numdim = mxGetN(A_IN);
  
  sizeB = mxGetM(B_IN);
  if (numdim != mxGetN(B_IN)) {
    mexErrMsgTxt("Number of columns must be equal for both matrices.");
  }
  
  if (!mxIsDouble(A_IN) || !mxIsDouble(A_IN)) {
    mexErrMsgTxt("Inputs must be double matrices");
  }
  
  INDEX_OUT = mxCreateDoubleMatrix(sizeA, 1, mxREAL);

  pA = mxGetPr(A_IN);
  pB = mxGetPr(B_IN);
  pIndex = mxGetPr(INDEX_OUT);

  pTree = (int *)mxCalloc(sizeB,sizeof(int));
  for (ii=0; ii<sizeB; ++ii) {
    pTree[ii] = ii;
  }
  

  makekdtreeindex(pTree,0,sizeB-1,pB,sizeB,numdim,0);

#pragma omp parallel for
  for (ii=0; ii<sizeA; ++ii) {
    /*
    for (jj=0; jj<numdim; ++jj) {
      pPoint[jj] = pA[ii+jj*sizeA];
    }
    */
    pIndex[ii] = (double)(npoint(&dist,pA+ii,sizeA,pTree,0,sizeB-1,
				 pB,sizeB,numdim,0)+1);
  }

  
  mxFree(pTree);
  
}


