function [X] = generatetone(F,fs,len,ramplen)
%GENERATETONE Create an audio file with multiple tones
%
%   [X] = generatetone(Freq,Fs,length,ramplength)
%        Freq   f or [f1 f2 ...] or {[f1 f2],[f1 f2],...}
%        Fs     samplerate
%        length T or [Tsb T Tse] Tsb is silence at beg
%                                Tse is silence at end
%               {T,T,T,...} corresponding to Freq cell array
%        ramplength length of onset and offset ramp
%   All times are in seconds
%

% Written by Colin Humphries
%     5/2013

% Copywrite (c) 2013-2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/



if nargin < 4
  ramplen = [];
end
if nargin < 3
  len = [];
end
if nargin < 2
  fs = [];
end
if ramplen == 0
  ramplen = [];
end
if isempty(len)
  len = 1;
end
if isempty(fs)
  fs = 44100;
end

if ~isempty(ramplen)
  ramplen = sin(linspace(-pi/2,pi/2,round(ramplen*fs)))/2+.5;
end

if iscell(len)
  N = 0;
  for ii=1:length(len)
    N = N + sum(len{ii});
  end
else
  N = sum(len);
end

N = round(N*fs);

X = zeros(1,N);

if iscell(len) || iscell(F)
  if iscell(len)
    trials = length(len);
  end
  if iscell(F)
    if iscell(len)
      if trials ~= length(F)
        error('Number of trials in Freq and length differ');
      end
    else
      trials = length(F);
    end
  end
  tcount = 0;
  for ii = 1:trials
    if iscell(len)
      if iscell(F)
        if isempty(F{ii})
          xx = zeros(1,len{ii}*fs);
        else
          xx = getsinewave(F{ii},len{ii},fs,ramplen);
        end
      else
        xx = getsinewave(F,len{ii},fs,ramplen);
      end
      TT = sum(len{ii});
    else
      if isempty(F{ii})
        xx = zeros(1,round(len*fs));
      else
        xx = getsinewave(F{ii},len,fs,ramplen);
      end
      TT = sum(len);
    end
    count = round(tcount*fs)+1;
    X(count:(count+length(xx)-1)) = xx;
    tcount = tcount + TT;
  end
  
  
else
  X = getsinewave(F,len,fs,ramplen);
  
end



function [X] = getsinewave(F,len,fs,ramplen)

aweight = 0;

if length(len) == 1
  tt = 1/fs:1/fs:len;
  X = zeros(size(tt));
  for ii=1:length(F)
    % X = X + sin(2*pi*F(ii)*tt);
    if aweight
      X = X + .5*sin(2*pi*F(ii)*tt) / ...
          (10^(spline([125 250 500 1000 2000 4000 8000],...
                      [-16.1 -8.6 -3.2 0 1.2 1 -1.1],F(ii))/20));
    else
      X = X + .5*sin(2*pi*F(ii)*tt);
    end
  end
  if ~isempty(ramplen)
    % keyboard
    X(1:length(ramplen)) = X(1:length(ramplen)) .* ...
        ramplen;
    % keyboard
    X(end-length(ramplen)+1:end) = ...
        X(end-length(ramplen)+1:end) .* fliplr(ramplen);
  end
else
  tt = 1/fs:1/fs:len(2);
  X = zeros(size(tt));
  for ii=1:length(F)
    % X = X + sin(2*pi*F(ii)*tt);
    if aweight
      X = X + .5*sin(2*pi*F(ii)*tt) / ...
          (10^(spline([125 250 500 1000 2000 4000 8000],...
                      [-16.1 -8.6 -3.2 0 1.2 1 -1.1],F(ii))/20));
    else
      X = X + .5*sin(2*pi*F(ii)*tt);    
    end
  end
  if ~isempty(ramplen)
    X(1:length(ramplen)) = X(1:length(ramplen)) .* ...
        ramplen;
    X(end-length(ramplen)+1:end) = ...
        X(end-length(ramplen)+1:end) .* fliplr(ramplen);
  end
  X = [zeros(1,round(len(1)*fs)) X zeros(1,round(len(3)*fs))];
  
end

