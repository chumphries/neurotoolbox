# NeuroToolbox - Matlab scripts for processing fMRI, EEG, and MEG data

## Introduction

This toolbox is a collection of scripts I have written over the years
to process different types of neuroimaging data. It is not intended to
be a complete analysis package like AFNI, FSL, or SPM. Rather, I wrote
these scripts to allow me to access different types of file formats from Matlab
and run specialized analyses. Many of the scripts in this toolbox are
unfinished and may need modification before use.

## Installation

After downloading the project archive or cloning it with git, simply
add the project directory to your Matlab path, see https://www.mathworks.com/help/matlab/matlab_env/add-folders-to-matlab-search-path-at-startup.html

Some of the scripts are MEX files and need to be compiled.

### Linux

## User Guide

[Command Reference](doc/reference.md)

