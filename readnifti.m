%READNIFTI   Read MRI files in the NIFTI format
%  DATA = READNIFTI(FILENAME) reads the MRI volume from the NIFTI
%  file called FILENAME. Both unzipped (.nii) and gzipped (.nii.gz)
%  files are supported.
%
%  [DATA,VSIZE] = READNIFTI(FILENAME) also loads the voxel sizes
%  into a vector VSIZE.
%
%  [DATA,VSIZE,ORIENT] = READNIFTI(FILENAME) returns the
%  orientation information in the structure ORIENT. The structure
%  has the following elements:
%       qfac    : 1 or -1
%       qform   : 'unknown', 'scanner', 'anatomy', 'talairach', 'mni'
%       qmatrix : quaternion [qb,qc,qd,ox,oy,oz]
%       sform   : 'unknown', 'scanner', 'anatomy', 'talairach', 'mni'
%       smatrix : 4x4 rotation matrix
%
%  [DATA,VSIZE,ORIENT,DTYPE] = READNIFTI(FILENAME) returns the
%  datatype (i.e., 'uint8') of the file.
%
%  Use DATA = READNIFTI(FILENAME,VOLNUM) to load a single volume
%  from a 4-d time series.
%
%  [ISIZE,VSIZE,ORIENT,DTYPE] = READNIFTI(FILENAME,'info') will
%  load information about the volume without loading the data.

% compile as mex readnifti.c -lz
%
