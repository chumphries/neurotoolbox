function [G] = fmridesign(list,bsize,ssize,tr,delay,pad,rsize,hrffun)
% FMRIDESIGN - Create design matrix for fmri regression
%
%   usage:
%      [G] = fmridesign(list,tsize,ssize,tr,delay,pad,rsize)
%
%      list   - List of stimuli (vector/matrix)
%      tsize  - trial lengths (secs) (scalar or vector/matrix)
%      ssize  - stimulus lengths (secs) (scalar or vector/matrix)
%      tr     - TR (secs)
%      delay  - shift (secs) amount to shift timecourse 
%      pad    - add or subtract zeros at the beg and end of each run
%               [beg,end]
%      rsize  - size in trials of each run (vector)
%
%          If list,tsize, or size are matrices then each row is treated
%          as a separate run and separate regressors are used for each
%          run.
%          
%          The variable rsize can be used for runs with different sizes.
%
%     This script generates a design matrix for analyzing fmri data. It
%     creates a time course vector for each condition/run and then
%     convolves it with a standard spm-style hrf function. It subsamples
%     the time course so the regressors can be shifted by small amounts
%     using the delay variable.
%
%   examples:
%      block design (1 condition, 1 run):
%        G = fmridesign([1 1 1 1 1],30,15,1,0,[0 0]);
%      event design (2 conditions, 2 runs):
%        G = fmridesign([1 2 1; 2 1 2],15,1,1,0,[0 0]);
%      event jittered design (1 condition, 2 runs)
%        G = fmridesign([1 1 1 1 1 1],[12 10 8 10 8 8],1,2,0,[0 0],[3 3]);
%      
%   External requirements: signal processing toolbox
%

% written by Colin Humphries
% chumphri@mcw.edu

% 10/30/02 Modified to accept different block times
% also changed format for list. Now expects each run to be on a separate
% row in the matrix.
% 10/26/03 Complete over-haul
%          runs no longer have to be on separate rows
%          can now accept different s sizes
%          can now accept negative delays
%          can now have different delays for each run
%          added pad variable
%          rsize is now in images, not seconds

% Note: uses the resample routine in the signal processing toolbox.

% Sub-sampling factor
SCALE_FACTOR = 10;

% Process command line imputs
if nargin < 8
  hrffun = [];
end
if nargin < 7
  rsize = [];
end
if nargin < 6
  pad = [0 0];
end
if nargin < 5
  delay = 0;
end
if all(size(list) > 1)
  rsize = repmat(size(list,2),1,size(list,1));
  list = list';
  list = list(:);
else
  if isempty(rsize)
    rsize = length(list);
  end
end
if length(bsize(:)) == 1
  bsize = bsize*ones(size(list));
else
  bsize = bsize';
  bsize = bsize(:);
end
if length(ssize(:)) == 1
  ssize = ssize*ones(size(list));
else
  ssize = ssize';
  ssize = ssize(:);
end

if length(delay) == 1
  delay = repmat(delay,1,length(rsize));
end

cond = unique(list(:));
numcond = length(cond);

% Allocate matrix
G = zeros(sum(bsize)/tr+sum(pad)*length(rsize),(numcond+1)*length(rsize));

bsize = bsize*SCALE_FACTOR;
ssize = ssize*SCALE_FACTOR;
delay = delay*SCALE_FACTOR;

% Hemodynamic response function
if isempty(hrffun)
  HRF = hrf(.1);
else
  HRF = hrffun;
end

for ii = 1:numcond
  iind = list == ii;
  for jj = 1:length(rsize)
    tmp = zeros(sum(bsize(sum(rsize(1:(jj-1)))+1:sum(rsize(1:(jj))))),1);
    for kk = 1:rsize(jj)
      curindex = kk+sum(rsize(1:(jj-1)));
      if iind(curindex)
	tmpind = sum(bsize((sum(rsize(1:(jj-1)))+1):(curindex-1)))+1;
	tmp(tmpind:tmpind+ssize(curindex)) = ones;
      end
    end
    tmp2 = conv(tmp,HRF);
    if delay(jj) < 0
      tmp2 = tmp2(round(abs(delay(jj))):end);
    elseif delay(jj) > 0
      tmp2 = [zeros(round(delay(jj)),1);tmp2];
    end
    % Each regressor is calculated at a finer sample rate determined by
    % the SCALE_FACTOR. The regressor is then resampled back to the
    % normal time scale.
    tmp3 = resample(tmp2(1:length(tmp)),1,tr*SCALE_FACTOR);
    if pad(1) > 0
      tmp3 = [zeros(pad(1),1);tmp3];
    elseif pad(1) < 0
      tmp3 = tmp3(1+abs(pad(1)):end);
    end
    if pad(2) > 0
      tmp3 = [tmp3;zeros(pad(2),1)];
    elseif pad(2) < 0
      tmp3 = tmp3(1:(end+pad(2)));
    end
    G(sum(bsize(1:(sum(rsize(1:(jj-1)))+1)-1))/SCALE_FACTOR/tr+1+(jj-1)*sum(pad): ...
      sum(bsize(1:(sum(rsize(1:(jj)))+1)-1))/SCALE_FACTOR/tr+jj*sum(pad),(ii-1)* ...
      length(rsize)+jj) = tmp3;
  end
end

% Add column(s) of ones for the intercepts.
for jj = 1:length(rsize)
  G(sum(bsize(1:(sum(rsize(1:(jj-1)))+1)-1))/SCALE_FACTOR/tr+1+(jj-1)*sum(pad): ...
    sum(bsize(1:(sum(rsize(1:(jj)))+1)-1))/SCALE_FACTOR/tr+jj*sum(pad), ...
    numcond*length(rsize)+jj)= ones;
end
