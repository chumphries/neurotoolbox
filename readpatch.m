function [index,pos] = readpatch(filename)
% READPATCH - Read Freesurfer patch file
%
%  usage: index = readpatch(filename);
%

% Written by Colin Humphries
%      11/2008

% 10/2010 - Apparently, the patch file is now one-indexed instead of
% zero-indexed???

% Copywrite (c) 2008-2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/

fid = fopen(filename,'r','b');
if fid < 0
  error('Cannot open file');
end

fseek(fid,4,'bof');

numverts = fread(fid,1,'int32');

datatmp = fread(fid,inf,'int32');
fseek(fid,8,'bof');
datatmp2 = fread(fid,inf,'single');
fclose(fid);

if length(datatmp) ~= numverts*4
  error('Error reading file');
end

% Note: each entry in the patch file has a vertex number (int32) and then
% the x,y,z positions (float32). Currently, I'm getting rid of the
% positions. Also, the vertex number is either positive or negative
% depending on whether it is at a boundary or not. Currently, this script
% returns everything as positive.

datatmp = reshape(datatmp,4,numverts);
% index = abs(datatmp(1,:))+1;
index = abs(datatmp(1,:))';
datatmp2 = reshape(datatmp2,4,numverts);
pos = datatmp2(2:4,:)';
% ind = find(datatmp(1,:) < 0);
% pos(ind,:) = -1*pos(ind,:);