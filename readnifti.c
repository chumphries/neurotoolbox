/*
readnifti

Read MRI files in the NIFTI format

%      [data,vsize,orient,dtype] = readnifti(filename);
%      [data,vsize,orient,dtype] = readnifti(filename,volumenum);
%      [data,vsize,orient,dtype] = readnifti(filename,datatype);
%      [data,vsize,orient,dtype] = readnifti(filename,volumenum,datatype);

%      [isize,vsize,orient,dtype] = readnifti(filename,'info');
%
%        filename - is the name of a file in NIFTI format. 
%                   (filename can include the .nii, .hdr, or .img
%                   extensions or just the base filename. If base
%                   filename is used then the file is assumed to have
%                   the .nii extension.)
%       volumenum - loads a specific volume from a 4D dataset
%
%            data - is a n-dimensional matrix of the data
%          pixdim - is the size of each voxel (mm)
%          orient - is a structure containing the qform and sform
%                   matrices.
%           dtype - is the datatype of the file (ie 'uint8', 'int16', etc)
%


Written by Colin Humphries

orient structure:
orient.qfac [1]
orient.qform string
orient.qmatrix [1x6]
orient.sform string
orient.smatrix [4x4]

Note: the datatype flag is not implemented yet.

Changes

6/2011 - Added support for reading .img/.hdr NIFTI files.

 */

#define USE_ZLIB

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "mex.h"
#include "nifti1.h"

#ifdef USE_ZLIB
#include "zlib.h"
#endif

/* Input Arguments */

#define FILENAME    prhs[0]
#define ARG2_IN     prhs[1]
#define ARG3_IN     prhs[2]
#define DATA_OUT    plhs[0]
#define VSIZE_OUT   plhs[1]
#define ORIENT_OUT  plhs[2]
#define DTYPE_OUT   plhs[3]


void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{

  int buflen, status, *dimensions, ii, numelements, numdims, dsize;
  double *dataptr;
  double *volnum = NULL;
  char *filename, *fileprefix;
  char *strtmp = NULL;
  nifti_1_header *nheader;
#ifdef USE_ZLIB
  gzFile fgzin;
#endif
  void *buffer;
  mxArray *mqfac, *mqform, *mqmatrix, *msform, *msmatrix;
  const char **sfnames;

  sfnames = (const char **)mxCalloc(sizeof(char**),5);
  sfnames[0] = "qfac";
  sfnames[1] = "qform";
  sfnames[2] = "qmatrix";
  sfnames[3] = "sform";
  sfnames[4] = "smatrix";


  /* Check for proper number of arguments. */
  if (nrhs < 1) {
    mexErrMsgTxt("Not enough inputs.");
  }

  /* Check for string. */
  if (mxIsChar(FILENAME) != 1) {
    mexErrMsgTxt("Filename must be a string.");
  }

  if (nlhs  < 1) {
    mexErrMsgTxt("Must have at least one output argument");
  }

  /* Get the length of the input string. */
  buflen = (mxGetM(FILENAME) * mxGetN(FILENAME)) + 1;

  /* Allocate memory for input and output strings. */
  filename = mxCalloc(buflen, sizeof(char));

  /* Copy the string data */
  status = mxGetString(FILENAME, filename, buflen);
  if (status != 0) {
    mexWarnMsgTxt("Not enough space. String is truncated.");
  }

#ifdef USE_ZLIB
  fgzin = gzopen(filename,"rb");
  if (!fgzin) {
    mexErrMsgTxt("Could not open file.");
  }
#else
  FILE *fin;
  fin = fopen(filename,"rb");
  if (!fin) {
    mexErrMsgTxt("Could not open file.");
  }
#endif

  nheader = (nifti_1_header *)mxMalloc(sizeof(nifti_1_header));

#ifdef USE_ZLIB
  gzread(fgzin,(char *)nheader,sizeof(nifti_1_header));
#else
  fread((char *)nheader,sizeof(nifti_1_header),1,fin);
#endif

  /* mexPrintf("dim %g %d %d %d\n",nheader->qoffset_x, nheader->dim[0],nheader->dim[1],nheader->dim[2]); */

  if ((nheader->sizeof_hdr != 348) || (nheader->magic[0] != 'n')) {
    /* mexErrMsgTxt("Incorrect header information."); */
  }


  if (nheader->dim[0] > 10) {
    mexErrMsgTxt("File is probably byte swapped.");
  }

  if (nrhs > 1) {
    if (mxIsChar(ARG2_IN)) {
      /* strtmp = mxGetChars(ARG2_IN); */
      strtmp = mxArrayToString(ARG2_IN);
      /* printf(strtmp); */
      if (strcmp(strtmp,"info") == 0) {
	/* Just return information about the volume */
	DATA_OUT = mxCreateDoubleMatrix(1,nheader->dim[0],mxREAL);
	for (ii=0; ii<nheader->dim[0]; ++ii) {
	  dataptr = mxGetPr(DATA_OUT);
	  dataptr[ii] = nheader->dim[ii+1];
	}
	if (nlhs  > 1) {
	  VSIZE_OUT = mxCreateDoubleMatrix(1,nheader->dim[0],mxREAL);
	  for (ii=0; ii<nheader->dim[0]; ++ii) {
	    dataptr = mxGetPr(VSIZE_OUT);
	    dataptr[ii] = nheader->pixdim[ii+1];
	  }
	}
	if (nlhs > 2) {
	  ORIENT_OUT = mxCreateStructMatrix(1,1,5,sfnames);
	  mqfac = mxCreateDoubleMatrix(1,1,mxREAL);
	  mxGetPr(mqfac)[0] = nheader->pixdim[0];
	  mxSetField(ORIENT_OUT,0,"qfac",mqfac);
	  if (nheader->qform_code == 0) {
	    mqform = mxCreateString("unknown");
	  }
	  else if (nheader->qform_code == 1) {
	    mqform = mxCreateString("scanner");
	  }
	  else if (nheader->qform_code == 2) {
	    mqform = mxCreateString("anatomy");
	  }
	  else if (nheader->qform_code == 3) {
	    mqform = mxCreateString("talairach");
	  }
	  else if (nheader->qform_code == 4) {
	    mqform = mxCreateString("mni");
	  }
	  else {
	    mqform = mxCreateDoubleMatrix(1,1,mxREAL);
	    mxGetPr(mqform)[0] = nheader->qform_code;
	  }
	  mxSetField(ORIENT_OUT,0,"qform",mqform);
	  
	  /* Note: mxCreateString */
	  mqmatrix = mxCreateDoubleMatrix(6,1,mxREAL);
	  mxGetPr(mqmatrix)[0] = nheader->quatern_b;
	  mxGetPr(mqmatrix)[1] = nheader->quatern_c;
	  mxGetPr(mqmatrix)[2] = nheader->quatern_d;
	  mxGetPr(mqmatrix)[3] = nheader->qoffset_x;
	  mxGetPr(mqmatrix)[4] = nheader->qoffset_y;
	  mxGetPr(mqmatrix)[5] = nheader->qoffset_z;
	  mxSetField(ORIENT_OUT,0,"qmatrix",mqmatrix);
	  if (nheader->sform_code == 0) {
	    msform = mxCreateString("unknown");
	  }
	  else if (nheader->sform_code == 1) {
	    msform = mxCreateString("scanner");
	  }
	  else if (nheader->sform_code == 2) {
	    msform = mxCreateString("anatomy");
	  }
	  else if (nheader->sform_code == 3) {
	    msform = mxCreateString("talairach");
	  }
	  else if (nheader->sform_code == 4) {
	    msform = mxCreateString("mni");
	  }
	  else {
	    msform = mxCreateDoubleMatrix(1,1,mxREAL);
	    mxGetPr(mqform)[0] = nheader->sform_code;
	  }
	  mxSetField(ORIENT_OUT,0,"sform",msform);
	  msmatrix = mxCreateDoubleMatrix(4,4,mxREAL);
	  mxGetPr(msmatrix)[0] = nheader->srow_x[0];
	  mxGetPr(msmatrix)[4] = nheader->srow_x[1];
	  mxGetPr(msmatrix)[8] = nheader->srow_x[2];
	  mxGetPr(msmatrix)[12] = nheader->srow_x[3];
	  mxGetPr(msmatrix)[1] = nheader->srow_y[0];
	  mxGetPr(msmatrix)[5] = nheader->srow_y[1];
	  mxGetPr(msmatrix)[9] = nheader->srow_y[2];
	  mxGetPr(msmatrix)[13] = nheader->srow_y[3];
	  mxGetPr(msmatrix)[2] = nheader->srow_z[0];
	  mxGetPr(msmatrix)[6] = nheader->srow_z[1];
	  mxGetPr(msmatrix)[10] = nheader->srow_z[2];
	  mxGetPr(msmatrix)[14] = nheader->srow_z[3];
	  mxGetPr(msmatrix)[3] = 0;
	  mxGetPr(msmatrix)[7] = 0;
	  mxGetPr(msmatrix)[11] = 0;
	  mxGetPr(msmatrix)[15] = 1; 
	  mxSetField(ORIENT_OUT,0,"smatrix",msmatrix);
	}
#ifdef USE_ZLIB
	gzclose(fgzin);
#else
	fclose(fin);
#endif

	mxFree(nheader);
	mxFree(sfnames);
	return;
	
      }
      else {
	/* ARG2 is the datatype */

      }
    }
    else {
      /* ARG2 is the volume number */
      volnum = mxGetData(ARG2_IN);
      
    }
  }
  
  if (volnum) {
    numdims = 3;
    if (*volnum > nheader->dim[4]) {
      mexErrMsgTxt("Volume number exceeds size of file.");
    }
  }
  else {
    numdims = (int)nheader->dim[0];
  }

  dimensions = (int *)mxCalloc(numdims,sizeof(int));
  for (ii=0;ii<numdims;++ii) {
    dimensions[ii] = nheader->dim[ii+1];
  }
  
  switch (nheader->datatype) {
  case NIFTI_TYPE_UINT8:
    DATA_OUT = mxCreateNumericArray(numdims,
				    dimensions, mxUINT8_CLASS,mxREAL);
    dsize = sizeof(uint8_t);
    break;
  case NIFTI_TYPE_INT8:
    DATA_OUT = mxCreateNumericArray(numdims,
				    dimensions, mxINT8_CLASS,mxREAL);
    dsize = sizeof(int8_t);
    break;
  case NIFTI_TYPE_UINT16:
    DATA_OUT = mxCreateNumericArray(numdims,
				    dimensions, mxUINT16_CLASS,mxREAL);
    dsize = sizeof(uint16_t);
    break;
  case NIFTI_TYPE_INT16:
    DATA_OUT = mxCreateNumericArray(numdims,
				    dimensions, mxINT16_CLASS,mxREAL);
    dsize = sizeof(int16_t);
    break;
  case NIFTI_TYPE_UINT32:
    DATA_OUT = mxCreateNumericArray(numdims,
				    dimensions, mxUINT32_CLASS,mxREAL);
    dsize = sizeof(uint32_t);
    break;
  case NIFTI_TYPE_INT32:
    DATA_OUT = mxCreateNumericArray(numdims,
				    dimensions, mxINT32_CLASS,mxREAL);
    dsize = sizeof(int32_t);
    break;
  case NIFTI_TYPE_UINT64:
    DATA_OUT = mxCreateNumericArray(numdims,
				    dimensions, mxUINT64_CLASS,mxREAL);
    dsize = sizeof(uint64_t);
    break;
  case NIFTI_TYPE_INT64:
    DATA_OUT = mxCreateNumericArray(numdims,
				    dimensions, mxINT64_CLASS,mxREAL);
    dsize = sizeof(int64_t);
    break;
  case NIFTI_TYPE_FLOAT32:
    DATA_OUT = mxCreateNumericArray(numdims,
				    dimensions, mxSINGLE_CLASS,mxREAL);
    dsize = sizeof(float);
    break;
  case NIFTI_TYPE_FLOAT64:
    DATA_OUT = mxCreateNumericArray(numdims,
				    dimensions, mxDOUBLE_CLASS,mxREAL);
    dsize = sizeof(double);
    break;
  default:
    /* No support yet for RGB or Complex types */
    mexErrMsgTxt("Unsupported data type.");
    break;
  }

  numelements = mxGetNumberOfElements(DATA_OUT);

  if (nheader->magic[1] == '+') {
    /* n+1 file. Data and header information are in one file.
       Usually called .nii */
#ifdef USE_ZLIB
    gzseek(fgzin,nheader->vox_offset,SEEK_SET);
#else
    fseek(fin,nheader->vox_offset,SEEK_SET);
#endif
  }
  else { /* if (nheader->magic[1] == 'i') { */
    /* ni1 file. Data and header information are in separate files.
       This program will strip off the last extension and look for a
       file called prefix + '.img' 
       Note: I commented this out to allow it to read Analyze files
    */
#ifdef USE_ZLIB
    gzclose(fgzin);
#else
    fclose(fin);
#endif

    fileprefix = (char *)mxCalloc(strlen(filename)+5,sizeof(char));
    if (strcmp(filename+(ii-3),".gz") == 0) {
      ii = strlen(filename)-4;
      while (filename[ii] != '.') {
	--ii;
	if (ii == 0) {
	  ii = strlen(filename)-3;
	  break;
	}
      }
    }
    else {
      ii = strlen(filename)-1;
      while (filename[ii] != '.') {
	--ii;
	if (ii == 0) {
	  ii = strlen(filename);
	  break;
	}
      }
    }
    strncpy(fileprefix,filename,ii);
    fileprefix = strcat(fileprefix,".img");

#ifdef USE_ZLIB
    fgzin = gzopen(fileprefix,"rb");
    if (!fgzin) {
      fileprefix = strcat(fileprefix,".gz");
      fgzin = gzopen(fileprefix,"rb");
      if (!fgzin) {
	mexErrMsgTxt("Could not open data (.img) file.");
      }
    }
#else
    fin = fopen(fileprefix,"rb");
    if (!fin) {
      mexErrMsgTxt("Could not open data (.img) file.");
    }
#endif

    mxFree(fileprefix);
  }
  /*
  else {
    mexErrMsgTxt("Error in magic number of header.");
  }
  */

#ifdef USE_ZLIB
  if (volnum) {
    gzseek(fgzin,dsize*numelements*(*volnum-1),SEEK_CUR);
  }
  gzread(fgzin,(char *)mxGetPr(DATA_OUT),numelements*dsize);
#else
  if (volnum) {
    fseek(fin,dsize*numelements*(*volnum-1),SEEK_CUR);
  }
  fread((char *)mxGetPr(DATA_OUT),dsize,numelements,fin);
#endif

  if (nlhs  > 1) {
   VSIZE_OUT = mxCreateDoubleMatrix(1,nheader->dim[0],mxREAL);
   for (ii=0; ii<nheader->dim[0]; ++ii) {
     dataptr = mxGetPr(VSIZE_OUT);
     dataptr[ii] = nheader->pixdim[ii+1];
   }
  }
  
  if (nlhs > 2) {
    ORIENT_OUT = mxCreateStructMatrix(1,1,5,sfnames);
    mqfac = mxCreateDoubleMatrix(1,1,mxREAL);
    mxGetPr(mqfac)[0] = nheader->pixdim[0];
    mxSetField(ORIENT_OUT,0,"qfac",mqfac);
	  
    if (nheader->qform_code == 0) {
      mqform = mxCreateString("unknown");
    }
    else if (nheader->qform_code == 1) {
      mqform = mxCreateString("scanner");
    }
    else if (nheader->qform_code == 2) {
      mqform = mxCreateString("anatomy");
    }
    else if (nheader->qform_code == 3) {
      mqform = mxCreateString("talairach");
    }
    else if (nheader->qform_code == 4) {
      mqform = mxCreateString("mni");
    }
    else {
      mqform = mxCreateDoubleMatrix(1,1,mxREAL);
      mxGetPr(mqform)[0] = nheader->qform_code;
    }
    mxSetField(ORIENT_OUT,0,"qform",mqform);

    mqmatrix = mxCreateDoubleMatrix(6,1,mxREAL);
    mxGetPr(mqmatrix)[0] = nheader->quatern_b;
    mxGetPr(mqmatrix)[1] = nheader->quatern_c;
    mxGetPr(mqmatrix)[2] = nheader->quatern_d;
    mxGetPr(mqmatrix)[3] = nheader->qoffset_x;
    mxGetPr(mqmatrix)[4] = nheader->qoffset_y;
    mxGetPr(mqmatrix)[5] = nheader->qoffset_z;
    mxSetField(ORIENT_OUT,0,"qmatrix",mqmatrix);

    if (nheader->sform_code == 0) {
      msform = mxCreateString("unknown");
    }
    else if (nheader->sform_code == 1) {
      msform = mxCreateString("scanner");
    }
    else if (nheader->sform_code == 2) {
      msform = mxCreateString("anatomy");
    }
    else if (nheader->sform_code == 3) {
      msform = mxCreateString("talairach");
    }
    else if (nheader->sform_code == 4) {
      msform = mxCreateString("mni");
    }
    else {
      msform = mxCreateDoubleMatrix(1,1,mxREAL);
      mxGetPr(msform)[0] = nheader->sform_code;
    }
    mxSetField(ORIENT_OUT,0,"sform",msform);

    msmatrix = mxCreateDoubleMatrix(4,4,mxREAL);
    mxGetPr(msmatrix)[0] = nheader->srow_x[0];
    mxGetPr(msmatrix)[4] = nheader->srow_x[1];
    mxGetPr(msmatrix)[8] = nheader->srow_x[2];
    mxGetPr(msmatrix)[12] = nheader->srow_x[3];
    mxGetPr(msmatrix)[1] = nheader->srow_y[0];
    mxGetPr(msmatrix)[5] = nheader->srow_y[1];
    mxGetPr(msmatrix)[9] = nheader->srow_y[2];
    mxGetPr(msmatrix)[13] = nheader->srow_y[3];
    mxGetPr(msmatrix)[2] = nheader->srow_z[0];
    mxGetPr(msmatrix)[6] = nheader->srow_z[1];
    mxGetPr(msmatrix)[10] = nheader->srow_z[2];
    mxGetPr(msmatrix)[14] = nheader->srow_z[3];
    mxGetPr(msmatrix)[3] = 0;
    mxGetPr(msmatrix)[7] = 0;
    mxGetPr(msmatrix)[11] = 0;
    mxGetPr(msmatrix)[15] = 1; 
    mxSetField(ORIENT_OUT,0,"smatrix",msmatrix);
  }

  if (nlhs > 3) {
    
    switch (nheader->datatype) {
    case NIFTI_TYPE_UINT8:
      DTYPE_OUT = mxCreateString("uint8");
      break;
    case NIFTI_TYPE_INT8:
      DTYPE_OUT = mxCreateString("int8");
      break;
    case NIFTI_TYPE_UINT16:
      DTYPE_OUT = mxCreateString("uint16");
      break;
    case NIFTI_TYPE_INT16:
      DTYPE_OUT = mxCreateString("int16");
      break;
    case NIFTI_TYPE_UINT32:
      DTYPE_OUT = mxCreateString("uint32");
      break;
    case NIFTI_TYPE_INT32:
      DTYPE_OUT = mxCreateString("int32");
      break;
    case NIFTI_TYPE_UINT64:
      DTYPE_OUT = mxCreateString("uint64");
      break;
    case NIFTI_TYPE_INT64:
      DTYPE_OUT = mxCreateString("int64");
      break;
    case NIFTI_TYPE_FLOAT32:
      DTYPE_OUT = mxCreateString("single");
      break;
    case NIFTI_TYPE_FLOAT64:
      DTYPE_OUT = mxCreateString("double");
      break;
    default:
      mexErrMsgTxt("Unsupported data type.");
      break;
    }
  }
#ifdef USE_ZLIB
  gzclose(fgzin);
#else
  fclose(fin);
#endif
  mxFree(nheader);

  mxFree(dimensions);
  mxFree(sfnames);
  if (strtmp) {
    mxFree(strtmp);
  }
}
