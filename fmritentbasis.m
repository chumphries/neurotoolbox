function [T] = fmritentbasis(numtents,tr,tlen)
% FMRISINEBASIS - Create tent function basis set for fMRI analysis
%
%   usage:


%      [T] = fmritentbasis(lags,numtents,tstart,tend,tr)
%
%         lags     - length in tr's of basis vectors
%         numtents - number of tentfunction
%         tstart   - position in time of first tent function
%         tend     - position in time of last tent function
%         tr       - length in time of each lag
%

% Written by Colin Humphries
%    Medical College of Wisconsin
%    1/2008

% Reference: Ziad S. Saad, Gang Chen, Richard C. Reynolds, Patricia
% P. Christidis, Kenneth R. Hammett, Patrick S.F. Bellgowan, Robert
% W. Cox. Functional imaging analysis contest (FIAC) analysis according to
% AFNI and SUMA. Human Brain Mapping. 27(5). 417-424.

% A tent basis function is simply a linear B spline
% T(x) = max(0,1-|x|);
%


% lagtimes = lags*tr;
lagtimes = (0:tr:tlen)';


% T = zeros(length(lags),numtents);
T = zeros(length(lagtimes),numtents);


if numtents > 1
  % tenttimes = linspace(tstart,tend,numtents);
  tenttimes = linspace(0,tlen,numtents);
  L = tenttimes(2)-tenttimes(1);
else
  % if tstart ~= tend
  %   error(['When the number of tents is one, the start and end times must ' ...
  %           'be equal']);
  % end
  % tenttimes = tstart;
  % L = tstart;
  error('need more than 1 tent')
end
  
% for ii = 1:numtents
%   for jj = 1:length(lags)
%     T(jj,ii) = max(0,1-abs((lagtimes(jj)-tenttimes(ii))/L));
%   end
% end

for ii = 1:numtents
  T(:,ii) = max(0,1-abs((lagtimes-tenttimes(ii))/L));
end