function [o1] = surfdraw(p0,p1,p2,p3,p4)
%SURFDRAW   Plot functional data on brain surface
%
% Setup initial display
%   SURFDRAW(VERTS,FACES,SURFSHAPE)
%      VERTS     - vertices matrix (NV x 3)
%      FACES     - faces matrix (NF x 3)
%      SURFSHAPE - curvature vector
%
%   SURFDRAW(VERTS,FACES,SURFSHAPE,DISPLAYTYPE)
%      DISPLAYTYPE - 'twocolor','continuous','whiteblack',
%                    'grayblack','whitegray','redgreen',
%                    'yellowblue'
%                    or a matlab style colormap
%                    e.g.  gray(32)
%                          jet(64)
%                          [.5 .5 .5;.9 .9 .9]
%                    default: 'twocolor'
%
%   SURFDRAW(SURFACE_FILE,SHAPE_FILE)
%      SURFACE_FILE - freesurfer surface file
%      SHAPE_FILE   - freesurfer curvature file
%      e.g. SURFDRAW('lh.inflated','lh.curv');
%
%   SURFDRAW(SURFACE_FILE,SHAPE_FILE,DISPLAYTYPE)
%
% Add a solid color map
%   SURFDRAW('addmap',MAP,COLOR)
%      MAP   - functional map (vector or metric filename)
%      COLOR - Map color (note: all non-zero values are colored)
%              'r','g','b','w','k','y','m','c' or
%              rgb vector (1x3)
%
%   SURFDRAW('addmap',MAP,COLOR,ALPHA)
%      ALPHA - alpha transparency level (between 0 and 1)
%
% Add a color scaled map (scaled -abs(max) to abs(max))
%   SURFDRAW('addfunc',MAP)
%   SURFDRAW('addfunc',MAP,CMAP)
%   SURFDRAW('addfunc',MAP,CMAP,ALPHA)
%      MAP   - functional map (vector or metric filename)
%      CMAP  - matlab style colormap ([] - use default)
%      ALPHA - alpha transparency level
%   SURFDRAW('addfunc',MAP,TH)
%   SURFDRAW('addfunc',MAP,TH,CMAP)
%   SURFDRAW('addfunc',MAP,TH,CMAP,ALPHA)
%   SURFDRAW('addfunc',MAP,TH,THTYPE)
%   SURFDRAW('addfunc',MAP,TH,THTYPE,CMAP)
%   SURFDRAW('addfunc',MAP,TH,THTYPE,CMAP,ALPHA)
%      TH     - threshold
%      THTYPE - theshold type 'p','n','pn','np'
%
% Add a color scaled map (scaled max to min)
%   SURFDRAW('addfuncmm',MAP)
%   SURFDRAW('addfuncmm',MAP,CMAP)
%   SURFDRAW('addfuncmm',MAP,CMAP,ALPHA)
%   SURFDRAW('addfuncmm',MAP,TH)
%   SURFDRAW('addfuncmm',MAP,TH,CMAP)
%   SURFDRAW('addfuncmm',MAP,TH,CMAP,ALPHA)
%   SURFDRAW('addfuncmm',MAP,TH,THTYPE)
%   SURFDRAW('addfuncmm',MAP,TH,THTYPE,CMAP)
%   SURFDRAW('addfuncmm',MAP,TH,THTYPE,CMAP,ALPHA)
%
% Add a color scaled map (user defined)
%   SURFDRAW('addfuncsc',MAP,SCALE)
%      SCALE - scaling limits [min max]
%   SURFDRAW('addfuncsc',MAP,SCALE,CMAP)
%   SURFDRAW('addfuncsc',MAP,SCALE,CMAP,ALPHA)
%   SURFDRAW('addfuncsc',MAP,SCALE,TH)
%   SURFDRAW('addfuncsc',MAP,SCALE,TH,CMAP)
%   SURFDRAW('addfuncsc',MAP,SCALE,TH,CMAP,ALPHA)
%   SURFDRAW('addfuncsc',MAP,SCALE,TH,THTYPE)
%   SURFDRAW('addfuncsc',MAP,SCALE,TH,THTYPE,CMAP)
%   SURFDRAW('addfuncsc',MAP,SCALE,TH,THTYPE,CMAP,ALPHA)
%
% Add an outline of a functional map
%   SURFDRAW('addoutline',MAP,COLOR)
%      MAP   - functional map (vector or metric filename)
%      COLOR - Map color (note: all non-zero values are colored)
%              'r','g','b','w','k','y','m','c' or
%              rgb vector (1x3)
%   SURFDRAW('addoutline',MAP,COLOR,WIDTH)
%      WIDTH - line width
%
%   SURFDRAW('setview',VIEW);
%      VIEW - 'left','right','top','bottom','front','back'
%

% Written by Colin Humphries
%

% Copywrite (c) 2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/


DEFAULT_LINEWIDTH = 2;
DEFAULT_LINECOLOR = 'r';

if ~isstr(p0)
  verts = p0;
  faces = p1;
  % ss = -p2;
  ss = p2;
  
  if length(ss) ~= size(verts,1)
    error('Surface shape size must match number of vertices')
  end
  
  if nargin > 3
    displaytype = p3;
  else
    displaytype = 'twocolor';
  end
  
  % Clip curvature
  % ii = find(ss < -.5);
  % ss(ii) = -.5*ones;
  % ii = find(ss > .5);
  % ss(ii) = .5*ones;
  
  % Scale from 0 to 1
  % ss = (ss-min(ss))/(max(ss)-min(ss));
  ss = clip(ss+.5,0,1);
  
  
  cd = zeros(length(ss),3);
  
  if ischar(displaytype)
    switch displaytype
     case 'twocolor'   
      ii = find(ss > .5);
      cd(ii,:) = repmat([.7059 .7059 .7059],length(ii),1);
      ii = find(ss <= .5);
      cd(ii,:) = repmat([.5098 .5098 .5098],length(ii),1);
     case 'continuous'
      cd = [ss(:) ss(:) ss(:)];
     case 'whiteblack'
      ii = find(ss > .5);
      cd(ii,:) = repmat([1 1 1],length(ii),1);
      ii = find(ss <= .5);
      cd(ii,:) = repmat([0 0 0],length(ii),1);
     case 'grayblack'
      ii = find(ss > .5);
      cd(ii,:) = repmat([.7059 .7059 .7059],length(ii),1);
      ii = find(ss <= .5);
      cd(ii,:) = repmat([0 0 0],length(ii),1);
     case 'whitegray'
      ii = find(ss > .5);
      cd(ii,:) = repmat([1 1 1],length(ii),1);
      ii = find(ss <= .5);
      cd(ii,:) = repmat([.5098 .5098 .5098],length(ii),1);
     case 'redgreen'
      ii = find(ss > .5);
      cd(ii,:) = repmat([.9 .4 .4],length(ii),1);
      ii = find(ss <= .5);
      cd(ii,:) = repmat([0 .4 0],length(ii),1);    
     case 'yellowblue'
      ii = find(ss > .5);
      cd(ii,:) = repmat([.9 .9 .4],length(ii),1);
      ii = find(ss <= .5);
      cd(ii,:) = repmat([.1 .1 .7],length(ii),1);    
     otherwise
      error('Unknown display type');
    end
  else
    CMN = size(displaytype,1);
    ss = floor(ss*(CMN-.0000001)+1);
    cd = displaytype(ss,:);
  end
  
  % cd = [ss(:) ss(:) ss(:)];
  % clf
  % set(gcf,'color','w')
  % axes('position',[0 0 1 1]);
  cla
  
  o1 = patch('vertices',verts,'faces',faces,...
        'facevertexcdata',cd,'edgecolor','none','facecolor','interp',...
        'tag','surfdraw');
  % axis equal;
  if size(verts,2) > 2
    axis vis3d;
  end
  axis off;
  if size(verts,2) > 2
    view(-90,0);
  else
    % camroll(-90)
  end
  % axes('position',[.8 .1 .19 .8]);
  % axis off
  % text(.5,.95,'Legend','horizontalalignment','center');
  
else
  switch p0
   case 'addmap'
    dmap = p1;
    col = p2;
    if nargin < 4
      alpha = 1;
    else
      alpha = p3;
    end
    if ischar(dmap)
      dmap = readmetric(dmap);
    end
    if ischar(col)
      col = colorlookup(col);
    end
    oh = findobj('tag','surfdraw');
    if length(oh) > 1
      oh = findobj('tag','surfdraw','parent',gca);
      if isempty(oh)
        error('Please click in axis and run command again');
      end
    end
    od = get(oh,'facevertexcdata');
    ii = find(dmap ~= 0);
    % keyboard
    if alpha == 1
      od(ii,:) = repmat(col(:)',length(ii),1);
    else
      od(ii,:) = (1-alpha)*od(ii,:) + ...
          alpha*repmat(col(:)',length(ii),1);
    end
    set(oh,'facevertexcdata',od);
    
   case {'addfunc','addfuncmm'}
    dmap = p1;
    alpha = [];
    thtype = [];
    cmap = [];
    th = [];
    if nargin > 2
      if length(p2) == 1
        th = p2;
        if nargin > 3
          if ischar(p3)
            thtype = p3;
            if nargin > 4
              cmap = p4;
            end
            if nargin > 5
              alpha = p5;
            end
          else
            cmap = p3;
            if nargin > 4
              alpha = p4;
            end
          end
        end
      else
        cmap = p2;
        if nargin > 3
          alpha = p3;
        end
      end
    end
    if ischar(dmap)
      dmap = readmetric(dmap);
    end
    if isempty(thtype)
      thtype = 'pn';
    end
    if isempty(alpha)
      alpha = 1;
    end
    if isempty(cmap)
      cmap = jet(128);
    end
    oh = findobj('tag','surfdraw');
    if length(oh) > 1
      oh = findobj('tag','surfdraw','parent',gca);
      if isempty(oh)
        error('Please click in axis and run command again');
      end
    end
    od = get(oh,'facevertexcdata');
    if ~isempty(th)
      switch thtype
       case 'p'
        ii = find(dmap > th);
       case 'n'
        ii = find(dmap < th);
       case 'pn'
        ii = find(dmap > th | dmap < -1*th);
       case 'np'
        ii = find(dmap < th & dmap > -1*th);
       otherwise
        error('unknown threshold type');
      end
    else
      ii = find(dmap ~= 0);
    end
    qq = find(~isnan(dmap(ii)));
    ii = ii(qq);
    if strcmp(p0,'addfunc')
      maxval = max(abs(dmap));
      minval = -maxval;
    else
      maxval = max(dmap);
      minval = min(dmap);
    end
    dmap2 = (dmap(ii)-minval)/(maxval-minval);
    % dmap2 = (dmap(ii)+maxval)/(maxval+maxval);
    dmap2 = floor(dmap2*(size(cmap,1)-.0001)+1);
    % keyboard
    if alpha == 1
      od(ii,:) = cmap(dmap2,:);
    else
      od(ii,:) = (1-alpha)*od(ii,:) + ...
          alpha*cmap(dmap2,:);
    end
    set(oh,'facevertexcdata',od);
    
   case 'addfuncsc'
    dmap = p1;
    scale = p2;
    alpha = [];
    thtype = [];
    cmap = [];
    th = [];
    if nargin > 3
      if length(p3) == 1
        th = p3;
        if nargin > 4
          if ischar(p4)
            thtype = p4;
            if nargin > 5
              cmap = p5;
            end
            if nargin > 6
              alpha = p6;
            end
          else
            cmap = p4;
            if nargin > 5
              alpha = p5;
            end
          end
        end
      else
        cmap = p3;
        if nargin > 4
          alpha = p4;
        end
      end
    end
    if ischar(dmap)
      dmap = readmetric(dmap);
    end
    if isempty(thtype)
      thtype = 'pn';
    end
    if isempty(alpha)
      alpha = 1;
    end
    if isempty(cmap)
      cmap = jet(128);
    end
    oh = findobj('tag','surfdraw');
    if length(oh) > 1
      oh = findobj('tag','surfdraw','parent',gca);
      if isempty(oh)
        error('Please click in axis and run command again');
      end
    end
    od = get(oh,'facevertexcdata');
    if ~isempty(th)
      switch thtype
       case 'p'
        ii = find(dmap > th);
       case 'n'
        ii = find(dmap < th);
       case 'pn'
        ii = find(dmap > th | dmap < -1*th);
       case 'np'
        ii = find(dmap < th & dmap > -1*th);
       otherwise
        error('unknown threshold type');
      end
    else
      ii = find(dmap ~= 0);
    end
    qq = find(~isnan(dmap(ii)));
    ii = ii(qq);
    % maxval = max(dmap);
    % minval = min(dmap);
    maxval = scale(2);
    minval = scale(1);
    dmap2 = (dmap(ii)-minval)/(maxval-minval);
    dmap2 = floor(dmap2*(size(cmap,1)-.0000001)+1);
    iind = find(dmap2 <= 0);
    dmap2(iind) = ones;
    iind = find(dmap2 >= size(cmap,1));
    dmap2(iind) = size(cmap,1)*ones;
    % keyboard
    if alpha == 1
      od(ii,:) = cmap(dmap2,:);
    else
      od(ii,:) = (1-alpha)*od(ii,:) + ...
          alpha*cmap(dmap2,:);
    end
    set(oh,'facevertexcdata',od);
    
   case 'addoutline'
    dmap = p1;
    if ischar(dmap)
      dmap = readmetric(dmap);
    end
    if nargin < 3
      color = DEFAULT_LINECOLOR;
    else 
      color = p2;
    end
    if ischar(color)
      color = colorlookup(color);
    end
    if nargin < 4
      width = DEFAULT_LINEWIDTH;
    else 
      width = p3;
    end
    
    oh = findobj('tag','surfdraw');
    if length(oh) > 1
      oh = findobj('tag','surfdraw','parent',gca);
      if isempty(oh)
        error('Please click in axis and run command again');
      end
    end
    verts = get(oh,'vertices');
    faces = get(oh,'faces');
    % keyboard
    cm = surfoutline(double(dmap),verts,faces);
    
    ind = 1;
    while(ind <= size(cm,1))
      numverts = cm(ind,1);
      if size(cm,2) == 2
        line(cm(ind+1:ind+numverts,1),cm(ind+1:ind+numverts,2),...
             'color',color,'linewidth',width);
      else
        line(cm(ind+1:ind+numverts,1),cm(ind+1:ind+numverts,2),...
             cm(ind+1:ind+numverts,3),'color',color,...
             'linewidth',width);
      end
      ind = ind + numverts + 1;
    end
    
   case 'setview'
    switch p1
     case 'medial'
      view(90,0);
     case 'lateral'
      view(-90,0);
     case 'left'
      view(-90,0);
     case 'right'
      view(90,0);
     case {'top','dorsal'}
      view(0,90);
     case {'bottom','ventral'}
      view(0,-90);
     case 'front'
      view(180,0)
     case 'back'
      view(0,0)
     otherwise
      error('Unknown view type');
    end

   case 'lastpoint'
    oh = findobj('tag','surfdraw');
    if length(oh) > 1
      oh = findobj('tag','surfdraw','parent',gca);
      if isempty(oh)
        error('Please click in axis and run command again');
      end
    end
    axh = get(oh,'parent');
    p = get(axh,'currentpoint');
    verts = get(oh,'Vertices');
    if size(verts,2) == 2
      verts = [verts zeros(size(verts,1),1)];
    end
    d = sqrt(sum((cross(verts-repmat(p(1,:),size(verts,1),1),...
                        verts-repmat(p(2,:),size(verts,1),1)).^2)')) / ...
        sqrt(sum(((p(2,:)-p(1,:)).^2)));
    [s,sind] = sort(d);
    d2 = sqrt(sum(((verts(sind(1:10),:)-repmat(p(1,:),10, ...
                                               1)).^2)'));
    iii = find(d2 == min(d2));
    o1 = sind(iii);
    
   otherwise
    [verts,faces] = readsurface(p0);
    ss = readcurvfile(p1);
    if nargin > 2
      surfdraw(verts,faces,-1*ss,p2);
    else
      surfdraw(verts,faces,-1*ss);
    end
    
  end
end



function [r] = colorlookup(col)
switch col
 case 'b'
  r = [0 0 1];
 case 'g'
  r = [0 1 0];
 case 'r'
  r = [1 0 0];
 case 'c'
  r = [0 1 1];
 case 'm'
  r = [1 0 1];
 case 'y'
  r = [1 1 0];
 case 'k'
  r = [0 0 0];
 case 'w'
  r = [1 1 1];
  
 otherwise
  error('unknown color');
end

function [o1] = clip(X,lim1,lim2)
%  [o1] = clip(X,lim1,lim2)
%

if nargin < 3
  lim2 = [];
end

o1 = X;

if ~isempty(lim1)
  ii = find(X < lim1);
  o1(ii) = lim1*ones;
end

if ~isempty(lim2)
  ii = find(X > lim2);
  o1(ii) = lim2*ones;
end

