function [o1,o2] = orthoview(varargin)
% ORTHOVIEW   View fMRI data in orthogonal windows
%    ORTHOVIEW(ANAT, FUNC, 'property', value, ...)
%    ANAT and FUNC are the anatomical and functional volumes. They are
%    specified as a filename (.nii or .img), a vector/matrix of MRI
%    data, or an empty matrix [].
%
%    If the MRI volume is a matrix or vector, orientation and voxel sizes
%    can be specified in a cell array following the volume. 
%    ORTHOVIEW(ANAT,{VSIZE,ORIENT},...) - 3d matrix
%    ORTHOVIEW(ANAT,{ISIZE,VSIZE,ORIENT},...) - 1d vector
%    ORTHOVIEW(ANAT,{ISIZE,VSIZE,ORIENT,INDEX},...) - indexed 1d vector
%       ISIZE - 3d volume size (x,y,z)
%       VSIZE - voxel size (x,y,z)
%       ORIENT - image orientation
%          structure (from readnifti), [], [xorig,yorig,yorig],
%          quaternion [qa qb qc xorig yorig zorig], 
%          rotation matrix (4x4)
%       INDEX - index into original 3d volume i.e.,
%          anat3d=zeros(ISIZE);anat3d(INDEX) = ANAT;
%
%    Or, orientation and voxel sizes can be specified by supplying a
%    filename after the volume. Information is loaded from the file.
%    ORTHOVIEW(ANAT,FILENAME,...)
%    ORTHOVIEW(ANAT,FUNC,FILENAME,INDEX,...)
%
%    Finally, orientation and voxel sizes can also be specified for both
%    volumes at once.
%    ORTHOVIEW(ANAT,FUNC,vsize,orient,...)
%    ORTHOVIEW(ANAT,FUNC,isize,vsize,orient,...)
%    ORTHOVIEW(ANAT,FUNC,isize,vsize,orient,index)
%
%    Several additional properties can be set from the command line.
%       'aisize'   - Anatomical image size
%       'avsize'   - Anatomical voxel size
%       'aorient'  - Anatomical orienation
%       'aindex'   - Anatomical index
%       'fisize'   - Functional image size
%       'fvsize'   - Functional voxel size
%       'foffset'  - Functional orienation
%       'findex'   - Functional index
%       'thresh'   - Initial threshold
%       'scaling'  - Colormap scaling: 'maxmin', 'absmax', [v1 v2]
%       'thtype'   - Threshold type: 'p', 'n', 'pn', 'np'
%       'position' - Initial crosshair position
%       
%    Examples:
%       orthoview('brain.nii')
%       orthoview('brain.nii','zmap.nii')
%       

% orthoview(A,B,isize,vsize,orient,index)
%          (A,B,aisize,avsize,aorient,aindex,bisize,bvsize,borient,bindex)
%          (A,B,isize,vsize)
%          (A,B,vsize,orient,index)
%          (A,B,vsize,orient)
%          (A,B,vsize)
%          (A,B,filename)
%          (A,B,filename,index)
%          (A,B)
%          (A)
%          (A,{isize,vsize,orient,index},B,{isize,vsize,orient,index},...
%          (A,{vsize,orient},B,{isize,vsize,orient,index},...
%          (A,filename,B,filename)
%

% Orthoview version 2.0
%
% Written by Colin Humphries
% chumphri@mcw.edu
% Feb, 2000

% Copywrite (c) 2000-2008, Colin J. Humphries, All Rights Reserved

% Updates:
% Version 1 (2000-2008)
%   -added voxel boxes
%   -added export feature
%   -added readanalyze
%   -added position boxes 8/00
%   -put rendering commands in the render_slice.m script 9/00
%   -changed coordinate system to SPM standard 9/00
%   -added some comments 12/02
% Version 2 (2008-)
%   -completely rewrote code (3/2008)
%      -changed command line options
%      -added nifti orientation support and changed internal coordinate
%        system to match nifti coordinate system supporting rotation of
%        the volumes into a similar coordinate system
%      -added new rendering code. Instead of resampling to one image on
%        each mouse press, two images with diff resolutions are
%        overlaid. The old rendering code is preserved for systems
%        without opengl.
%      -added ability to change anat volume. Added UI options to load new
%        volumes. 8/08 

% Current bugs and todo
%  Read 4x4 rotation matrix




DEFAULT_VSIZE = [1 1 1];
DEFAULT_ORIENT = 0;
% DEFAULT_ASCALE = 1;
DEFAULT_ASCALE_CLIP = 1;
DEFAULT_ASCALE_CLIP_VAL = .99;  % number less than or equal to 1
DEFAULT_CMAPSIZE = [32 32];
DEFAULT_FCMAP = 'jet';
DEFAULT_ACMAP = 'gray';
DEFAULT_FOV = 'bound'; % number, 'bound'
DEFAULT_CHCOLOR = [1 0 1];
DEFAULT_THRESH = 0;
DEFAULT_THRESHOLDTYPE = 'pn'; % p, n, pn, np, eq
DEFAULT_SCALING = 'absmax'; % absmax, maxmin, [x y] - user defined
DEFAULT_OPENGL = 1; % 1 - use opengl, 0 - don't use opengl
DEFAULT_CROSSHAIRS = 1;
DEFAULT_MASK_COLOR = [1 .2 1];

%
SMALLNUM = .000001;

% Useful Info
% OV_DISPLAY(1) : crosshairs
% OV_DISPLAY(2) : anat
% OV_DISPLAY(3) : func
% OV_DISPLAY(4) : masking
% OV_DISPLAY(5) : view mask

% Define global variables
global OV_ADATA OV_APOSX OV_APOSY OV_APOSZ OV_CPOINT OV_IH OV_AXH OV_LH
global OV_FDATA OV_FPOSX OV_FPOSY OV_FPOSZ OV_UIH OV_TH OV_FINDEX OV_DISPLAY
global OV_VEUIH OV_MEUIH OV_MASK OV_FDATA_OLD

% Find the first string in the arguments. Ignore strings with .img and
% .nii
numargs = nargin;
firststring = 0;
if numargs > 0
  for ii = 1:numargs
    if ischar(varargin{ii})
      if isempty(findstr('.img',varargin{ii})) && ...
            isempty(findstr('.nii',varargin{ii}))
        firststring = ii;
        break;
      end
    end
  end
end

if firststring == 1
  % Check if we are running a subroutine
  subroutine = 1;
  switch varargin{1}
    % The following are subroutines called by various orthoview
    % functions.
   case 'drawslices'
    % Update and redraw brain images
    % This is the main display routine for rendering the fmri slices.
    % Figure out current slices and thresholds
    if ~isempty(OV_ADATA)
      [tmp,iix] = min(abs(OV_APOSX-OV_CPOINT(1)));
      [tmp,iiy] = min(abs(OV_APOSY-OV_CPOINT(2)));
      [tmp,iiz] = min(abs(OV_APOSZ-OV_CPOINT(3))); 
    end
    if ~isempty(OV_FDATA)
      [tmp,fiix] = min(abs(OV_FPOSX-OV_CPOINT(1)));
      [tmp,fiiy] = min(abs(OV_FPOSY-OV_CPOINT(2)));
      [tmp,fiiz] = min(abs(OV_FPOSZ-OV_CPOINT(3)));
      if isnan(OV_TH(6))
        if OV_TH(5) == 1
          fmx = max(abs(OV_TH(2:3)));
          fmn = -fmx;
        else
          fmx = OV_TH(3);
          fmn = OV_TH(2);
        end
      else
        fmn = OV_TH(5);
        fmx = OV_TH(6);
      end
      switch OV_TH(4)
       case 1
        % 'p'       
        if ~isempty(OV_MASK) & OV_DISPLAY(4)
          fmaskx = (OV_FDATA(:,:,fiiz)' > OV_TH(1) & OV_MASK(:,:,fiiz)');
          fmasky = (squeeze(OV_FDATA(:,fiiy,:))' > OV_TH(1) & ...
                    squeeze(OV_MASK(:,fiiy,:))');
          fmaskz = (squeeze(OV_FDATA(fiix,:,:))' > OV_TH(1) & ...
                    squeeze(OV_MASK(fiix,:,:))');
        else
          fmaskx = (OV_FDATA(:,:,fiiz)' > OV_TH(1));
          fmasky = (squeeze(OV_FDATA(:,fiiy,:))' > OV_TH(1));
          fmaskz = (squeeze(OV_FDATA(fiix,:,:))' > OV_TH(1));
        end
       case 2
        % 'n'
        if ~isempty(OV_MASK) & OV_DISPLAY(4)
          fmaskx = (OV_FDATA(:,:,fiiz)' < OV_TH(1) & OV_MASK(:,:,fiiz)');
          fmasky = (squeeze(OV_FDATA(:,fiiy,:))' < OV_TH(1) & ...
                    squeeze(OV_MASK(:,fiiy,:))');
          fmaskz = (squeeze(OV_FDATA(fiix,:,:))' < OV_TH(1) & ...
                    squeeze(OV_MASK(fiix,:,:))');
        else
          fmaskx = (OV_FDATA(:,:,fiiz)' < OV_TH(1));
          fmasky = (squeeze(OV_FDATA(:,fiiy,:))' < OV_TH(1));
          fmaskz = (squeeze(OV_FDATA(fiix,:,:))' < OV_TH(1));
        end
       case 3
        % 'pn'
        if ~isempty(OV_MASK) & OV_DISPLAY(4)
          fmaskx = ((OV_FDATA(:,:,fiiz)' > OV_TH(1) | ...
                     OV_FDATA(:,:,fiiz)' < -OV_TH(1)) & OV_MASK(:,:,fiiz)');
          fmasky = ((squeeze(OV_FDATA(:,fiiy,:))' > OV_TH(1) | ...
                     squeeze(OV_FDATA(:,fiiy,:))' < -OV_TH(1)) & ...
                    squeeze(OV_MASK(:,fiiy,:))');
          fmaskz = ((squeeze(OV_FDATA(fiix,:,:))' > OV_TH(1) | ...
                     squeeze(OV_FDATA(fiix,:,:))' < -OV_TH(1)) & ...
                    squeeze(OV_MASK(fiix,:,:))');
        else
          fmaskx = (OV_FDATA(:,:,fiiz)' > OV_TH(1) | ...
                    OV_FDATA(:,:,fiiz)' < -OV_TH(1));
          fmasky = (squeeze(OV_FDATA(:,fiiy,:))' > OV_TH(1) | ...
                    squeeze(OV_FDATA(:,fiiy,:))' < -OV_TH(1));
          fmaskz = (squeeze(OV_FDATA(fiix,:,:))' > OV_TH(1) | ...
                    squeeze(OV_FDATA(fiix,:,:))' < -OV_TH(1));
        end
       case 4
        % 'np'
        if ~isempty(OV_MASK) & OV_DISPLAY(4)
          fmaskx = ((OV_FDATA(:,:,fiiz)' < OV_TH(1) & ...
                     OV_FDATA(:,:,fiiz)' > -OV_TH(1)) & OV_MASK(:,:,fiiz)');
          fmasky = ((squeeze(OV_FDATA(:,fiiy,:))' < OV_TH(1) & ...
                     squeeze(OV_FDATA(:,fiiy,:))' > -OV_TH(1)) & ...
                    squeeze(OV_MASK(:,fiiy,:))');
          fmaskz = ((squeeze(OV_FDATA(fiix,:,:))' < OV_TH(1) & ...
                     squeeze(OV_FDATA(fiix,:,:))' > -OV_TH(1)) & ...
                    squeeze(OV_MASK(fiix,:,:))');
        else
          fmaskx = (OV_FDATA(:,:,fiiz)' < OV_TH(1) & ...
                    OV_FDATA(:,:,fiiz)' > -OV_TH(1));
          fmasky = (squeeze(OV_FDATA(:,fiiy,:))' < OV_TH(1) & ...
                    squeeze(OV_FDATA(:,fiiy,:))' > -OV_TH(1));
          fmaskz = (squeeze(OV_FDATA(fiix,:,:))' < OV_TH(1) & ...
                    squeeze(OV_FDATA(fiix,:,:))' > -OV_TH(1));
        end
       case 5
        % 'eq'
        if ~isempty(OV_MASK) & OV_DISPLAY(4)
          fmaskx = (OV_FDATA(:,:,fiiz)' == OV_TH(1) & OV_MASK(:,:,fiiz)');
          fmasky = (squeeze(OV_FDATA(:,fiiy,:))' == OV_TH(1) & ...
                    squeeze(OV_MASK(:,fiiy,:))');
          fmaskz = (squeeze(OV_FDATA(fiix,:,:))' == OV_TH(1) & ...
                    squeeze(OV_MASK(fiix,:,:))');
        else
          fmaskx = (OV_FDATA(:,:,fiiz)' == OV_TH(1));
          fmasky = (squeeze(OV_FDATA(:,fiiy,:))' == OV_TH(1));
          fmaskz = (squeeze(OV_FDATA(fiix,:,:))' == OV_TH(1));
        end
      end
    end
    
    if DEFAULT_OPENGL
      % Render images using transparency
      % Note: in this mode, two images are displayed on the axes at the
      % same time. The top image (ie the functional) has transparent
      % sections below threshold.
      if ~isempty(OV_ADATA) & OV_DISPLAY(2)
        % Axial axis
        set(OV_IH(1),'CData',OV_ADATA(:,:,iiz)');
        % Coronal Axis
        set(OV_IH(2),'CData',squeeze(OV_ADATA(:,iiy,:))');
        % Sagittal Axis
        set(OV_IH(3),'CData',squeeze(OV_ADATA(iix,:,:))');
      end
      % keyboard
      if ~isempty(OV_FDATA) & OV_DISPLAY(3)
        % Axial axis
        ftmp = (((OV_FDATA(:,:,fiiz)-fmn)/(fmx-fmn))* ...
                (DEFAULT_CMAPSIZE(2)-1)+DEFAULT_CMAPSIZE(1)+ ...
                1)';
        set(OV_IH(4),'CData',ftmp)
        set(OV_IH(4),'AlphaData',fmaskx)
        % Coronal Axis
        ftmp = (((squeeze(OV_FDATA(:,fiiy,:))-fmn)/(fmx-fmn))* ...
                (DEFAULT_CMAPSIZE(2)-1)+DEFAULT_CMAPSIZE(1)+1)';
        set(OV_IH(5),'CData',ftmp)
        set(OV_IH(5),'AlphaData',fmasky);
        % Sagittal Axis
        ftmp = (((squeeze(OV_FDATA(fiix,:,:))-fmn)/(fmx-fmn))* ...
                (DEFAULT_CMAPSIZE(2)-1)+DEFAULT_CMAPSIZE(1)+1)';
        set(OV_IH(6),'CData',ftmp)
        set(OV_IH(6),'AlphaData',fmaskz)
      elseif ~OV_DISPLAY(3)
        set(OV_IH(4),'AlphaData',zeros(size(fmaskx)));
        set(OV_IH(5),'AlphaData',zeros(size(fmasky)));
        set(OV_IH(6),'AlphaData',zeros(size(fmaskz)));
      end
      
      if OV_DISPLAY(5)
        % Axial axis
        ftmp = get(OV_IH(4),'CData');
        ind = find(OV_MASK(:,:,fiiz)');
        ftmp(ind) = (sum(DEFAULT_CMAPSIZE)+1)*ones;
        set(OV_IH(4),'CData',ftmp);
        ftmp = get(OV_IH(4),'AlphaData');
        ftmp(ind) = ones;
        set(OV_IH(4),'AlphaData',ftmp);
        % Coronal axis
        ftmp = get(OV_IH(5),'CData');
        ind = find(squeeze(OV_MASK(:,fiiy,:))');
        ftmp(ind) = (sum(DEFAULT_CMAPSIZE)+1)*ones;
        set(OV_IH(5),'CData',ftmp);
        ftmp = get(OV_IH(5),'AlphaData');
        ftmp(ind) = ones;
        set(OV_IH(5),'AlphaData',ftmp);
        % Axial 
        ftmp = get(OV_IH(6),'CData');
        ind = find(squeeze(OV_MASK(fiix,:,:))');
        ftmp(ind) = (sum(DEFAULT_CMAPSIZE)+1)*ones;
        set(OV_IH(6),'CData',ftmp);
        ftmp = get(OV_IH(6),'AlphaData');
        ftmp(ind) = ones;
        set(OV_IH(6),'AlphaData',ftmp);
        
        
      end
      
    else
      % Render images without transparency (i.e. don't use opengl)
      if ~isempty(OV_ADATA)
        if ~isempty(OV_FDATA)
          % Axial axis
          if OV_DISPLAY(2)
            img = OV_ADATA(:,:,iiz)';
          else
            img = zeros(size(OV_ADATA,2),size(OV_ADATA,1),'uint8');
          end
          if OV_DISPLAY(3)
            fimg = interp2(OV_FPOSX,OV_FPOSY',...
                           (((OV_FDATA(:,:,fiiz)-fmn)/(fmx-fmn))* ...
                            (DEFAULT_CMAPSIZE(2)-1)+DEFAULT_CMAPSIZE(1))' .* ...
                           fmaskx,OV_APOSX,OV_APOSY','nearest');
            qq = find(fimg~=0 & ~isnan(fimg));
            img(qq) = uint8(fimg(qq));
          end
          set(OV_IH(1),'CData',img);
          % Coronal Axis
          if OV_DISPLAY(2)
            img = squeeze(OV_ADATA(:,iiy,:))';
          else
            img = zeros(size(OV_ADATA,3),size(OV_ADATA,1),'uint8');
          end
          if OV_DISPLAY(3)
            fimg = interp2(OV_FPOSX,OV_FPOSZ',...
                           (((squeeze(OV_FDATA(:,fiiy,:))-fmn)/(fmx-fmn))* ...
                            (DEFAULT_CMAPSIZE(2)-1)+DEFAULT_CMAPSIZE(1))' .* ...
                           fmasky,OV_APOSX,OV_APOSZ','nearest');
            qq = find(fimg~=0 & ~isnan(fimg));
            img(qq) = uint8(fimg(qq));
          end
          set(OV_IH(2),'CData',img);
          % Sagittal Axis
          if OV_DISPLAY(2)
            img = squeeze(OV_ADATA(iix,:,:))';
          else
            img = zeros(size(OV_ADATA,3),size(OV_ADATA,2),'uint8');
          end
          if OV_DISPLAY(3)
            fimg = interp2(OV_FPOSY,OV_FPOSZ',...
                           (((squeeze(OV_FDATA(fiix,:,:))-fmn)/(fmx-fmn))* ...
                            (DEFAULT_CMAPSIZE(2)-1)+DEFAULT_CMAPSIZE(1))' .* ...
                           fmaskz,OV_APOSY,OV_APOSZ','nearest');            
            qq = find(fimg~=0 & ~isnan(fimg));
            img(qq) = uint8(fimg(qq));
          end
          set(OV_IH(3),'CData',img);
        else
          if OV_DISPLAY(2)
            % Axial axis
            set(OV_IH(1),'CData',OV_ADATA(:,:,iiz)');
            % Coronal Axis
            set(OV_IH(2),'CData',squeeze(OV_ADATA(:,iiy,:))');
            % Sagittal Axis
            set(OV_IH(3),'CData',squeeze(OV_ADATA(iix,:,:))');
          end
        end
      else
        if ~isempty(OV_FDATA) & OV_DISPLAY(3)
          % Axial axis
          set(OV_IH(1),'CData',...
                       (((OV_FDATA(:,:,fiiz)-fmn)/(fmx-fmn))* ...
                        (DEFAULT_CMAPSIZE(2)-1)+DEFAULT_CMAPSIZE(1))' .* ...
                       fmaskx + 1)  
          % Coronal Axis
          set(OV_IH(2),'CData',...
                       (((squeeze(OV_FDATA(:,fiiy,:))-fmn)/(fmx-fmn))* ...
                        (DEFAULT_CMAPSIZE(2)-1)+DEFAULT_CMAPSIZE(1))' .* ...
                       fmasky + 1)
          % Sagittal Axis
          set(OV_IH(3),'CData',...
                       (((squeeze(OV_FDATA(fiix,:,:))-fmn)/(fmx-fmn))* ...
                        (DEFAULT_CMAPSIZE(2)-1)+DEFAULT_CMAPSIZE(1))' .* ...
                       fmaskz + 1)          
        end
      end
    end
    
    % Cross hairs
    % Axial axis
    xlim = get(OV_AXH(1),'xlim');
    ddd = (xlim(2)-xlim(1))/10;
    set(OV_LH(1),'xdata',[xlim(1) OV_CPOINT(1)-ddd],...
                 'ydata',[OV_CPOINT(2) OV_CPOINT(2)]);
    set(OV_LH(2),'xdata',[OV_CPOINT(1)+ddd xlim(2)],...
                 'ydata',[OV_CPOINT(2) OV_CPOINT(2)]);
    ylim = get(OV_AXH(1),'ylim');
    set(OV_LH(3),'ydata',[ylim(1) OV_CPOINT(2)-ddd],...
                 'xdata',[OV_CPOINT(1) OV_CPOINT(1)]);
    set(OV_LH(4),'ydata',[OV_CPOINT(2)+ddd ylim(2)],...
                 'xdata',[OV_CPOINT(1) OV_CPOINT(1)]);
    % Coronal Axis    
    xlim = get(OV_AXH(2),'xlim');
    set(OV_LH(5),'xdata',[xlim(1) OV_CPOINT(1)-ddd],...
                 'ydata',[OV_CPOINT(3) OV_CPOINT(3)]);
    set(OV_LH(6),'xdata',[OV_CPOINT(1)+ddd xlim(2)],...
                 'ydata',[OV_CPOINT(3) OV_CPOINT(3)]);
    ylim = get(OV_AXH(2),'ylim');
    set(OV_LH(7),'ydata',[ylim(1) OV_CPOINT(3)-ddd],...
                 'xdata',[OV_CPOINT(1) OV_CPOINT(1)]);
    set(OV_LH(8),'ydata',[OV_CPOINT(3)+ddd ylim(2)],...
                 'xdata',[OV_CPOINT(1) OV_CPOINT(1)]);
    % Sagittal Axis
    xlim = get(OV_AXH(3),'xlim');
    set(OV_LH(9),'xdata',[xlim(1) OV_CPOINT(2)-ddd],...
                 'ydata',[OV_CPOINT(3) OV_CPOINT(3)]);
    set(OV_LH(10),'xdata',[OV_CPOINT(2)+ddd xlim(2)],...
                 'ydata',[OV_CPOINT(3) OV_CPOINT(3)]);
    ylim = get(OV_AXH(3),'ylim');
    set(OV_LH(11),'ydata',[ylim(1) OV_CPOINT(3)-ddd],...
                 'xdata',[OV_CPOINT(2) OV_CPOINT(2)]);
    set(OV_LH(12),'ydata',[OV_CPOINT(3)+ddd ylim(2)],...
                 'xdata',[OV_CPOINT(2) OV_CPOINT(2)]);

   case 'clearimages'
    if DEFAULT_OPENGL
      [m,n] = size(get(OV_IH(1),'Cdata'));
      set(OV_IH(1),'Cdata',zeros(m,n));
      [m,n] = size(get(OV_IH(2),'Cdata'));
      set(OV_IH(2),'Cdata',zeros(m,n));
      [m,n] = size(get(OV_IH(3),'Cdata'));
      set(OV_IH(3),'Cdata',zeros(m,n));      
      [m,n] = size(get(OV_IH(4),'Cdata'));
      set(OV_IH(4),'Cdata',zeros(m,n));
      [m,n] = size(get(OV_IH(5),'Cdata'));
      set(OV_IH(5),'Cdata',zeros(m,n));
      [m,n] = size(get(OV_IH(6),'Cdata'));
      set(OV_IH(6),'Cdata',zeros(m,n));    
    else
      [m,n] = size(get(OV_IH(1),'Cdata'));
      set(OV_IH(1),'Cdata',zeros(m,n));
      [m,n] = size(get(OV_IH(2),'Cdata'));
      set(OV_IH(2),'Cdata',zeros(m,n));
      [m,n] = size(get(OV_IH(3),'Cdata'));
      set(OV_IH(3),'Cdata',zeros(m,n));
    end
    
    
   case 'drawthresh'
    % Draw threshold on colobar
    if ~isempty(OV_FDATA)
      % Calculated max and min of fdata
      if isnan(OV_TH(6))
        if OV_TH(5) == 1
          fmx = max(abs(OV_TH(2:3)));
          fmn = -fmx;
        else
          fmx = OV_TH(3);
          fmn = OV_TH(2);
        end
      else
        fmn = OV_TH(5);
        fmx = OV_TH(6);
      end
      lpp = (DEFAULT_CMAPSIZE(2)-1)*((OV_TH(2)-fmn)/(fmx-fmn))+1;
      set(OV_LH(13),'ydata',[lpp lpp]);
      lpp = (DEFAULT_CMAPSIZE(2)-1)*((OV_TH(3)-fmn)/(fmx-fmn))+1;
      set(OV_LH(14),'ydata',[lpp lpp]);
      lpp = (DEFAULT_CMAPSIZE(2)-1)*((OV_TH(1)-fmn)/(fmx-fmn))+1;
      if (OV_TH(4) == 1 || OV_TH(4) == 3)
        set(OV_LH(15),'Ydata',[lpp lpp lpp+DEFAULT_CMAPSIZE(2)/40]);
      else
        set(OV_LH(15),'Ydata',[lpp lpp lpp-DEFAULT_CMAPSIZE(2)/40]);
      end
      if (OV_TH(4) == 3 || OV_TH(4) == 4)
        lpp = (DEFAULT_CMAPSIZE(2)-1)*((-OV_TH(1)-fmn)/(fmx-fmn))+1;
        if (OV_TH(4) == 3)
          if isnan(OV_LH(16))
            axes(OV_AXH(4))
            OV_LH(16) = patch([1.6 1.9 1.75]',...
                              [lpp lpp lpp-DEFAULT_CMAPSIZE(2)/40],...
                              'k','clipping','off');
          else
            set(OV_LH(16),'Ydata',[lpp lpp lpp-DEFAULT_CMAPSIZE(2)/40]);
          end
        else
          if isnan(OV_LH(16))
            axes(OV_AXH(4))
            OV_LH(16) = patch([1.6 1.9 1.75]',...
                              [lpp lpp lpp+DEFAULT_CMAPSIZE(2)/40],...
                              'k','clipping','off');
          else          
            set(OV_LH(16),'Ydata',[lpp lpp lpp+DEFAULT_CMAPSIZE(2)/40]);
          end
        end
      else
        if ~isnan(OV_LH(16))
          delete(OV_LH(16));
          OV_LH(16) = nan;
        end
      end
    end
    
   case 'mousedown'
    axhandle = gca;
    ii = find(axhandle == OV_AXH(1:3));
    if isempty(ii)
      return
    end
    button = get(gcf,'SelectionType');
    point = get(OV_AXH(ii),'CurrentPoint');
    point = point(1,1:2);
    xlim = get(OV_AXH(ii),'xlim');
    ylim = get(OV_AXH(ii),'ylim');
    if point(1) < xlim(1) || point(1) > xlim(2)
      return
    end
    if point(2) < ylim(1) || point(2) > ylim(2)
      return
    end
    if strcmp(button,'normal')
      % left mouse button
      switch ii
       case 1
        OV_CPOINT(1) = point(1);
        OV_CPOINT(2) = point(2);
       case 2
        OV_CPOINT(1) = point(1);
        OV_CPOINT(3) = point(2);        
       case 3
        OV_CPOINT(2) = point(1);
        OV_CPOINT(3) = point(2);        
      end
    elseif strcmp(button,'alt')
      if (OV_VEUIH(1))
        if OV_VEUIH(2) == 1
          switch ii
           case 1
            [tmp,fSX] = sort(abs(OV_FPOSX-point(1)));
            [tmp,fSY] = sort(abs(OV_FPOSY-point(2)));
            [tmp,fSZ] = sort(abs(OV_FPOSZ-OV_CPOINT(3)));
           case 2
            [tmp,fSX] = sort(abs(OV_FPOSX-point(1)));
            [tmp,fSY] = sort(abs(OV_FPOSY-OV_CPOINT(2)));
            [tmp,fSZ] = sort(abs(OV_FPOSZ-point(2)));
           case 3
            [tmp,fSX] = sort(abs(OV_FPOSX-OV_CPOINT(1)));
            [tmp,fSY] = sort(abs(OV_FPOSY-point(1)));
            [tmp,fSZ] = sort(abs(OV_FPOSZ-point(2)));
          end
          OV_FDATA(fSX(1:OV_VEUIH(4)),fSY(1:OV_VEUIH(4)),...
                   fSZ(1:OV_VEUIH(4))) = OV_VEUIH(3);
          if OV_VEUIH(5)
            orthoview('processthresh');
            orthoview('drawthresh');
            OV_VEUIH(5) = 0;
          end
        end
      end
    end
    orthoview('drawslices');
    orthoview('updatesliders');
    orthoview('updateeditboxes');
    
   case 'poscallback'
    xp = str2double(get(OV_UIH(1),'string'));
    if ~isnan(xp)
      OV_CPOINT(1) = xp;
    end
    yp = str2double(get(OV_UIH(2),'string'));
    if ~isnan(yp)
      OV_CPOINT(2) = yp;
    end
    zp = str2double(get(OV_UIH(3),'string'));
    if ~isnan(zp)
      OV_CPOINT(3) = zp;
    end
    orthoview('drawslices');
    orthoview('updatesliders');
    orthoview('updateeditboxes');

   case 'voxeditcallback'
    [tmp,fSX] = min(abs(OV_FPOSX-OV_CPOINT(1)));
    [tmp,fSY] = min(abs(OV_FPOSY-OV_CPOINT(2)));
    [tmp,fSZ] = min(abs(OV_FPOSZ-OV_CPOINT(3)));
    
    xp = str2double(get(OV_UIH(11),'string'));
    if ~isnan(xp)
      xp = round(xp);
      if (xp ~= fSX) && (xp > 0 && xp <= length(OV_FPOSX))
        OV_CPOINT(1) = OV_FPOSX(xp);
      end
    end
    yp = str2double(get(OV_UIH(12),'string'));
    if ~isnan(yp)
      yp = round(yp);
      if (yp ~= fSY) && (yp > 0 && yp <= length(OV_FPOSY))
        OV_CPOINT(2) = OV_FPOSY(yp);
      end
    end    
    zp = str2double(get(OV_UIH(13),'string'));
    if ~isnan(zp)
      zp = round(zp);
      if (zp ~= fSZ) && (zp > 0 && zp <= length(OV_FPOSZ))
        OV_CPOINT(3) = OV_FPOSZ(zp);
      end
    end

    orthoview('drawslices');
    orthoview('updatesliders');
    orthoview('updateeditboxes');    
    
   case 'updateeditboxes'
    set(OV_UIH(1),'string',num2str(OV_CPOINT(1),'%.1f'));
    set(OV_UIH(2),'string',num2str(OV_CPOINT(2),'%.1f'));
    set(OV_UIH(3),'string',num2str(OV_CPOINT(3),'%.1f'));
    if ~isempty(OV_FDATA)
      [tmp,fSX] = min(abs(OV_FPOSX-OV_CPOINT(1)));
      [tmp,fSY] = min(abs(OV_FPOSY-OV_CPOINT(2)));
      [tmp,fSZ] = min(abs(OV_FPOSZ-OV_CPOINT(3)));
      set(OV_UIH(11),'string',num2str(fSX));
      set(OV_UIH(12),'string',num2str(fSY));
      set(OV_UIH(13),'string',num2str(fSZ));
      set(OV_UIH(14),'string',num2str(OV_FDATA(fSX,fSY,fSZ)));
      if (OV_UIH(15))
        indtmp = find(OV_FINDEX == sub2ind(size(OV_FDATA),fSX,fSY,fSZ));
        set(OV_UIH(15),'string',num2str(indtmp));
      end
      controller('send','orthoview','voxel',...
                 [fSX fSY fSZ]);
    end
    
   case 'slidercallback'
    tog = varargin{2};
    switch tog
     case 1
      val = round(get(OV_UIH(4),'value'));
      set(OV_UIH(5),'string',num2str(val));
      if ~isempty(OV_ADATA)
        OV_CPOINT(3) = OV_APOSZ(val);
      else
        if ~isempty(OV_FDATA)
          OV_CPOINT(3) = OV_FPOSZ(val);
        else
          OV_CPOINT(3) = val;
        end
      end
     case 2
      val = round(get(OV_UIH(6),'value'));
      set(OV_UIH(7),'string',num2str(val));
      if ~isempty(OV_ADATA)
        OV_CPOINT(2) = OV_APOSY(val);
      else
        if ~isempty(OV_FDATA)
          OV_CPOINT(2) = OV_FPOSY(val);
        else
          OV_CPOINT(2) = val;
        end
      end
     case 3
      val = round(get(OV_UIH(8),'value'));
      set(OV_UIH(9),'string',num2str(val));
      if ~isempty(OV_ADATA)
        OV_CPOINT(1) = OV_APOSX(val);
      else
        if ~isempty(OV_FDATA)
          OV_CPOINT(1) = OV_FPOSX(val);
        else
          OV_CPOINT(1) = val;
        end
      end
    end
    orthoview('drawslices');
    orthoview('updateeditboxes');

   case 'updatesliders'
    if ~isempty(OV_ADATA)
      [tmp,iix] = min(abs(OV_APOSX-OV_CPOINT(1)));
      [tmp,iiy] = min(abs(OV_APOSY-OV_CPOINT(2)));
      [tmp,iiz] = min(abs(OV_APOSZ-OV_CPOINT(3)));
    else
      if ~isempty(OV_FDATA)
        [tmp,iix] = min(abs(OV_FPOSX-OV_CPOINT(1)));
        [tmp,iiy] = min(abs(OV_FPOSY-OV_CPOINT(2)));
        [tmp,iiz] = min(abs(OV_FPOSZ-OV_CPOINT(3)));
      else
        iix = round(OV_CPOINT(1)+120.5);
        iiy = round(OV_CPOINT(2)+120.5);
        iiz = round(OV_CPOINT(3)+120.5);
      end
    end
    set(OV_UIH(4),'value',iiz);
    set(OV_UIH(5),'string',num2str(iiz));
    set(OV_UIH(6),'value',iiy);
    set(OV_UIH(7),'string',num2str(iiy));
    set(OV_UIH(8),'value',iix);
    set(OV_UIH(9),'string',num2str(iix));

   case 'thresheditcallback'
    tmp = str2double(get(OV_UIH(10),'string'));
    if ~isempty(tmp)
      OV_TH(1) = tmp;
      orthoview('drawslices');
      orthoview('drawthresh');
    end

   case 'threshtypecallback'
    toggle = get(gcbo,'value');
    OV_TH(4) = toggle;
    orthoview('drawslices');
    orthoview('drawthresh');
    
   case 'scalingcallback'
    toggle = get(gcbo,'value');
    if toggle == 1
      OV_TH(5) = 1;
      OV_TH(6) = NaN;
    elseif toggle == 2
      OV_TH(5) = 2;
      OV_TH(6) = NaN;
    else
      
    end
    orthoview('drawslices');
    orthoview('drawthresh');

   case 'rbafcallback'
    tog = varargin{2};
    if tog == 1
      OV_DISPLAY(2) = get(OV_UIH(16),'Value');
      if OV_DISPLAY(2)
        set(OV_UIH(19),'checked','on');
      else
        set(OV_UIH(19),'checked','off');
      end
    elseif tog == 2
      OV_DISPLAY(3) = get(OV_UIH(17),'Value');
      if OV_DISPLAY(3)
        set(OV_UIH(20),'checked','on');
      else
        set(OV_UIH(20),'checked','off');
      end
    elseif tog == 3
      OV_DISPLAY(4) = get(OV_UIH(18),'Value');
      if OV_DISPLAY(4)
        set(OV_UIH(21),'checked','on');
      else
        set(OV_UIH(21),'checked','off');
      end
    else
      error('Unknown option');
    end
    orthoview('clearimages');
    orthoview('drawslices');
    
   case 'vmenucallback'
    % Callback for view menu
    tog = varargin{2};
    switch tog
     case 1
      % Anatomical
      if OV_DISPLAY(2)
        set(OV_UIH(16),'Value',0);
        set(OV_UIH(19),'Checked','off')
        OV_DISPLAY(2) = 0;
      else
        set(OV_UIH(16),'Value',1);
        set(OV_UIH(19),'Checked','on')
        OV_DISPLAY(2) = 1;
      end
     case 2
      % Functional
      if OV_DISPLAY(3)
        set(OV_UIH(17),'Value',0);
        set(OV_UIH(20),'Checked','off')
        OV_DISPLAY(3) = 0;
      else
        set(OV_UIH(17),'Value',1);
        set(OV_UIH(20),'Checked','on')
        OV_DISPLAY(3) = 1;
      end
     case 3
      % Mask mode
      if OV_DISPLAY(4)
        set(OV_UIH(18),'Value',0);
        set(OV_UIH(21),'Checked','off')
        OV_DISPLAY(4) = 0;
      else
        set(OV_UIH(18),'Value',1);
        set(OV_UIH(21),'Checked','on')
        OV_DISPLAY(4) = 1;
      end
     case 4
      % Cross hairs
      if OV_DISPLAY(1)
        set(OV_UIH(22),'Checked','off')
        OV_DISPLAY(1) = 0;
        set(OV_LH(1:12),'visible','off');
      else
        set(OV_UIH(22),'Checked','on')
        OV_DISPLAY(1) = 1;
        set(OV_LH(1:12),'visible','on');
      end
     case 5
      % View mask
      if OV_DISPLAY(5)
        set(OV_UIH(23),'Checked','off')
        OV_DISPLAY(5) = 0;
      else
        set(OV_UIH(23),'Checked','on')
        OV_DISPLAY(5) = 1;
      end
     otherwise
      error('Unknown option');
    end
    orthoview('clearimages');
    orthoview('drawslices');
    
   case 'setanat'
    % 'setanat',data)
    %          ,data,index)
    %          ,data,isize,vsize,orient)
    %          ,data,isize,vsize,orient,index)
    %          ,filename)
    %          ,filename,orient)
    adata = varargin{2};
    if ischar(adata)
      if ~isempty(findstr('.img',adata))
        [adata,avsize] = readanalyze(adata);
        aisize = size(adata);
        if numargs > 2
          aorient = varargin{3};
        else
          aorient = DEFAULT_ORIENT;
        end
      elseif ~isempty(findstr('.nii',adata))
        [adata,avsize,aorient] = readnifti(adata);
        aisize = size(adata);
      else
        error('Error parsing arguments');
      end
      if numargs > 2
        aorient = varargin{3};
      end
      orthoview('processanat',adata,aisize,avsize,aorient);
    else
      
    end
    orthoview('resetimageaxes');
    orthoview('drawslices');
    
   case 'setfunc'
    % 'setfunc',data)
    %          ,data,index)
    %          ,data,isize,vsize,orient)
    %          ,data,isize,vsize,orient,index)
    %          ,filename)
    %          ,filename,orient)
    if isempty(OV_FDATA)
      newvolume = 1;
    else
      newvolume = 0;
    end
    fdata = varargin{2};
    if ischar(fdata)
      if ~isempty(findstr('.img',fdata))
        [fdata,fvsize] = readanalyze(fdata);
        fisize = size(fdata);
        if numargs > 2
          forient = varargin{3};
        else
          forient = DEFAULT_ORIENT;
        end
      elseif ~isempty(findstr('.nii',fdata))
        [fdata,fvsize,forient] = readnifti(fdata);
        fisize = size(fdata);
      else
        error('Error parsing arguments');
      end
      if numargs > 2
        forient = varargin{3};
      end
      orthoview('processfunc',fdata,fisize,fvsize,forient);
      
    else
      switch numargs
       case 2
        % Assume that the matrix is the same size and orientation
        if length(size(fdata)) == 3
          OV_FDATA = fdata;
        else
          if isempty(OV_FINDEX)
            OV_FDATA = reshape(fdata,size(OV_FDATA));
          else
            if length(fdata) == length(OV_FINDEX)
              OV_FDATA = zeros(size(OV_FDATA));
              OV_FDATA(OV_FINDEX) = fdata;
            else
              OV_FDATA = reshape(fdata,size(OV_FDATA));
            end
          end
        end
       case 3
        % Assume that the matrix is the same size and orientation but the
        % index is different.
        index = varargin{3};
        OV_FDATA = zeros(size(OV_FDATA));
        OV_FDATA(index) = fdata;
        OV_FINDEX = index;
       case 5
        fisize = varargin{3};
        fvsize = varargin{4};
        forient = varargin{5};
        orthoview('processfunc',fdata,fisize,fvsize,forient);
       case 6
        fisize = varargin{3};
        fvsize = varargin{4};
        forient = varargin{5};
        findex = varargin{6};
        orthoview('processfunc',fdata,fisize,fvsize,forient,findex);
       otherwise
        error('Incorrect number of arguments');
      end
    end
    orthoview('processthresh');
    if newvolume
      orthoview('createfuncui');
    end
    orthoview('resetimageaxes');
    orthoview('drawslices');
    orthoview('drawthresh');
       
   case 'setthresh'
    OV_TH(1) = varargin{2};
    orthoview('drawslices');
    orthoview('drawthresh');
    set(OV_UIH(10),'string',num2str(OV_TH(1)));
    
   case 'setposition'
    OV_CPOINT = varargin{2};
    orthoview('drawslices');
    orthoview('updatesliders');
    orthoview('updateeditboxes');    
   
   case 'setcolormap'

   case 'closefigure'
    figh = get(OV_AXH(1),'parent');
    clear global OV_ADATA OV_APOSX OV_APOSY OV_APOSZ OV_CPOINT OV_IH OV_AXH OV_LH
    clear global OV_FDATA OV_FPOSX OV_FPOSY OV_FPOSZ OV_UIH OV_TH
    clear global OV_VEUIH OV_MEUIH OV_MASK OV_FDATA_OLD
    delete(figh);
    oh = findobj('tag','oveditvolgui');
    if ~isempty(oh)
      delete(oh);
    end
    oh = findobj('tag','oveditmaskgui');
    if ~isempty(oh)
      delete(oh);
    end
    
    
   case 'processanat'
    adata = varargin{2};
    aisize = varargin{3};
    avsize = varargin{4};
    aorient = varargin{5};
    cmapsize = DEFAULT_CMAPSIZE;
    if numargs < 6
      aindex = [];
    else
      aindex = varargin{6};
    end
    % keyboard
    if ~isempty(adata)
      % Anatomical
      if length(size(adata)) == 3
        if isempty(aisize)
          aisize = size(adata);
        else
          if ~all(aisize == size(adata))
            error('Incorrect aisize');
          end
        end
      else
        if ~isempty(aindex)
          if isempty(aisize)
            error('No aisize set')
          else
            tmp = zeros(aisize);
            tmp(aindex) = adata;
            adata = tmp;
            clear tmp;
            % to do: I might want to optimize this to be a uint8
          end
        else
          if isempty(aisize)
            error('No aisize set')
          else
            adata = reshape(adata,aisize);
          end
        end
      end
      % Get rotation matrix
      if isstruct(aorient)
        % aR = quat2rot(aorient.qmatrix,aorient.qfac);
        aR = aorient.smatrix;
        % Todo: I should put something here that checks whether we should use
        % the qmatrix or smatrix.
        % aR(1:3,1:3) = aR(1:3,1:3).*repmat(avsize(:)',3,1);
      else
        if length(aorient) == 1
          if aorient == 0
            aR = eye(4);
            aR(1:3,1:3) = aR(1:3,1:3).*repmat(avsize(:)',3,1);
          else
            error('Unknown aorient');
          end
        elseif length(aorient) == 3
          aR = eye(4);
          aR(1:3,4) = aorient(:);
          aR(1:3,1:3) = aR(1:3,1:3).*repmat(avsize(:)',3,1);
        elseif length(aorient) == 6
          aR = quat2rot(aorient,1);
          aR(1:3,1:3) = aR(1:3,1:3).*repmat(avsize(:)',3,1);
        elseif length(aorient) == 7
          aR = quat2rot(aorient(1:6),aorient(7));
          aR(1:3,1:3) = aR(1:3,1:3).*repmat(avsize(:)',3,1);
        else
          aR = aorient;
        end
      end
      % Check if we have off diagonal terms in the rotation matrix
      tmp = sum(abs(aR(:,1:3)) > SMALLNUM);
      if all(tmp == 1)
        % In this case we simply have to flip and possibly exchange the 3
        % dimensions.
        % Permute data
        order = zeros(1,3);
        order(1) = find(abs(aR(1,1:3)) > SMALLNUM);
        order(2) = find(abs(aR(2,1:3)) > SMALLNUM);
        order(3) = find(abs(aR(3,1:3)) > SMALLNUM);
        adata = permute(adata,order);
        aisize = aisize(order);
        avsize = avsize(order);
        % Figure out bounds
        aaa = [zeros(3,aisize(1));ones(1,aisize(1))];
        aaa(order(1),:) = 0:(aisize(1)-1);
        aaa = aR*aaa;
        aposx = aaa(1,:);
        aaa = [zeros(3,aisize(2));ones(1,aisize(2))];
        aaa(order(2),:) = 0:(aisize(2)-1);
        aaa = aR*aaa;
        aposy = aaa(2,:);
        aaa = [zeros(3,aisize(3));ones(1,aisize(3))];
        aaa(order(3),:) = 0:(aisize(3)-1);
        aaa = aR*aaa;
        aposz = aaa(3,:);
      else
        % Since we have off diagonal terms the volume needs to be rotated to
        % get to standard LPI space.
        error('Not implemented yet');  
      end
      
      % Scale anatomical and convert to 8-bit unsigned integer
      
      % Note: matlab images in double format are 1-based referenced, and
      % those in uint8 are 0-based referenced, therefore data is between 0
      % and cmapsize(1)-1.
      if DEFAULT_ASCALE_CLIP
        tmp = sort(adata(:));
        % keyboard
        aval = tmp(round(DEFAULT_ASCALE_CLIP_VAL*length(tmp)));
        ii = find(adata > aval);
        adata(ii) = aval*ones;
      end
      amax = double(max(max(max(adata))));
      amin = double(min(min(min(adata))));
      % keyboard
      OV_ADATA = uint8((cmapsize(1)-1)*(double(adata)-amin)/(amax-amin));

      OV_APOSX = aposx;
      OV_APOSY = aposy;
      OV_APOSZ = aposz;
    else
      OV_ADATA = [];
      OV_APOSX = [];
      OV_APOSY = [];
      OV_APOSZ = [];
    end
    
   case 'processfunc'
    fdata = double(varargin{2});
    fisize = varargin{3};
    fvsize = varargin{4};
    forient = varargin{5};
    if numargs < 6
      findex = [];
    else
      findex = varargin{6};
    end
    cmapsize = DEFAULT_CMAPSIZE;
    OV_FINDEX = [];
    if ~isempty(fdata)
      if length(size(fdata)) == 3
        if isempty(fisize)
          fisize = size(fdata);
        else
          if ~all(fisize == size(fdata))
            error('Incorrect fisize');
          end
        end
      else
        if ~isempty(findex)
          OV_FINDEX = findex;
          if isempty(fisize)
            error('No fisize set')
          else
            tmp = zeros(fisize);
            tmp(findex) = fdata;
            fdata = tmp;
          end
        else
          if isempty(fisize)
            error('No fisize set')
          else
            fdata = reshape(fdata,fisize);
          end
        end
      end
      % Get rotation matrix      
      if isstruct(forient)
        % fR = quat2rot(forient.qmatrix,forient.qfac);
        fR = forient.smatrix;
        % Todo: I should put something here that checks whether we should use
        % the qmatrix or smatrix.
        % fR(1:3,1:3) = fR(1:3,1:3).*repmat(fvsize(:)',3,1);
        
      else
        if length(forient) == 1
          if forient == 0
            fR = eye(4);
            fR(1:3,1:3) = fR(1:3,1:3).*repmat(fvsize(:)',3,1);
          else
            error('Unknown aorient');
          end
        elseif length(forient) == 3
          fR = eye(4);
          fR(1:3,4) = forient(:);
          fR(1:3,1:3) = fR(1:3,1:3).*repmat(fvsize(:)',3,1);
        elseif length(forient) == 6
          fR = quat2rot(forient,1);
          fR(1:3,1:3) = fR(1:3,1:3).*repmat(fvsize(:)',3,1);
        elseif length(forient) == 7
          fR = quat2rot(forient(1:6),forient(7));
          fR(1:3,1:3) = fR(1:3,1:3).*repmat(fvsize(:)',3,1);
        else
          fR = forient;
        end
      end
      % Check if we have off diagonal terms in the rotation matrix
      tmp = sum(abs(fR(:,1:3)) > SMALLNUM);

      if all(tmp == 1)
        % In this case we simply have to flip and possibly exchange the 3
        % dimensions.
        % Permute data
        order = zeros(1,3);
        order(1) = find(abs(fR(1,1:3)) > SMALLNUM);
        order(2) = find(abs(fR(2,1:3)) > SMALLNUM);
        order(3) = find(abs(fR(3,1:3)) > SMALLNUM);
        fdata = permute(fdata,order);
        fisize = fisize(order);
        fvsize = fvsize(order);
        % Figure out bounds
        aaa = [zeros(3,fisize(1));ones(1,fisize(1))];
        aaa(order(1),:) = 0:(fisize(1)-1);
        aaa = fR*aaa;
        fposx = aaa(1,:);
        aaa = [zeros(3,fisize(2));ones(1,fisize(2))];
        aaa(order(2),:) = 0:(fisize(2)-1);
        aaa = fR*aaa;
        fposy = aaa(2,:);
        aaa = [zeros(3,fisize(3));ones(1,fisize(3))];
        aaa(order(3),:) = 0:(fisize(3)-1);
        aaa = fR*aaa;
        fposz = aaa(3,:);
      else
        % Since we have off diagonal terms the volume needs to be rotated to
        % get to standard LPI space.
        error('Not implemented yet');  
      end
      OV_FDATA = fdata;

      OV_FPOSX = fposx;
      OV_FPOSY = fposy;
      OV_FPOSZ = fposz;
    else
      OV_FDATA = [];
      OV_FPOSX = [];
      OV_FPOSY = [];
      OV_FPOSZ = [];
    end

   case 'processmask'
    if isempty(OV_FDATA)
      error('Functional data is undefined');
    end
    mask = varargin{2};
    if length(size(mask)) == 3
      if all(size(mask) == size(OV_FDATA))
        OV_MASK = mask;
      else
        error('Mask size must be the same as functional size');
      end
    else
      if length(mask) == length(OV_FDATA(:))
        OV_MASK = reshape(mask,size(OV_FDATA));
      elseif length(mask) == length(OV_FINDEX)
        OV_MASK = zeros(size(OV_FDATA));
        OV_MASK(OV_FINDEX) = mask;
      else
        error('Mask size must be the same as functional size');
      end
    end
    
    
   case 'processthresh'
    if numargs > 1
      fth = varargin{2};
      fthtype = varargin{3};
      fscaling = varargin{4};
      if length(fscaling) == 1
        fscaling = [fscaling NaN];
      end
    else
      if ~isempty(OV_TH)
        fth = OV_TH(1);
        fthtype = OV_TH(4);
        fscaling = OV_TH(5:6);
      else
        fth = DEFAULT_THRESH;
        switch lower(DEFAULT_THRESHOLDTYPE)
         case 'p'
          fthtype = 1;
         case 'n'
          fthtype = 2;
         case 'pn'
          fthtype = 3;
         case 'np'
          fthtype = 4;
         otherwise
          error('Unknown option for threshold type');
        end
        fscaling = DEFAULT_SCALING;
        if ischar(fscaling)
          switch lower(fscaling)
           case 'absmax'
             fscaling = 1;
           case 'maxmin'
            fscaling = 2;
           otherwise
            error('Unknown option for scaling');
          end
          fscaling = [fscaling NaN];
        end
      end
    end

    if ~isempty(OV_FDATA)
      OV_TH = [fth min(min(min(OV_FDATA))) max(max(max(OV_FDATA))) ...
               fthtype fscaling(1:2)];
    end

   case 'resetimageaxes'

    if ~isempty(OV_ADATA)
      set(OV_IH(1),'XData',OV_APOSX,'YData',OV_APOSY,...
                   'CData',ones(length(OV_APOSY),length(OV_APOSX)));
      set(OV_IH(2),'XData',OV_APOSX,'YData',OV_APOSZ,...
                   'CData',ones(length(OV_APOSZ),length(OV_APOSX)));
      set(OV_IH(3),'XData',OV_APOSY,'YData',OV_APOSZ,...
                   'CData',ones(length(OV_APOSZ),length(OV_APOSY)));
      if ~isempty(OV_FDATA) && DEFAULT_OPENGL
        set(OV_IH(4),'XData',OV_FPOSX,'YData',OV_FPOSY,...
                     'CData',ones(length(OV_FPOSY),length(OV_FPOSX)));
        set(OV_IH(5),'XData',OV_FPOSX,'YData',OV_FPOSZ,...
                     'CData',ones(length(OV_FPOSZ),length(OV_FPOSX)));
        set(OV_IH(6),'XData',OV_FPOSY,'YData',OV_FPOSZ,...
                     'CData',ones(length(OV_FPOSZ),length(OV_FPOSY)));
      end
    else
      if ~isempty(OV_FDATA)
        if DEFAULT_OPENGL
          set(OV_IH(4),'XData',OV_FPOSX,'YData',OV_FPOSY,...
                       'CData',ones(length(OV_FPOSY),length(OV_FPOSX)));
          set(OV_IH(5),'XData',OV_FPOSX,'YData',OV_FPOSZ,...
                       'CData',ones(length(OV_FPOSZ),length(OV_FPOSX)));
          set(OV_IH(6),'XData',OV_FPOSY,'YData',OV_FPOSZ,...
                       'CData',ones(length(OV_FPOSZ),length(OV_FPOSY)));
        else

          set(OV_IH(1),'XData',OV_FPOSX,'YData',OV_FPOSY,...
                       'CData',ones(length(OV_FPOSY),length(OV_FPOSX)));
          set(OV_IH(2),'XData',OV_FPOSX,'YData',OV_FPOSZ,...
                       'CData',ones(length(OV_FPOSZ),length(OV_FPOSX)));
          set(OV_IH(3),'XData',OV_FPOSY,'YData',OV_FPOSZ,...
                       'CData',ones(length(OV_FPOSZ),length(OV_FPOSY)));
        end
      end
    end
    limits = orthoview('calculatefov',DEFAULT_FOV);
    set(OV_AXH(1),'xlim',limits(1:2),'ylim',limits(3:4));
    set(OV_AXH(2),'xlim',limits(1:2),'ylim',limits(5:6));
    set(OV_AXH(3),'xlim',limits(3:4),'ylim',limits(5:6));
    
   case 'calculatefov'
    % Calculate a FOV if a default one is not set
    fov = varargin{2};
    if ~isempty(OV_ADATA)
      avsize = abs([OV_APOSX(2)-OV_APOSX(1),OV_APOSY(2)-OV_APOSY(1),...
                    OV_APOSZ(2)-OV_APOSZ(1)]);
    end
    if ~isempty(OV_FDATA)
      fvsize = abs([OV_FPOSX(2)-OV_FPOSX(1),OV_FPOSY(2)-OV_FPOSY(1),...
                    OV_FPOSZ(2)-OV_FPOSZ(1)]);
    end
    
    if ~isempty(OV_ADATA)
      if ~isempty(OV_FDATA)
        if ischar(fov)
          fov = max([max([OV_APOSX OV_FPOSX])-min([OV_APOSX OV_FPOSX]) ...
                     max([OV_APOSY OV_FPOSY])-min([OV_APOSY OV_FPOSY]) ...
                     max([OV_APOSZ OV_FPOSZ])-min([OV_APOSZ OV_FPOSZ])]);
          
        end
        xlim = (max([OV_APOSX OV_FPOSX])+min([OV_APOSX OV_FPOSX]))/2;
        xlim = [xlim-fov/2-avsize(1)/2 xlim+fov/2+avsize(1)/2];
        ylim = (max([OV_APOSY OV_FPOSY])+min([OV_APOSY OV_FPOSY]))/2;
        ylim = [ylim-fov/2-avsize(2)/2 ylim+fov/2+avsize(2)/2];
        zlim = (max([OV_APOSZ OV_FPOSZ])+min([OV_APOSZ OV_FPOSZ]))/2;
        zlim = [zlim-fov/2-avsize(3)/2 zlim+fov/2+avsize(3)/2];
      else
        if ischar(fov)
          fov = max([max(OV_APOSX)-min(OV_APOSX) ...
                     max(OV_APOSY)-min(OV_APOSY) ...
                     max(OV_APOSZ)-min(OV_APOSZ)]);      
        end
        xlim = (max(OV_APOSX)+min(OV_APOSX))/2;
        xlim = [xlim-fov/2-avsize(1)/2 xlim+fov/2+avsize(1)/2];
        ylim = (max(OV_APOSY)+min(OV_APOSY))/2;
        ylim = [ylim-fov/2-avsize(2)/2 ylim+fov/2+avsize(2)/2];
        zlim = (max(OV_APOSZ)+min(OV_APOSZ))/2;
        zlim = [zlim-fov/2-avsize(3)/2 zlim+fov/2+avsize(3)/2];    
      end  
    else
      if ~isempty(OV_FDATA)
        if ischar(fov)
          fov = max([max(OV_FPOSX)-min(OV_FPOSX) ...
                     max(OV_FPOSY)-min(OV_FPOSY) ...
                     max(OV_FPOSZ)-min(OV_FPOSZ)]);      
        end
        xlim = (max(OV_FPOSX)+min(OV_FPOSX))/2;
        xlim = [xlim-fov/2-fvsize(1)/2 xlim+fov/2+fvsize(1)/2];
        ylim = (max(OV_FPOSY)+min(OV_FPOSY))/2;
        ylim = [ylim-fov/2-fvsize(2)/2 ylim+fov/2+fvsize(2)/2];
        zlim = (max(OV_FPOSZ)+min(OV_FPOSZ))/2;
        zlim = [zlim-fov/2-fvsize(3)/2 zlim+fov/2+fvsize(3)/2];
        
      else
        if ischar(fov)
          fov = 240;
        end
        xlim = [-120 120];
        ylim = [-120 120];
        zlim = [-120 120];
        
      end
    end
    o1 = [xlim ylim zlim];
    o2 = fov;
    
   case 'createfuncui'
    % Set of UI controls to display when there is a functional volume
    % loaded.
    asizex = .327;
    asizey = .38;
    xpos2 = .55;
    xpos1 = .08;
    ypos2 = .578;
    ypos1 = .09;
    % xyzd = fov/10;
    editheight = .06;
    textheight = .06;
    sliderheight = .04;
    poseditlength = .08;
    cmapsize = DEFAULT_CMAPSIZE;
    bgcolor = get(gcf,'color');
    vsbpad = .09;
    
    % Threshold colorbar and controls
    % Threshold edit box
    % colorbar axis
    OV_AXH(4) = axes('position',[xpos2-.09 ypos1 asizex/6 asizey]);
    image(((cmapsize(1)+1):(sum(cmapsize)))');
    set(OV_AXH(4),'ydir','normal','xtick',[],'YAxisLocation','left',...
                  'ytick',linspace(1,cmapsize(2),5),'yticklabel',[],...
                   'xlim',[0.5 2.0])
    axis off
    % Threshold edit box
    OV_UIH(10) = uicontrol('style','edit',...
                           'string',num2str(OV_TH(1)),'units',...
                           'normalized','position',...
                           [xpos2 ypos1+asizey*.5-.03 .08 .06],...
                           'callback',...
                           'orthoview(''thresheditcallback'')');
    % Threshold type popupmenu
    valtmp = OV_TH(4);
    uicontrol('style','popupmenu','units','normalized',...
              'position',[xpos2 ypos1+asizey*.5-.11 .08 .06],...
              'string',{'p','n','pn','np','eq'},'Value',valtmp,...
              'callback','orthoview(''threshtypecallback'');');
    % Scaling popup menu
    if isnan(OV_TH(6))
      valtmp = OV_TH(5);
    else
      valtmp = 3;
    end
    uicontrol('style','popupmenu','units','normalized',...
              'position',[xpos2 ypos1+asizey*.5+.05 .08 .06],...
              'string',{'am','mm','ud'},'Value',valtmp,...
              'callback','orthoview(''scalingcallback'');');
    % threshold lines and patches
    if isnan(OV_TH(6))
      if OV_TH(5) == 1
        fmx = max(abs(OV_TH(2:3)));
        fmn = -fmx;
      else
        fmx = OV_TH(3);
        fmn = OV_TH(2);
      end
    else
      fmn = OV_TH(5);
      fmx = OV_TH(6);
    end
    
    lpp = (DEFAULT_CMAPSIZE(2)-1)*((OV_TH(2)-fmn)/(fmx-fmn))+1;
    OV_LH(13) = line([1.6 1.9],[lpp lpp],'color','r','clipping','off',...
                     'linewidth',2);
    lpp = (DEFAULT_CMAPSIZE(2)-1)*((OV_TH(3)-fmn)/(fmx-fmn))+1;
    OV_LH(14) = line([1.6 1.9],[lpp lpp],'color','r','clipping','off',...
                     'linewidth',2);
    lpp = (DEFAULT_CMAPSIZE(2)-1)*((OV_TH(1)-fmn)/(fmx-fmn))+1;
    if (OV_TH(4) == 1 || OV_TH(4) == 3)
      OV_LH(15) = patch([1.6 1.9 1.75]',[lpp lpp lpp+DEFAULT_CMAPSIZE(2)/40],...
                        'k','clipping','off');
    else
      OV_LH(15) = patch([1.6 1.9 1.75]',[lpp lpp lpp-DEFAULT_CMAPSIZE(2)/40],...
                        'k','clipping','off');
    end
    if (OV_TH(4) == 3 || OV_TH(4) == 4)
      lpp = (DEFAULT_CMAPSIZE(2)-1)*((-OV_TH(1)-fmn)/(fmx-fmn))+1;
      if (OV_TH(4) == 3)
        OV_LH(16) = patch([1.6 1.9 1.75]',[lpp lpp lpp-DEFAULT_CMAPSIZE(2)/40],...
                          'k','clipping','off');
      else
        OV_LH(16) = patch([1.6 1.9 1.75]',[lpp lpp lpp+DEFAULT_CMAPSIZE(2)/40],...
                          'k','clipping','off');
      end
    else
      OV_LH(16) = nan;
    end    
    
    % Voxel string and edit boxes
    uicontrol('style','text','string','Voxel','units',...
              'normalized','BackgroundColor',bgcolor,'position',...
              [xpos2+vsbpad ypos1+asizey*.6 .08 textheight]);
    [tmp,fSX] = min(abs(OV_FPOSX-OV_CPOINT(1)));
    [tmp,fSY] = min(abs(OV_FPOSY-OV_CPOINT(2)));
    [tmp,fSZ] = min(abs(OV_FPOSZ-OV_CPOINT(3)));
    
    OV_UIH(11) = uicontrol('style','edit','string',num2str(fSX),...
                           'units','normalized',...
                           'callback','orthoview(''voxeditcallback'');',...
                           'position',...
                           [xpos2+.08+vsbpad ypos1+asizey*.6 .06 editheight]);
    OV_UIH(12) = uicontrol('style','edit','string',num2str(fSY),...
                           'units','normalized',...
                           'callback','orthoview(''voxeditcallback'');',...
                           'position',...
                           [xpos2+.08*2+vsbpad ypos1+asizey*.6 .06 editheight]);
    OV_UIH(13) = uicontrol('style','edit','string',num2str(fSZ),...
                           'units','normalized',...
                           'callback','orthoview(''voxeditcallback'');',...
                           'position',...
                           [xpos2+.08*3+vsbpad ypos1+asizey*.6 .06 editheight]);
    
    
    % Value string and edit box
    uicontrol('style','text','string','Value','units',...
              'normalized','BackgroundColor',bgcolor,'position',...
              [xpos2+vsbpad ypos1+asizey*.35 .08 textheight]);
    OV_UIH(14) = uicontrol('style','edit','string',num2str(OV_FDATA(fSX,fSY,fSZ)),...
                           'units','normalized',...
                           'position',...
                           [xpos2+.08+vsbpad ypos1+asizey*.35 .12 editheight]);
    if ~isempty(OV_FINDEX)
      % Index string and edit box
      indtmp = find(OV_FINDEX == sub2ind(size(OV_FDATA),fSX,fSY,fSZ));
      uicontrol('style','text','string','Index','units',...
                'normalized','BackgroundColor',bgcolor,'position',...
                [xpos2+vsbpad ypos1+asizey*.1 .08 textheight]);
      OV_UIH(15) = uicontrol('style','edit','string',num2str(indtmp),...
                             'units','normalized',...
                             'position',...
                             [xpos2+.08+vsbpad ypos1+asizey*.1 .12 editheight]);  
    else
      OV_UIH(15) = 0;
    end
    
   case 'exporttofigure'
    figh = figure;
    fpos = get(figh,'position');
    fpos(4) = round(fpos(3)*.33333333);
    set(figh,'position',fpos);
    figho = get(OV_AXH(1),'parent');
    ax1 = subplot(1,3,1);
    ih1 = image;
    tmp = get(OV_IH(1),'Cdata');
    set(ih1,'Cdata',tmp,'Xdata',get(OV_IH(1),'Xdata'),...
            'Ydata',get(OV_IH(1),'Ydata'));
    set(ax1,'xlim',get(OV_AXH(1),'xlim'),...
            'ylim',get(OV_AXH(1),'ylim'),'color','k',...
            'tickdir','out');
    
    ax2 = subplot(1,3,2);
    ih2 = image;
    tmp = get(OV_IH(2),'Cdata');
    set(ih2,'Cdata',tmp,'Xdata',get(OV_IH(2),'Xdata'),...
            'Ydata',get(OV_IH(2),'Ydata'));
    set(ax2,'xlim',get(OV_AXH(2),'xlim'),...
            'ylim',get(OV_AXH(2),'ylim'),'color','k',...
            'tickdir','out');
    
    ax3 = subplot(1,3,3);
    ih3 = image;
    tmp = get(OV_IH(3),'Cdata');
    set(ih3,'Cdata',tmp,'Xdata',get(OV_IH(3),'Xdata'),...
            'Ydata',get(OV_IH(3),'Ydata'));
    set(ax3,'xlim',get(OV_AXH(3),'xlim'),...
            'ylim',get(OV_AXH(3),'ylim'),'color','k',...
            'tickdir','out');
    
    set(figh,'colormap',get(figho,'colormap'));
    
    
   case 'dialog'
    % Open up a general purpose dialog
    % orthoview('dialog','message',string);
    % string = orthoview('dialog','edit',string);
    % string = orthoview('dialog','editfile',string);
    mtype = lower(varargin{2});
    displaystring = varargin{3};
    
    TIMESTRING2 = ['set(gcbf,''UserData'',''Cancel'');',...
		   'uiresume(gcbf);'];    
    figx = 400;
    figy = 200;
    if ~isempty(get(0,'children'))
      fig = gcf;
      pos = get(fig,'Position');
      figh = figure('Units','pixels',...
                    'Position',...
                    [pos(1)+pos(3)/2-figx/2 pos(2)+pos(4)/2-figy/2 figx figy],...
                    'Resize','off','CloseRequestFcn',...
                    TIMESTRING2,'menubar','none',...
                    'numbertitle','off');
    else
      figh = figure('Units','pixels',...
                    'Resize','off','CloseRequestFcn',...
                    TIMESTRING2,'menubar','none',...
                    'numbertitle','off');
      pos = get(figh,'Position');
      set(figh,'Position',...
               [pos(1)+pos(3)/2-figx/2 pos(2)+pos(4)/2-figy/2 figx ...
                figy]);
    end
    uicolor = get(figh,'Color');    
    
    % text uicontrol
    uicontrol('Style','Text','Units','Pixels',...
              'String',displaystring,...
              'Position',[20 figy-40 300 22],'HorizontalAlignment','left',...
              'FontSize',14,'BackGroundColor',uicolor)
    if strcmp(mtype(1:4),'edit')
      % edit uicontrol
      ui1 = uicontrol('Style','Edit','Units','Pixels',...
                      'FontSize',12,...
                      'Position',[100 figy-100 170 30]);
      
      
    end
    if strcmp(mtype,'editfile')
      uicontrol('Style','pushbutton','Units','Pixels',...
                'fontsize',12,...
                'position',[290 figy-100 80 30],...
                'string','Choose','userdata',ui1,...
                'callback',...
                ['[FFFF_NAME1,PPPP_NAME1]=uigetfile(''*'');'...
                 'set(get(gcbo,''userdata''),''string'',[PPPP_NAME1,FFFF_NAME1]);'...
                'clear FFFF_NAME1 PPPP_NAME1;']);
      
    end
    
    TIMESTRING = ['set(gcbf,''UserData'',''OK'');',...
                    'uiresume(gcbf);'];
    % OK Button
    uicontrol('Style','PushButton','Units','Pixels',...
              'String','OK','FontSize',14,...
              'Position',[figx/4-20 10 65 30],...
              'UserData',[ui1 fig],'Callback',TIMESTRING)
    
    % Cancel Button
    uicontrol('Style','PushButton','Units','Pixels',...
              'String','Cancel','FontSize',14,...
              'Position',[3*figx/4-20 10 65 30],...
              'Callback',TIMESTRING2)      
      
    uiwait(figh);
    if strcmp(get(figh,'UserData'),'OK')
      o1 = get(ui1,'String');
    else
      o1 = [];
    end
    delete(figh)

   case 'editvolgui'
    if ~isempty(findobj('tag','oveditvolgui')) | isempty(OV_FDATA)
      return
    end
    % Copy data so we can restore if needed
    OV_FDATA_OLD = OV_FDATA;
    
    figh = figure('tag','oveditvolgui','menubar','none',...
                  'numbertitle','off','name','Volume Editor',...
                  'units','pixels');
    figx = 400;
    figy = 400;
    pos = get(figh,'position');
    pos(3:4) = [figx figy];
    set(figh,'position',pos);
    bgcolor = get(gcf,'color');
    
    uicontrol('style','text','Units','pixels',...
              'position',[10 figy-50 380 30],...
              'string','Volume Editor','backgroundcolor',bgcolor,...
              'HorizontalAlignment','left');
    % OV_VEUIH(5) = uicontrol('style','radiobutton','Units','pixels',...
    %                         'position',[20 figy-80 170 30],...
    %                         'string','Functional','backgroundcolor',bgcolor,...
    %                         'selectionhighlight','off',...
    %                         'callback','orthoview(''editvolguihandler'',1);');
    % OV_VEUIH(6) = uicontrol('style','radiobutton','Units','pixels',...
    %                         'position',[200 figy-80 170 30],...
    %                         'string','Mask','backgroundcolor',bgcolor,...
    %                         'selectionhighlight','off',...
    %                         'callback','orthoview(''editvolguihandler'',2);');
    
    % OV_VEUIH(1) : Draw mode on/off
    %         (2) : Brush or Box
    %         (3) : Value
    %         (4) : radius
    %         (5) : New value
    
    % if varargin{2} == 2
    %   set(OV_VEUIH(5),'Value',1);
    %   OV_VEUIH(1) = 2; % Functional
    % else
    %   set(OV_VEUIH(6),'Value',1);
    %   OV_VEUIH(1) = 1; % Mask
    % end
    OV_VEUIH(8) = uicontrol('style','radiobutton','Units','pixels',...
                            'position',[10 figy-100 380 30],...
                            'string','Brush','backgroundcolor',bgcolor,...
                            'HorizontalAlignment','left','Value',1,...
                            'callback','orthoview(''editvolguihandler'',4);');
    uicontrol('style','text','Units','pixels',...
              'position',[20 figy-160 60 30],...
              'string','Size','backgroundcolor',bgcolor,...
              'HorizontalAlignment','left');
    OV_VEUIH(7) = uicontrol('style','edit','Units','pixels',...
                            'position',[80 figy-160 50 30],...
                            'string','1',...
                            'HorizontalAlignment','left','callback',...
                            'orthoview(''editvolguihandler'',3);');
    
    OV_VEUIH(9) = uicontrol('style','radiobutton','Units','pixels',...
                            'position',[10 figy-200 380 30],...
                            'string','Drag Box','backgroundcolor',bgcolor,...
                            'HorizontalAlignment','left',...
                            'callback','orthoview(''editvolguihandler'',5);');
    
    uicontrol('style','text','Units','pixels',...
              'position',[10 figy-260 90 30],...
              'string','Values','backgroundcolor',bgcolor,...
              'HorizontalAlignment','left');
    OV_VEUIH(10) = uicontrol('style','radiobutton','Units','pixels',...
                             'position',[30 figy-290 50 30],...
                             'string','0','backgroundcolor',bgcolor,...
                             'HorizontalAlignment','left','value',1,...
                             'callback','orthoview(''editvolguihandler'',6);');
    OV_VEUIH(11) = uicontrol('style','radiobutton','Units','pixels',...
                             'position',[80 figy-290 50 30],...
                             'string','1','backgroundcolor',bgcolor,...
                             'HorizontalAlignment','left',...
                             'callback','orthoview(''editvolguihandler'',7);');
    OV_VEUIH(12) = uicontrol('style','radiobutton','Units','pixels',...
                             'position',[130 figy-290 100 30],...
                             'string','Other','backgroundcolor',bgcolor,...
                             'HorizontalAlignment','left',...
                             'callback','orthoview(''editvolguihandler'',8);');
    OV_VEUIH(13) = uicontrol('style','edit','Units','pixels',...
                             'position',[230 figy-290 80 30],...
                             'string','',...
                             'HorizontalAlignment','left',...
                             'callback','orthoview(''editvolguihandler'',8);');
    
    uicontrol('style','pushbutton','Units','pixels',...
              'position',[30 figy-350 80 30],...
              'string','Reset','callback',...
              'orthoview(''editvolguihandler'',9)');
    uicontrol('style','pushbutton','Units','pixels',...
              'position',[230 figy-350 80 30],...
              'string','Close','callback',...
              'orthoview(''editvolguihandler'',10)');
    
    
    OV_VEUIH(1) = 1;
    OV_VEUIH(2) = 1;
    OV_VEUIH(3) = 0;
    OV_VEUIH(4) = 1;
    OV_VEUIH(5) = 1;
    
   case 'editvolguihandler'
    switch varargin{2}
     % case 1
     %  set(OV_VEUIH(5),'Value',1,'selected','off');
     %  set(OV_VEUIH(6),'Value',0);
     %  OV_VEUIH(1) = 1;
     % case 2
     %  set(OV_VEUIH(5),'Value',0);
     %  set(OV_VEUIH(6),'Value',1,'selected','off');
     %  OV_VEUIH(1) = 2;
     case 3
      tmp = str2double(get(OV_VEUIH(7),'string'));
      if ~isempty(tmp)
        OV_VEUIH(4) = round(tmp);
      end
     case 4
      set(OV_VEUIH(8),'Value',1);
      set(OV_VEUIH(9),'Value',0);
      OV_VEUIH(2) = 1;
     case 5
      set(OV_VEUIH(8),'Value',0);
      set(OV_VEUIH(9),'Value',1);
      OV_VEUIH(2) = 2;
     case 6
      set(OV_VEUIH(10),'Value',1);
      set(OV_VEUIH(11),'Value',0);
      set(OV_VEUIH(12),'Value',0);
      OV_VEUIH(3) = 0;
      OV_VEUIH(5) = 1;
     case 7
      set(OV_VEUIH(10),'Value',0);
      set(OV_VEUIH(11),'Value',1);
      set(OV_VEUIH(12),'Value',0);
      OV_VEUIH(3) = 1;
      OV_VEUIH(5) = 1;
     case 8
      set(OV_VEUIH(10),'Value',0);
      set(OV_VEUIH(11),'Value',0);
      set(OV_VEUIH(12),'Value',1);
      tmp = str2double(get(OV_VEUIH(13),'string'));
      if ~isempty(tmp)
        OV_VEUIH(3) = tmp;
        OV_VEUIH(5) = 1;
      end
     case 9
      % Reset
      OV_FDATA = OV_FDATA_OLD;
      orthoview('processthresh');
      orthoview('drawslices');
      orthoview('drawthresh');
     case 10
      % Close
      oh = findobj('tag','oveditvolgui');
      delete(oh);
      OV_VEUIH = zeros(size(OV_VEUIH));
      
    end

   case 'editmaskgui'
    if ~isempty(findobj('tag','oveditmaskgui')) | isempty(OV_FDATA)
      return
    end
    
    figh = figure('tag','oveditmaskgui','menubar','none',...
                  'numbertitle','off','name','Mask Editor',...
                  'units','pixels');
    figx = 400;
    figy = 400;
    pos = get(figh,'position');
    pos(3:4) = [figx figy];
    set(figh,'position',pos);
    bgcolor = get(gcf,'color');
    
    uicontrol('style','text','Units','pixels',...
              'position',[10 figy-50 380 30],...
              'string','Mask Editor','backgroundcolor',bgcolor,...
              'HorizontalAlignment','left');
    %OV_VEUIH(5) = uicontrol('style','radiobutton','Units','pixels',...
    %                        'position',[20 figy-80 170 30],...
    %                        'string','Functional','backgroundcolor',bgcolor,...
    %                        'selectionhighlight','off',...
    %                        'callback','orthoview(''editvolguihandler'',1);');
    %OV_VEUIH(6) = uicontrol('style','radiobutton','Units','pixels',...
    %                        'position',[200 figy-80 170 30],...
    %                        'string','Mask','backgroundcolor',bgcolor,...
    %                        'selectionhighlight','off',...
    %                        'callback','orthoview(''editvolguihandler'',2);');
    
    % OV_VEUIH(1) : Func or Mask
    %         (2) : Brush or Box
    %         (3) : Value
    %if varargin{2} == 2
    %  set(OV_VEUIH(5),'Value',1);
    %  OV_VEUIH(1) = 2; % Functional
    %else
    %  set(OV_VEUIH(6),'Value',1);
    %  OV_VEUIH(1) = 1; % Mask
    %end
    OV_VEUIH(8) = uicontrol('style','radiobutton','Units','pixels',...
                            'position',[10 figy-130 380 30],...
                            'string','Brush','backgroundcolor',bgcolor,...
                            'HorizontalAlignment','left','Value',1,...
                            'callback','orthoview(''editvolguihandler'',4);');
    uicontrol('style','text','Units','pixels',...
              'position',[20 figy-190 60 30],...
              'string','Size','backgroundcolor',bgcolor,...
              'HorizontalAlignment','left');
    OV_VEUIH(7) = uicontrol('style','edit','Units','pixels',...
                            'position',[80 figy-190 50 30],...
                            'string','1',...
                            'HorizontalAlignment','left','callback',...
                            'orthoview(''editvolguihandler'',3);');
    
    OV_VEUIH(9) = uicontrol('style','radiobutton','Units','pixels',...
                            'position',[10 figy-230 380 30],...
                            'string','Box','backgroundcolor',bgcolor,...
                            'HorizontalAlignment','left',...
                            'callback','orthoview(''editvolguihandler'',5);');
    
    uicontrol('style','text','Units','pixels',...
              'position',[10 figy-290 90 30],...
              'string','Values','backgroundcolor',bgcolor,...
              'HorizontalAlignment','left');
    OV_VEUIH(10) = uicontrol('style','radiobutton','Units','pixels',...
                             'position',[30 figy-320 50 30],...
                             'string','0','backgroundcolor',bgcolor,...
                             'HorizontalAlignment','left','value',1,...
                             'callback','orthoview(''editvolguihandler'',6);');
    OV_VEUIH(11) = uicontrol('style','radiobutton','Units','pixels',...
                             'position',[80 figy-320 50 30],...
                             'string','1','backgroundcolor',bgcolor,...
                             'HorizontalAlignment','left',...
                             'callback','orthoview(''editvolguihandler'',7);');
    OV_VEUIH(12) = uicontrol('style','radiobutton','Units','pixels',...
                             'position',[130 figy-320 100 30],...
                             'string','Other','backgroundcolor',bgcolor,...
                             'HorizontalAlignment','left',...
                             'callback','orthoview(''editvolguihandler'',8);');
    OV_VEUIH(13) = uicontrol('style','edit','Units','pixels',...
                             'position',[230 figy-320 80 30],...
                             'string','',...
                             'HorizontalAlignment','left',...
                             'callback','orthoview(''editvolguihandler'',8);');
    
    OV_VEUIH(2) = 1;
    OV_VEUIH(3) = 0;
    OV_VEUIH(4) = 1;
    
   case 'editmaskguihandler'
    switch varargin{2}
     case 1
      set(OV_VEUIH(5),'Value',1,'selected','off');
      set(OV_VEUIH(6),'Value',0);
      OV_VEUIH(1) = 1;
     case 2
      set(OV_VEUIH(5),'Value',0);
      set(OV_VEUIH(6),'Value',1,'selected','off');
      OV_VEUIH(1) = 2;
     case 3
      tmp = str2double(get(OV_VEUIH(7),'string'));
      if ~isempty(tmp)
        OV_VEUIH(4) = round(tmp);
      end
     case 4
      set(OV_VEUIH(8),'Value',1);
      set(OV_VEUIH(9),'Value',0);
      OV_VEUIH(2) = 1;
     case 5
      set(OV_VEUIH(8),'Value',0);
      set(OV_VEUIH(9),'Value',1);
      OV_VEUIH(2) = 2;
     case 6
      set(OV_VEUIH(10),'Value',1);
      set(OV_VEUIH(11),'Value',0);
      set(OV_VEUIH(12),'Value',0);
      OV_VEUIH(3) = 0;
     case 7
      set(OV_VEUIH(10),'Value',0);
      set(OV_VEUIH(11),'Value',1);
      set(OV_VEUIH(12),'Value',0);
      OV_VEUIH(3) = 1;
     case 8
      set(OV_VEUIH(10),'Value',0);
      set(OV_VEUIH(11),'Value',0);
      set(OV_VEUIH(12),'Value',1);
      tmp = str2double(get(OV_VEUIH(13),'string'));
      if ~isempty(tmp)
        OV_VEUIH(3) = tmp;
      end
    end

   case 'histogram'
    if ~isempty(OV_FDATA)
      figure;
      hist(OV_FDATA(:),20);
      title('Histogram');
    end
    
   otherwise
    subroutine = 0;
  end
  if subroutine
    % if we did run a subroutine then exit script
    return;
  end
end

if firststring > 0
  numvargs = firststring-1;
else
  numvargs = numargs;
end


adata = [];
aisize = [];
avsize = [];
aorient = [];
aindex = [];
fdata = [];
fisize = [];
fvsize = [];
forient = [];
findex = [];
vsize = [];
isize = [];
orient = [];
index = [];
cmap = [];
fth = [];
fthtype = [];
fscaling = [];
cpoint = [];
mask = [];

if numvargs > 0
  adata = varargin{1};
  if ischar(adata)
    % if adata is a string then we have a filename
    if ~isempty(findstr('.img',adata))
      [adata,avsize] = readanalyze(adata);
      aisize = size(adata);
    elseif ~isempty(findstr('.nii',adata))
      [adata,avsize,aorient] = readnifti(adata);
      aisize = size(adata);
    else
      error('Error parsing arguments');
    end
  end
  if numvargs > 1
    if iscell(varargin{2})
      % Read volume information from cell array
      % {isize,vsize,orient,index} if vector
      % {vsize,orient} if 3d
      % todo: add {filename}
      if length(size(adata)) == 3
        % if adata is a 3-d matrix
        avsize = varargin{2}{1};
        if length(varargin{2}) > 1
          aorient = varargin{2}{2};
        end
        aisize = size(adata);
      else
        % else adata is a vector and aisize is a required argument
        aisize = varargin{2}{1};
        if length(varargin{2}) > 1
          avsize = varargin{2}{2};
          if length(varargin{2}) > 2
            aorient = varargin{2}{3};
            if length(varargin{2}) > 3
              aindex = varargin{2}{4};
            end
          end
        end
        % keyboard
      end
      fstart = 3; % the functional volume should start at the 3rd arg
    elseif ischar(varargin{2})
      % Read volume information from file
      % if ~isempty(findstr('.img',varargin{2}))
      %   avsize = readanalyze(varargin{2},'info');
      % elseif ~isempty(findstr('.nii',varargin{2}))
      %   [avsize,aorient] = readnifti(varargin{2},'info');
      % else
      %   error('Error parsing arguments');
      % end
      % fstart = 3;
      if ~isempty(findstr('.mat',varargin{2}))
        
      else
        fstart = 2;
      end
    else
      fstart = 2;
    end
    if numvargs >= fstart
      fdata = varargin{fstart};
      if ischar(fdata)
        if ~isempty(findstr('.img',fdata))
          [fdata,fvsize] = readanalyze(fdata);
          fisize = size(fdata);
        elseif ~isempty(findstr('.nii',fdata))
          [fdata,fvsize,forient] = readnifti(fdata);
          fisize = size(fdata);
        else
          error('Error parsing arguments');
        end
      end
      % keyboard
      if numvargs > fstart
        if iscell(varargin{fstart+1})
          % Read volume information from cell array
          if length(size(fdata)) == 3
            if length(varargin{fstart+1}) ~= 2
              error('Cell array with image information should be length 2');
            end
            fvsize = varargin{fstart+1}{1};
            if length(varargin{fstart+1}) > 1
              forient = varargin{fstart+1}{2};
            end
            fisize = size(fdata);
           
          else
            
            fisize = varargin{fstart+1}{1};
            if length(varargin{fstart+1}) > 1
              fvsize = varargin{fstart+1}{2};
              if length(varargin{fstart+1}) > 2
                forient = varargin{fstart+1}{3};
                if length(varargin{fstart+1}) > 3
                  findex = varargin{fstart+1}{4};
                end
              end
            end
          end
        elseif ischar(varargin{fstart+1})
          % Read volume information from file
          if ~isempty(findstr('.img',varargin{fstart+1}))
            fvsize = readanalyze(varargin{fstart+1},'info');
          elseif ~isempty(findstr('.nii',varargin{fstart+1}))
            [fisize,fvsize,forient] = readnifti(varargin{fstart+1},'info');
            % keyboard
          else
            error('Error parsing arguments');
          end          
        else
          if (length(size(adata)) == 3) & (length(size(fdata)) == 3)
            estart = fstart +1;
          else
            isize = varargin{fstart+1};
            estart = fstart+2;
          end
          % if (length(size(adata)) < 3) | (length(size(fdata)) < 3)
          %   isize = varargin{fstart+1};
          %   estart = fstart+2;
          % else
          %   estart = fstart +1;
          % end
          if numvargs >= estart
            vsize = varargin{estart};
            if numvargs >= estart + 1
              orient = varargin{estart + 1};
              if numvargs >= estart +2
                index = varargin{estart+2};
              end
            end
          end
        end      
      end
    end
  end
end

if firststring
  vind = firststring;
  % numargs
  while (vind <= numargs)
    % fprintf('%s\n',varargin{vind});
    carg = lower(varargin{vind});
    switch carg
     case 'isize'
      isize = varargin{vind+1};
      vind = vind + 2;
     case 'vsize'
      vsize = varargin{vind+1};
      vind = vind + 2;
     case 'orient'
      orient = varargin{vind+1};
      vind = vind + 2;
     case 'aisize'
      aisize = varargin{vind+1};
      vind = vind + 2;
     case 'avsize'
      avsize = varargin{vind+1};
      vind = vind + 2;
     case 'aorient'
      aorient = varargin{vind+1};
      vind = vind + 2;
     case 'fisize'
      fisize = varargin{vind+1};
      vind = vind + 2;
     case 'fvsize'
      fvsize = varargin{vind+1};
      vind = vind + 2;
     case 'forient'
      forient = varargin{vind+1};
      vind = vind + 2;
     case 'thresh'
      fth = varargin{vind+1};
      vind = vind + 2;
     case 'thtype'
      fthtype = varargin{vind+1};
      vind = vind + 2;
     case 'scaling'
      fscaling = varargin{vind+1};
      vind = vind + 2;
     case 'position'
      cpoint = varargin{vind+1};
      vind = vind + 2;
     case 'mask'
      mask = varargin{vind+1};
      vind = vind + 2;
     case 'cmap'
      cmap = varargin{vind+1};
      vind = vind + 2;
     otherwise
      error(['Unknown option: ',carg]);
      
      
    end

  end
end
% isize
if ~isempty(isize)
  if isempty(aisize)
    aisize = isize;
  end
  if isempty(fisize)
    fisize = isize;
  end
end
if ~isempty(vsize)
  if isempty(avsize)
    avsize = vsize;
  end
  if isempty(fvsize)
    fvsize = vsize;
  end
end
if ~isempty(orient)
  if isempty(aorient)
    aorient = orient;
  end
  if isempty(forient)
    forient = orient;
  end
end
if ~isempty(index)
  if isempty(aindex)
    aindex = index;
  end
  if isempty(findex)
    findex = index;
  end
end

if isempty(avsize)
  avsize = DEFAULT_VSIZE;
end
if isempty(fvsize)
  fvsize = DEFAULT_VSIZE;
end
if isempty(aorient)
  aorient = DEFAULT_ORIENT;
end
if isempty(forient)
  forient = DEFAULT_ORIENT;
end
if isempty(fth)
  fth = DEFAULT_THRESH;
end
if isempty(fthtype)
  fthtype = DEFAULT_THRESHOLDTYPE;
end
if isempty(fscaling)
  fscaling = DEFAULT_SCALING;
end

switch lower(fthtype)
 case 'p'
  fthtype = 1;
 case 'n'
  fthtype = 2;
 case 'pn'
  fthtype = 3;
 case 'np'
  fthtype = 4;
 case 'eq'
  fthtype = 5;
 otherwise
  error('Unknown option for threshold type');
end
if ischar(fscaling)
  switch lower(fscaling)
   case 'absmax'
    fscaling = 1;
   case 'maxmin'
    fscaling = 2;
   otherwise
    error('Unknown option for scaling');
  end
end
  

% $$$ adata
% $$$ aisize
% $$$ avsize
% $$$ aorient
% $$$ fdata
% $$$ fisize
% $$$ fvsize
% $$$ forient

% Colormap
cmapsize = DEFAULT_CMAPSIZE;
if isempty(cmap)
  % cmap = [gray(cmapsize(1));...
  %         jet(cmapsize(2));...
  %         DEFAULT_MASK_COLOR];

  cmap = [eval([DEFAULT_ACMAP,'(cmapsize(1))']);...
          eval([DEFAULT_FCMAP,'(cmapsize(2))']);...
          DEFAULT_MASK_COLOR];
else
  if isstr(cmap)
    cmap = [eval([DEFAULT_ACMAP,'(cmapsize(1))']);...
            eval([cmap,'(cmapsize(2))']);...
            DEFAULT_MASK_COLOR];
  else
    
  end
end


orthoview('processanat',adata,aisize,avsize,aorient,aindex);
orthoview('processfunc',fdata,fisize,fvsize,forient,findex);
if ~isempty(mask)
  orthoview('processmask',mask);
end
orthoview('processthresh',fth,fthtype,fscaling);


if ~isempty(OV_ADATA)
  aisize = size(OV_ADATA);
end
if ~isempty(OV_FDATA)
  fisize = size(OV_FDATA);
end


% Some of the following stuff might be removed
if isempty(OV_APOSX)
  aposx = -120:2:120;
  aposy = -120:2:120;
  aposz = -120:2:120;
  aisize = [128 128 128];
else
  aposx = OV_APOSX;
  aposy = OV_APOSY;
  aposz = OV_APOSZ;
end
if isempty(OV_FPOSX)
  fposx = -120:2:120;
  fposy = -120:2:120;
  fposz = -120:2:120;
  fisize = [128 128 128];
else
  fposx = OV_FPOSX;
  fposy = OV_FPOSY;
  fposz = OV_FPOSZ;
end
fov = DEFAULT_FOV;

% Find a suitable starting point for the crosshairs
%   By default we use the center of the anatomical volume. If that
%   doesn't exist, we use the functional volume, or else [0 0 0]
if isempty(cpoint)
  if ~isempty(adata)
    OV_CPOINT = [aposx(ceil(length(aposx)/2)) aposy(ceil(length(aposy)/2)) ...
                 aposz(ceil(length(aposz)/2))];
  else
    if ~isempty(fdata)
      OV_CPOINT = [fposx(ceil(length(fposx)/2)) fposy(ceil(length(fposy)/2)) ...
                   fposz(ceil(length(fposz)/2))];
    else
      OV_CPOINT = [0 0 0];
    end
  end
else
  OV_CPOINT = cpoint;
end

[limits,fov] = orthoview('calculatefov',fov);
xlim = limits(1:2);
ylim = limits(3:4);
zlim = limits(5:6);

% Set up figure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
OV_UIH = zeros(1,10);

figh = figure('tag','orthoview');

pos = get(figh,'position');
bgcolor = get(figh,'color');
set(figh,'position',[pos(1) pos(2)-75 582 488],'CloseRequestFcn',...
         'orthoview(''closefigure'');')
set(figh,'colormap',cmap);

asizex = .327;
asizey = .38;
xpos2 = .55;
xpos1 = .08;
ypos2 = .578;
ypos1 = .09;
xyzd = fov/10;
editheight = .06;
textheight = .06;
sliderheight = .04;
poseditlength = .08;

% Axes %%%%%%%%%%%%%%%%%%%%%
% brain axes
% Axial axis
OV_AXH(1) = axes('position',[xpos1 ypos2 asizex asizey]);
OV_IH(1) = image(aposx,aposy,ones(aisize(2),aisize(1)));
hold on
if DEFAULT_OPENGL
  OV_IH(4) = image(fposx,fposy,(cmapsize(1)+1)*ones(fisize(2), ...
                                                      fisize(1)));
end
hold off
set(OV_AXH(1),'ydir','normal','xlim',xlim,'ylim',ylim,...
              'color',[0 0 0],'XAxisLocation','top','tickdir','out');
OV_LH(1) = line([xlim(1) OV_CPOINT(1)-xyzd],[OV_CPOINT(2) OV_CPOINT(2)],...
     'color',DEFAULT_CHCOLOR);
OV_LH(2) = line([OV_CPOINT(1)+xyzd xlim(2)],[OV_CPOINT(2) OV_CPOINT(2)],...
     'color',DEFAULT_CHCOLOR);
OV_LH(3) = line([OV_CPOINT(1) OV_CPOINT(1)],[ylim(1) OV_CPOINT(2)-xyzd],...
     'color',DEFAULT_CHCOLOR);
OV_LH(4) = line([OV_CPOINT(1) OV_CPOINT(1)],[OV_CPOINT(2)+xyzd ylim(2)],...
     'color',DEFAULT_CHCOLOR);

% Coronal Axis
OV_AXH(2) = axes('position',[xpos2 ypos2 asizex asizey]);
OV_IH(2) = image(aposx,aposz,ones(aisize(1),aisize(3)));
hold on
if DEFAULT_OPENGL
  OV_IH(5) = image(fposx,fposz,(cmapsize(1)+1)*ones(fisize(1),fisize(3)));
end
hold off
set(OV_AXH(2),'ydir','normal','xlim',xlim,'ylim',zlim,...
              'color',[0 0 0],'XAxisLocation','top','tickdir','out');
OV_LH(5) = line([xlim(1) OV_CPOINT(1)-xyzd],[OV_CPOINT(3) OV_CPOINT(3)],...
     'color',DEFAULT_CHCOLOR);
OV_LH(6) = line([OV_CPOINT(1)+xyzd xlim(2)],[OV_CPOINT(3) OV_CPOINT(3)],...
     'color',DEFAULT_CHCOLOR);
OV_LH(7) = line([OV_CPOINT(1) OV_CPOINT(1)],[zlim(1) OV_CPOINT(3)-xyzd],...
     'color',DEFAULT_CHCOLOR);
OV_LH(8) = line([OV_CPOINT(1) OV_CPOINT(1)],[OV_CPOINT(3)+xyzd zlim(2)],...
     'color',DEFAULT_CHCOLOR);

% Sagittal Axis
OV_AXH(3) = axes('position',[xpos1 ypos1 asizex asizey]);
OV_IH(3) = image(aposy,aposz,ones(aisize(2),aisize(3)));
hold on
if DEFAULT_OPENGL
  OV_IH(6) = image(fposy,fposz,(cmapsize(1)+1)*ones(fisize(2),fisize(3)));
end
hold off
set(OV_AXH(3),'ydir','normal','xdir','reverse',...
              'xlim',ylim,'ylim',zlim,...
              'color',[0 0 0],'XAxisLocation','top','tickdir','out');
OV_LH(9) = line([ylim(1) OV_CPOINT(2)-xyzd],[OV_CPOINT(3) OV_CPOINT(3)],...
     'color',DEFAULT_CHCOLOR);
OV_LH(10) = line([OV_CPOINT(2)+xyzd ylim(2)],[OV_CPOINT(3) OV_CPOINT(3)],...
     'color',DEFAULT_CHCOLOR);
OV_LH(11) = line([OV_CPOINT(2) OV_CPOINT(2)],[zlim(1) OV_CPOINT(3)-xyzd],...
     'color',DEFAULT_CHCOLOR);
OV_LH(12) = line([OV_CPOINT(2) OV_CPOINT(2)],[OV_CPOINT(3)+xyzd zlim(2)],...
     'color',DEFAULT_CHCOLOR);

orthoview('resetimageaxes');

if ~DEFAULT_CROSSHAIRS
  set(OV_LH(1:12),'visible','off');
end
  

vsbpad = .09;
% Uicontrols
% Position string and edit boxes
uicontrol('style','text','string','Position','units',...
          'normalized','BackgroundColor',bgcolor,'position',...
          [xpos2+vsbpad-.07 ypos1+asizey-editheight .1 editheight]);
OV_UIH(1) = uicontrol('style','edit','string',num2str(OV_CPOINT(1)),...
                      'units','normalized',...
                      'callback','orthoview(''poscallback'');',...
                      'position',...
                      [xpos2+.04+vsbpad ypos1+asizey-editheight ...
                    poseditlength editheight]);
OV_UIH(2) = uicontrol('style','edit','string',num2str(OV_CPOINT(2)),...
                      'units','normalized',...
                      'callback','orthoview(''poscallback'');',...
                      'position',...
                      [xpos2+.04+.1+vsbpad ypos1+asizey-editheight ...
                    poseditlength editheight]);
OV_UIH(3) = uicontrol('style','edit','string',num2str(OV_CPOINT(3)),...
                      'units','normalized',...
                      'callback','orthoview(''poscallback'');',...
                      'position',...
                      [xpos2+.08*3+vsbpad ypos1+asizey-editheight ...
                    poseditlength editheight]);



% Slider and text boxes for display axes
if ~isempty(adata)
  [tmp,iix] = min(abs(OV_APOSX-OV_CPOINT(1)));
  [tmp,iiy] = min(abs(OV_APOSY-OV_CPOINT(2)));
  [tmp,iiz] = min(abs(OV_APOSZ-OV_CPOINT(3)));
  sstep = [1./(aisize-1) 2./(aisize-1)];
  smax = aisize;
else
  if ~isempty(fdata)
    [tmp,iix] = min(abs(OV_FPOSX-OV_CPOINT(1)));
    [tmp,iiy] = min(abs(OV_FPOSY-OV_CPOINT(2)));
    [tmp,iiz] = min(abs(OV_FPOSZ-OV_CPOINT(3)));
    sstep = [1./(fisize-1) 2./(fisize-1)];
    smax = fisize;
  else
    iix = 120;
    iiy = 120;
    iiz = 120;
    sstep = [1 1 1 2 2 2]/240;
    smax = [240 240 240];
  end
end
OV_UIH(4) = uicontrol('style','slider','units','normalized',...
                      'position',[xpos1 ypos2-.06 asizex*.8 sliderheight],...
                      'Max',smax(3),...
                      'min',1,'Value',iiz,...
                      'sliderstep',[sstep(3) sstep(6)],...
                      'callback','orthoview(''slidercallback'',1);');
OV_UIH(5) = uicontrol('style','text','units','normalized',...
                      'position',[xpos1+asizex*.8+.02 ypos2-.08 .06 textheight],...
                      'string',num2str(iiz),'FontSize',14,...
                      'BackgroundColor',bgcolor);
OV_UIH(6) = uicontrol('style','slider','units','normalized',...
                      'position',[xpos2 ypos2-.06 asizex*.8 sliderheight],...
                      'Max',smax(2),...
                      'min',1,'Value',iiy,...
                      'sliderstep',[sstep(2) sstep(5)],...
                      'callback','orthoview(''slidercallback'',2);');
OV_UIH(7) = uicontrol('style','text','units','normalized',...
                      'position',[xpos2+asizex*.8+.02 ypos2-.08 .06 textheight],...
                      'string',num2str(iiy),'FontSize',14,...
                      'BackgroundColor',bgcolor);
OV_UIH(8) = uicontrol('style','slider','units','normalized',...
                      'position',[xpos1 ypos1-.06 asizex*.8 sliderheight],...
                      'Max',smax(1),...
                      'min',1,'Value',iix,...
                      'sliderstep',[sstep(1) sstep(4)],...
                      'callback','orthoview(''slidercallback'',3);');
OV_UIH(9) = uicontrol('style','text','units','normalized',...
                      'position',[xpos1+asizex*.8+.02 ypos1-.08 .06 textheight],...
                      'string',int2str(iix),'FontSize',14,...
                      'BackgroundColor',bgcolor);

% Anatomical and Functional radio buttons
OV_DISPLAY = [DEFAULT_CROSSHAIRS 1 1 0 0];
OV_UIH(16) = uicontrol('style','radiobutton','units','normalized',...
                       'position',...
                       [xpos2-.1 .05 .175 editheight],...
                       'String','Anatomical',...
                       'Backgroundcolor',bgcolor,...
                       'value',1,'callback',...
                       'orthoview(''rbafcallback'',1);');
if isempty(OV_ADATA)
  set(OV_UIH(16),'Value',0,'enable','off')
  OV_DISPLAY(2) = 0;
end
OV_UIH(17) = uicontrol('style','radiobutton','units','normalized',...
                       'position',...
                       [xpos2-.1+.18 .05 .175 editheight],...
                       'String','Functional',...
                       'Backgroundcolor',bgcolor,...
                       'value',1,'callback',...
                       'orthoview(''rbafcallback'',2);');
if isempty(OV_FDATA)
  set(OV_UIH(17),'Value',0,'enable','off')
  OV_DISPLAY(3) = 0;
end
OV_UIH(18) = uicontrol('style','radiobutton','units','normalized',...
                       'position',...
                       [xpos2-.1+.36 .05 .175 editheight],...
                       'String','Mask Mode',...
                       'Backgroundcolor',bgcolor,...
                       'value',0,'callback',...
                       'orthoview(''rbafcallback'',3);');
if ~isempty(OV_MASK)
  set(OV_UIH(18),'Value',1);
  OV_DISPLAY(4) = 1;
end

% Controls that display when there is a functional image
if ~isempty(fdata)
  orthoview('createfuncui');
end

% Figure Menus

% File Menu
uitmp = uimenu('Parent',figh,'Label','File');
uimenu('Parent',uitmp,'Label','Open Anatomical from File','callback',...
       ['FFFF_NAME1 = orthoview(''dialog'',''editfile'',''Select file'');',...
       'orthoview(''setanat'',FFFF_NAME1);','clear FFFF_NAME1;']);
% uimenu('Parent',uitmp,'Label','Open anatomical (desktop)');
uimenu('Parent',uitmp,'Label','Open Functional from File','callback',...
       ['FFFF_NAME1 = orthoview(''dialog'',''editfile'',''Select file'');',...
       'orthoview(''setfunc'',FFFF_NAME1);','clear FFFF_NAME1;']);
% uimenu('Parent',uitmp,'Label','Open functional (desktop)');
uimenu('Parent',uitmp,'Label','Save Functional');
uimenu('Parent',uitmp,'Label','Save Mask');
uimenu('Parent',uitmp,'Label','Export Functional');
uimenu('Parent',uitmp,'Label','Export Mask');


uimenu('Parent',uitmp,'Label','Print','separator','on',...
       'callback','printdlg(gcbf)');
uimenu('Parent',uitmp,'Label','Export to image');
uimenu('Parent',uitmp,'Label','Export to figure','callback',...
       'orthoview(''exporttofigure'');');

uimenu('Parent',uitmp,'Label','Close','callback',...
       'orthoview(''closefigure'');');

% Edit Menu
uitmp = uimenu('Parent',figh,'Label','Edit');
uimenu('Parent',uitmp,'Label','Mask','callback',...
       'orthoview(''editmaskgui'');');
uimenu('Parent',uitmp,'Label','Functional','callback',...
       'orthoview(''editvolgui'');');
cbstr = ['makecmap(gcbf,''fig'',',num2str(cmapsize(1)),...
         '+1:',num2str(cmapsize(1)),...
         '+',num2str(cmapsize(2)),');'];
uimenu('Parent',uitmp,'Label','Colormap (Functional)',...
       'callback',cbstr);
cbstr = ['makecmap(gcbf,''fig'',1:',num2str(cmapsize(1)),');'];
uimenu('Parent',uitmp,'Label','Colormap (Anatomical)',...
       'callback',cbstr);

% View Menu
uitmp = uimenu('Parent',figh,'Label','View');
OV_UIH(19) = uimenu('Parent',uitmp,'Label','Antomical','callback',...
                    'orthoview(''vmenucallback'',1);');
if ~isempty(OV_ADATA)
  set(OV_UIH(19),'checked','on');
end
OV_UIH(20) = uimenu('Parent',uitmp,'Label','Functional','callback',...
                    'orthoview(''vmenucallback'',2);');
if ~isempty(OV_FDATA)
  set(OV_UIH(20),'checked','on');
end
OV_UIH(21) = uimenu('Parent',uitmp,'Label','Mask Mode','callback',...
                    'orthoview(''vmenucallback'',3);');
if ~isempty(OV_MASK)
  set(OV_UIH(21),'checked','on');
end
OV_UIH(22) = uimenu('Parent',uitmp,'Label','Cross Hairs',...
                    'callback','orthoview(''vmenucallback'',4);');
if DEFAULT_CROSSHAIRS
  set(OV_UIH(22),'checked','on');
end
OV_UIH(23) = uimenu('Parent',uitmp,'Label','Mask',...
                    'callback','orthoview(''vmenucallback'',5);');


uimenu('Parent',uitmp,'Label','Histogram','callback',...
       'orthoview(''histogram'');','separator','on')



controller('setupmenu','orthoview')

orthoview('drawslices')

OV_VEUIH = zeros(1,6);

% Set up mouse handlers
set(figh,'WindowButtonDownFcn','orthoview(''mousedown'');');

% Call controller to set up menu