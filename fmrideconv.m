function [G] = fmrideconv(list,bsize,ssize,lags,tr,pad,rsize)
% FMRIDECONV - Create design matrix for fmri regression using deconvolution
%
%   usage:
%      [G] = fmrideconv(list,tsize,ssize,lags,tr,pad,rsize)
%
%      list   - List of stimuli (vector/matrix)
%      tsize  - trial lengths (secs) (scalar or vector/matrix)
%      ssize  - stimulus lengths (secs) (scalar or vector/matrix)
%      lags   -
%      tr     - TR (secs)
%      pad    - add or subtract zeros at the beg and end of each run
%               [beg,end]
%      rsize  - size in trials of each run
%
%   examples:
%      block design (1 condition, 1 run):
%        G = fmridesign([1 1 1 1 1],30,15,1,0,[0 0]);
%      event design (2 conditions, 2 runs):
%        G = fmridesign([1 2 1; 2 1 2],15,1,1,0,[0 0]);
%      event jittered design (1 condition, 2 runs)
%        G = fmridesign([1 1 1 1 1 1],[12 10 8 10 8 8],1,2,0,[0 0],[3 3]);
%      

% written by Colin Humphries
% chumphri@mcw.edu

% Sub-sampling factor
SCALE_FACTOR = 10;

if nargin < 7
  rsize = [];
end

if nargin < 6
  pad = [0 0];
end



if all(size(list) > 1)
  rsize = repmat(size(list,2),1,size(list,1));
  list = list';
  list = list(:);
else
  if isempty(rsize)
    rsize = length(list);
  end
end

if length(bsize(:)) == 1
  bsize = bsize*ones(size(list));
else
  bsize = bsize';
  bsize = bsize(:);
end

if length(ssize(:)) == 1
  ssize = ssize*ones(size(list));
else
  ssize = ssize';
  ssize = ssize(:);
end

cond = unique(list(:));
numcond = length(cond);

G = zeros(sum(bsize)/tr+sum(pad)*length(rsize),numcond*length(rsize)* ...
	  length(lags)+length(rsize));

% bsize = bsize*SCALE_FACTOR;
% ssize = ssize*SCALE_FACTOR;
% delay = delay*SCALE_FACTOR;
bsize = bsize/tr;
ssize = ssize/tr;


% G = zeros(numblocks*bsize,numcond+numruns);


for ii = 1:numcond
  iind = list == ii;
  for jj = 1:length(rsize)
    % tmp = zeros(size(G,1)*SCALE_FACTOR*tr,1);
    % keyboard
    tmp = zeros(sum(bsize(sum(rsize(1:(jj-1)))+1:sum(rsize(1:(jj))))),1);
    for kk = 1:rsize(jj)
      curindex = kk+sum(rsize(1:(jj-1)));
      if iind(curindex)
	tmpind = sum(bsize((sum(rsize(1:(jj-1)))+1):(curindex-1)))+1;
	tmp(tmpind:tmpind+ssize(curindex)-1) = ones;
      end
    end
    % tmp2 = conv(tmp,HRF);
    % if delay(jj) < 0
    %   tmp2 = tmp2(round(abs(delay(jj))):end);
    % elseif delay(jj) > 0
    %   tmp2 = [zeros(round(delay(jj)),1);tmp2];
    % end
    % keyboard
    % tmp3 = resample(tmp2(1:length(tmp)),1,tr*SCALE_FACTOR);
    % keyboard
    if pad(1) > 0
      tmp = [zeros(pad(1),1);tmp];
    elseif pad(1) < 0
      tmp = tmp(1+abs(pad(1)):end);
    end
    if pad(2) > 0
      tmp = [tmp;zeros(pad(2),1)];
    elseif pad(2) < 0
      tmp = tmp(1:(end+pad(2)));
    end
    
%    G(sum(bsize(1:(sum(rsize(1:(jj-1)))+1)-1))/SCALE_FACTOR/tr+1: ...
 %     sum(bsize(1:(sum(rsize(1:(jj)))+1)-1))/SCALE_FACTOR/tr,(ii-1)* ...
  %    length(rsize)+jj) = resample(tmp2(1:length(tmp)),1,tr*SCALE_FACTOR);
    %G(sum(bsize(1:(sum(rsize(1:(jj-1)))+1)-1))/SCALE_FACTOR/tr+1+(jj-1)*sum(pad): ...
     % sum(bsize(1:(sum(rsize(1:(jj)))+1)-1))/SCALE_FACTOR/tr+jj*sum(pad),(ii-1)* ...
      %length(rsize)+jj) = tmp3;
      % keyboard
      for ll = 1:length(lags)
	G(sum(bsize(1:(sum(rsize(1:(jj-1)))+1)-1))+1+(jj-1)*sum(pad): ...
	  sum(bsize(1:(sum(rsize(1:(jj)))+1)-1))+jj*sum(pad),(ii-1)* ...
	  length(rsize)*length(lags)+(jj-1)*length(lags)+ll) = ...
	    [zeros(lags(ll)-1,1); tmp(1:end-(lags(ll)-1))];
      end
    
    % keyboard
  end
end

for jj = 1:length(rsize)
  G(sum(bsize(1:(sum(rsize(1:(jj-1)))+1)-1))+1+(jj-1)*sum(pad): ...
    sum(bsize(1:(sum(rsize(1:(jj)))+1)-1))+jj*sum(pad), ...
    numcond*length(rsize)*length(lags)+jj)= ones;
end



