
# List of Commands

## FMRI

* fmrianalysis.m - Analyze fMRI data using regression
* fmricluster.mex - Find and label contiguous 'clusters' in gridded data
* fmrideconv.m - Create design matrix for fmri regression using deconvolution
* fmridesign.m - Create design matrix for fmri regression
* fmridetrend.m - Remove mean, trend, and nuisance variables from fMRI data
* fmrireadxmat.m - Read AFNI xmat file
* fmrisinebasis.m - Create sinewave basis set for fMRI analysis
* fmrisurf2volindex.m - Map points on a cortical surface to volume indices
* fmritentbasis.m - Create tent function basis set for fMRI analysis
* orthoview.m - View fMRI data in orthogonal windows
* readanalyze.m - Read MRI files in the Analyze format
* readcurvfile.m - Read FreeSurfer binary curv/thickness file
* readnifti.mex - Read MRI files in the NIFTI format
* readpatch.m - Read Freesurfer patch file
* readsurface.m - Read FreeSurfer surface file
* surfdraw.m - Plot functional data on brain surface
* surfoutline.mex
* writeanalyze.m - Save MRI data in the Anaylze format
* writenifti.m - Write MRI files in the NIFTI format

## MEG/EEG

## Graphics
* caxiscenter.m - Center caxis around zero
* makecmap.m - Interactive colormap editor
* svgwrite.m - Output figure as an SVG file
* xaxis.m - Control x-axis appearance
* yaxis.m - Control y-axis appearance
* zaxis.m - Control z-axis appearance

## Stats

## Audio

* generatetone.m - Create an audio file with multiple tone


## General

* nearestpoint.mex - Find the nearest point to a set of reference points
