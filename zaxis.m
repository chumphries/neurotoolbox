% ZAXIS   Control z-axis appearance
%
% changes state of the zaxis
%
%   usage: [A,B] = zaxis;            - get current zaxis limits
%                  zaxis([A,B])      - change axis limits
%                  zaxis auto        - change axis scaling to auto
%                        manual      - change axis scaling to manual
%                        grid        - toggle grid on/off
%                        dir         - toggle direction normal/reverse
%                        log         - set axis scale to log
%                        linear      - set axis scale to linear
%
%

% Written by Colin Humphries, Salk Institute
%   April, 1998    colin@salk.edu

% Copywrite (c) 1998-2018, Colin J. Humphries

% This file is distributed under the terms of the GNU General Public
% License version 3. See accompanying license file for more details or
% visit http://www.gnu.org/licenses/


function out = zaxis(in)

axh = gca;

if nargin < 1
  out = get(axh,'zlim');
  return  
end

if isstr(in)
  switch in
    case 'auto'
      set(axh,'zlimmode','auto')
    case 'manual'
      set(axh,'zlimMode','manual')
    case 'log'
      set(axh,'Zscale','log')
    case 'linear'
      set(axh,'Zscale','linear')
    case 'grid'
      if strcmp(get(axh,'zgrid'),'on')
        set(axh,'Zgrid','off')
      else
	set(axh,'Zgrid','on')
      end
    case 'dir'
      if strcmp(get(axh,'zdir'),'normal')
        set(axh,'Zdir','reverse')
      else
	set(axh,'Zdir','normal')
      end      
    otherwise
      error('Invalid Parameter for zaxis')
  end
else
  if length(in) > 2
    set(axh,'zlim',[in(1) in(end)],'ztick',in)
  else
    set(axh,'zlim',in)
  end
end

