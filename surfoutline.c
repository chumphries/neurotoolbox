#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "mex.h"

#define MAP_IN    prhs[0]
#define VERTS_IN  prhs[1]
#define FACES_IN  prhs[2]
#define CMAT_OUT  plhs[0]

int dsurfmaxlinks(int, int, double *);
void makeneighbormatrix(int *, int, int, int, double *);
int sharepoint(int, int, int *, int, int, double *);
void tagbranches(int, uint8_t *, int *, int *,int *, int *, int *, int, int, double *,int);
 
void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{

  int numverts, numfaces, nummapverts, numdims, ii, jj, kk, ll, mm;
  int *nmatrix, maxn, isane, numn, lbindex, curpoint, endloop;
  uint8_t *maptags;
  double *p_faces, *p_verts, *p_map, *p_cmat;
  int *linebuffer;
  int *dimensions, *linesizes, lsindex, indind;

  if (nrhs < 3) {
    mexErrMsgTxt("Not enough inputs.");
  }
  
  numverts = mxGetM(VERTS_IN);
  numfaces = mxGetM(FACES_IN);

  /* if (mxGetN(VERTS_IN) != 3) {
    mexErrMsgTxt("Vertices matrix must have 3 columns");
    } */
  numdims = mxGetN(VERTS_IN);
  if (mxGetN(FACES_IN) != 3) {
    mexErrMsgTxt("Faces matrix must have 3 columns");
  }

  if (mxGetM(MAP_IN) > mxGetN(MAP_IN)) {
    nummapverts = mxGetM(MAP_IN);
  }
  else {
    nummapverts = mxGetN(MAP_IN);
  }
  if (nummapverts != numverts) {
    mexErrMsgTxt("Map must have the same number of vertices as verts.");
  }

  if (!mxIsDouble(MAP_IN)) {
    mexErrMsgTxt("Inputs must be double precision");
  }
  if (!mxIsDouble(VERTS_IN)) {
    mexErrMsgTxt("Inputs must be double precision");
  }
  if (!mxIsDouble(FACES_IN)) {
    mexErrMsgTxt("Inputs must be double precision");
  }

  maptags = (uint8_t *)mxCalloc(numverts,sizeof(uint8_t));

  p_faces = mxGetPr(FACES_IN);
  p_map = mxGetPr(MAP_IN);
  p_verts = mxGetPr(VERTS_IN);

  maxn = dsurfmaxlinks(numverts,numfaces,p_faces);
  
  nmatrix = (int *)mxCalloc(numverts*maxn,sizeof(int));
  /* mexPrintf("Start point 2\n"); */
  
  makeneighbormatrix(nmatrix,numverts,maxn,numfaces,p_faces);
  /* mexPrintf("Start point 3\n"); */

  /*
    Go through and identify edge vertices
    Only keep vertices with 2 or more neighbors
  */

  /* Note: try tagging 0 vertices that have a non-zero neighbor */

  for (ii=0; ii<numverts; ++ii) {
    if (p_map[ii] == 0) {
      jj = 0;
      isane = 0;
      numn = 0;
      while(jj < maxn) {
	if (nmatrix[ii+jj*numverts]) {
	  /*
	  if (p_map[nmatrix[ii+jj*numverts]] == 0) {
	    isane = 1;
	  }
	  else {
	    ++numn;
	  }
	  */
	  if (p_map[nmatrix[ii+jj*numverts]] != 0) {
	    isane = 1;
	  }


	}
	else {
	  break;
	}
	

	++jj;
      }
      /*
      if (numn > 0) {
	maptags[ii] = (uint8_t)isane;
      }
      else {
	maptags[ii] = 0;
      }
      */
      maptags[ii] = (uint8_t)isane;

    }
    else {
      maptags[ii] = 0;
    }
  }
  /* mexPrintf("Start point 4\n"); */
  /*
  for(ii=0; ii<numverts; ++ii) {
    p_cmat[ii] = (double)maptags[ii];
  }
  */

  /* Trace lines along edge vertices */

  linebuffer = (int *)mxCalloc(numverts,sizeof(int));
  linesizes = (int *)mxCalloc(numverts,sizeof(int));

  lbindex = 0;
  lsindex = 0;
  for (ii=0; ii<numverts; ++ii) {
    if (maptags[ii] == 1) {

      curpoint = ii;
      maptags[curpoint] = 2;
      tagbranches(curpoint,maptags,linebuffer,linesizes,&lbindex,&lsindex,nmatrix,numverts,maxn,p_map,-1);
      ++lsindex;

    }
  }

  dimensions = (int *)mxCalloc(2,sizeof(int));

  lbindex = 0;
  indind = 0;
  for (ii=0; ii<numverts; ++ii) {
    lbindex = lbindex + linesizes[ii];
    if (linesizes[ii] > 0) {
      ++indind;
    }
  }

  if (lbindex > 0) {
    /* mexPrintf("%d %d %d\n",kk,lsindex,lbindex); */
    dimensions[0] = lbindex+indind;
    /* dimensions[1] = 3; */
    dimensions[1] = numdims;
    CMAT_OUT = mxCreateNumericArray(2,dimensions, mxDOUBLE_CLASS,mxREAL);
    p_cmat = mxGetPr(CMAT_OUT);

    ii = 0;
    ll = 0;
    for (jj=0; jj<lsindex; ++jj) {
      if (linesizes[jj] > 0) {
	p_cmat[ii] = (double)linesizes[jj];
	++ii;
	for (kk=0; kk<linesizes[jj]; ++kk) {
	  for (mm=0; mm<numdims; ++mm) {
	    p_cmat[ii+(lbindex+indind)*mm] = 
	      p_verts[linebuffer[ll]+mm*numverts];
	  }
	  /* p_cmat[ii] = p_verts[linebuffer[ll]];
	  p_cmat[ii+(lbindex+indind)] = p_verts[linebuffer[ll]+numverts];
	  p_cmat[ii+(lbindex+indind)*2] = p_verts[linebuffer[ll]+numverts*2]; */
	  ++ii;
	  ++ll;
	}
      }
    }
    
  }


  mxFree(maptags);
  mxFree(nmatrix);
  mxFree(linebuffer);
  mxFree(dimensions);
  mxFree(linesizes);
}


int dsurfmaxlinks(int numvertices, int numfaces, double *faces)
{

  int ii, faceind, *linkbuffer;

  /*
    Note: the size of the buffer is numvertices + 1 because if c-code
    is calling this function then it is 0 indexed and if matlab is
    calling it then it is 1-indexed.
  */
  linkbuffer = mxCalloc(numvertices+1,sizeof(int));

  for (ii=0; ii<numfaces; ++ii) {
    ++linkbuffer[(int)faces[ii]];
    ++linkbuffer[(int)faces[ii+numfaces]];
    ++linkbuffer[(int)faces[ii+numfaces*2]];
  }

  faceind = 0;
  for (ii=0; ii<numvertices+1; ++ii) {
    if (linkbuffer[ii] > faceind) {
      faceind = linkbuffer[ii];
    }
  }

  mxFree(linkbuffer);

  return faceind;
}

void makeneighbormatrix(int *nmatrix, int numverts, int maxn, int numfaces, double *faces) 
{

  int ii, jj, f1, f2, f3;

  for (ii=0; ii<numfaces; ++ii) {
    f1 = (int)(faces[ii]-1);
    f2 = (int)(faces[ii+numfaces]-1);
    f3 = (int)(faces[ii+2*numfaces]-1);
    
    jj = 0;
    while (jj<maxn) {
      if (nmatrix[f1+jj*numverts] == 0) {
	nmatrix[f1+jj*numverts] = f2;
	break;
      }
      else if (nmatrix[f1+jj*numverts] == f2) {
	break;
      }
      ++jj;
    }
    jj = 0;
    while (jj<maxn) {
      if (nmatrix[f1+jj*numverts] == 0) {
	nmatrix[f1+jj*numverts] = f3;
	break;
      }
      else if (nmatrix[f1+jj*numverts] == f3) {
	break;
      }
      ++jj;
    }

    jj = 0;
    while (jj<maxn) {
      if (nmatrix[f2+jj*numverts] == 0) {
	nmatrix[f2+jj*numverts] = f1;
	break;
      }
      else if (nmatrix[f2+jj*numverts] == f1) {
	break;
      }
      ++jj;
    }
    jj = 0;
    while (jj<maxn) {
      if (nmatrix[f2+jj*numverts] == 0) {
	nmatrix[f2+jj*numverts] = f3;
	break;
      }
      else if (nmatrix[f2+jj*numverts] == f3) {
	break;
      }
      ++jj;
    }

    jj = 0;
    while (jj<maxn) {
      if (nmatrix[f3+jj*numverts] == 0) {
	nmatrix[f3+jj*numverts] = f1;
	break;
      }
      else if (nmatrix[f3+jj*numverts] == f1) {
	break;
      }
      ++jj;
    }
    jj = 0;
    while (jj<maxn) {
      if (nmatrix[f3+jj*numverts] == 0) {
	nmatrix[f3+jj*numverts] = f2;
	break;
      }
      else if (nmatrix[f3+jj*numverts] == f2) {
	break;
      }
      ++jj;
    }

  }


}

int sharepoint(int n1, int n2, int *nmatrix, int numverts, int maxn, double *p_map)
{

  /* Check if the two vertices (n1, n2) share any neighbors and if
     those neighbors are non zero in p_map
  */

  int ii, jj;

  for (ii=0; ii<maxn; ++ii) {
    if (nmatrix[n1+ii*numverts] == 0) {
      break;
    }
    for (jj=0; jj<maxn; ++jj) {
      if (nmatrix[n2+jj*numverts] == 0) {
	break;
      }
      if (nmatrix[n1+ii*numverts] == nmatrix[n2+jj*numverts]) {
	if (p_map[nmatrix[n1+ii*numverts]] != 0) {
	  return 1;
	}
      }

    }
  }


  return 0;


}


void tagbranches(int curpoint, uint8_t *maptags, int *linebuffer, int *linesizes,int *lbindex, int *lsindex, int *nmatrix, int numverts, int maxn, double *p_map, int lastpoint) 
{

  int jj, foundbranch;
  /* mexPrintf("%d %d %d\n",*lbindex,curpoint,maptags[curpoint]); */
  /* maptags[curpoint] = 0; */

  /* Loop through each neighbor */
  foundbranch = 0;
  jj = 0;
  while (jj<maxn) {
    if (nmatrix[curpoint+jj*numverts]) {
      if (maptags[nmatrix[curpoint+jj*numverts]] == 1) {
	if (sharepoint(curpoint,nmatrix[curpoint+jj*numverts],
		       nmatrix,numverts,maxn,p_map)) {
	  if (nmatrix[curpoint+jj*numverts] != lastpoint) {
	    
	    /* Here we have a valid branch */
	    if (foundbranch) {
	      ++(*lsindex);
	    }
	    linebuffer[*lbindex] = curpoint;
	    ++(*lbindex);
	    ++linesizes[*lsindex];
	    
	    foundbranch = 1;
	    maptags[nmatrix[curpoint+jj*numverts]] = 2;
	    tagbranches(nmatrix[curpoint+jj*numverts],maptags,
			linebuffer,linesizes,lbindex,lsindex,
			nmatrix,numverts,maxn,p_map,curpoint);
	  }
	}
      }
      else if (maptags[nmatrix[curpoint+jj*numverts]] == 2) {
	if (sharepoint(curpoint,nmatrix[curpoint+jj*numverts],
		       nmatrix,numverts,maxn,p_map)) {
	  if (nmatrix[curpoint+jj*numverts] != lastpoint) {
	    
	    /* Here we have a valid branch */
	    if (foundbranch) {
	      ++(*lsindex);
	    }
	    linebuffer[*lbindex] = curpoint;
	    ++(*lbindex);
	    ++linesizes[*lsindex];
	    
	    linebuffer[*lbindex] = nmatrix[curpoint+jj*numverts];
	    ++(*lbindex);
	    ++linesizes[*lsindex];
	    
	    foundbranch = 1;
	    
	  }
	}

      }

    }
    else {
      /* End of neighbors */
      break;
    }
    ++jj;
  }
  
  if (foundbranch == 0) {
    linebuffer[*lbindex] = curpoint;
    ++(*lbindex);
    ++linesizes[*lsindex];
  }

}
