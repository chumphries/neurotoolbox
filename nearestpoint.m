%NEARESTPOINT Find the nearest point to a set of reference points 
%   IND = NEARESTPOINT(X,Y) finds the closest point in matrix Y that
%   is closest in space to each point in X. Each row of Y is a
%   separate point. If X is a a matrix then the function returns a
%   value for each row of X. The number of columns for X and Y must be
%   equal.

% Written by Colin Humphries
%   Feb, 2010

% See nearestpoint.c for source code.

% The program uses a k-d tree algorithm
% https://en.wikipedia.org/wiki/K-d_tree

% Mex file can be compiled by typing: 
% >> mex nearestpoint.c
% at the matlab prompt.

