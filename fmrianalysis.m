function [C,E,df] = fmrianalysis(dataname,G,c)
% FMRIANALYSIS   Analyze fMRI data using regression
%
%   usage:
%         [B,E,df] = fmrianalysis(data,G)
%         [C,E,df] = fmrianalysis(data,G,c)
%         [F,df1,df2] = fmrianalysis(data,G,{c})
%
%      data   : data matrix [images x voxels]
%      G      : design matrix [images x regressors]
%      c      : contrasts either a
%               matrix [contrasts x regressors] or a
%               cell array
%
%  note: t = C./sqrt(E);
%
%  contrast is t = (c*B)./sqrt((E*(c*inv(G'*G)*c')));

% Written by Colin Humphries
%

if nargin < 3
  c = [];
end

[N,voxelsize]  = size(dataname);
rG = rank(G);
k = size(G,2);
Ginv = inv(G'*G);

% B = zeros(k,voxelsize);

if isempty(c)
  % Calculate Beta coefficents
  C = zeros(k,voxelsize);
  E = zeros(1,voxelsize);
  for ii = 1:voxelsize
    D = dataname(:,ii);
    X = double(D);
    C(:,ii) = Ginv*(G'*X);
    if length(nargout > 1)
      Rf = (X-G*C(:,ii))'*(X-G*C(:,ii));
      E(ii) = (Rf/(N-k));
      % E(ii) = Rf;
    end
    % E(:,ii) = diag((Rf/(N-k))*Ginv);
  end
  df = N-rG;
else
  % Calculate contrasts
  if ~iscell(c)
    % T-tests
    E = zeros(size(c,1),voxelsize);
    C = zeros(size(c,1),voxelsize);
    
    for ii = 1:voxelsize
      % waitbar(i/voxelsize,wb);
      D = dataname(:,ii);
      X = double(D);
      B = Ginv*(G'*X);
      Rf = (X-G*B)'*(X-G*B);
      for jj = 1:size(c,1)
	cc = c(jj,:);
	E(jj,ii) = (Rf/(N-k))*cc*Ginv*cc';
	C(jj,ii) = cc*B;
      end
    end
    df = N-rG;
  else
    % F-tests
    C = zeros(length(c),voxelsize);    
    for ii = 1:voxelsize
      D = dataname(:,ii);
      if any(D)
        X = double(D);
        B = Ginv*(G'*X);
        Rf = (X-G*B)'*(X-G*B);
        for jj = 1:length(c)
          cc = c{jj};
          C(jj,ii) = (((cc*B)'*inv(cc*Ginv*cc')*(cc*B))/size(cc,1))/(Rf/(N- ...
                                                            rG));
        end
      else
        for jj = 1:length(c)
          C(jj,ii) = 0;
        end
      end
    end
    df = repmat(N-rG,1,length(c));
    for jj = 1:length(c)
      E(1,jj) = size(c{jj},1);
    end
  end
end

