%FMRICLUSTER   Find and label contiguous clusters in gridded data
%   FMRICLUSTER(DATA,LEVEL) returns an array the same size as DATA
%   with contiguous points labeled with unique numbers. DATA is an N-d
%   array.
%
%   LEVEL is a number less than or equal to the number of dimensions
%   in DATA that is used to define the degree of contiguity. A
%   value of 1 will only consider neighbors along 1 of the N
%   dimensions. A value of 2 will consider neighbors along 1
%   dimension and neighbors along diagonals defined by 2
%   dimensions. A value of 3 will add diagonals along 3
%   dimensions. Eg. For a 3-dimensional array. Level 1 will be 6
%   neighbors, level 2 will be 17 neighbors, and level 3 will be 26
%   neighbors.
%

% Written by Colin Humphries
%    3/2010
